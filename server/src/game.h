#ifndef _GAME_H_
#define _GAME_H_

/* C standard library */
#include <stdint.h>

typedef struct game_t
{
    
} Game;

Game* game_create();
void  game_destroy(Game* game);
void  game_start(Game* game);
void  game_stop(Game* game);
void* game_thread(void* param);

#endif
