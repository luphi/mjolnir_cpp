#ifndef _LOG_H_
#define _LOG_H_

/* C standard library */
#include <stdio.h>

/* additional libraries */
#include <pthread.h>

typedef struct Log_t
{
    char            user_text[2056];  /* in short, stuff shown in the UI */
    char            debug_text[2056]; /* information for developers */
    int             user_len;         /* current length of user_text */
    int             debug_len;        /* current length of debug_text */
    FILE*           user_fd;
    FILE*           debug_fd;
    pthread_mutex_t mutex;
} Log;

Log* log_create();
void log_destroy(Log* log);
void log_enter(Log* log, const char* function);
void log_exit(Log* log, const char* function);
void log_append_user(Log* log, char* text);
void log_append_debug(Log* log, char* text);
void log_write(Log* log);

#endif
