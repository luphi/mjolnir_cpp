#ifndef _EVENTFIFO_H_
#define _EVENTFIFO_H_

#include <cstdint>
#include <queue>

#include <pthread.h>

typedef struct EventTag
{
    uint16_t op;       /* operation code */
    uint16_t id;       /* player ID, originator or recipient of */
    uint16_t len;      /* length of data in bytes */
    uint8_t  buf[512]; /* data/payload */
} Event;

enum Events
{
    NO_OP,
    PING,
    PONG,
    VIKING_CONNECT,
    VIKING_DISCONNECT,
    VIKING_NAME,
    VIKING_CHOICE,
    WEAPON_SUBTYPE,
    MESSAGE_ALL,
    MESSAGE_TEAM,
    SERVER_FULL,
    SERVER_SHUTDOWN
};

class EventFIFO
{
public:
    
    EventFIFO();
    ~EventFIFO();
    
    void  pushEvent(Event ev);
    Event popEvent();
    
    void packPong(Event* ev);
    void packVikingName(Event* ev, uint16_t id, char* name);
    void packVikingChoice(Event* ev, uint16_t id, uint16_t choice);
    void packWeaponSubtype(Event* ev, uint16_t id, uint16_t subtype);
    void packMessageAll(Event* ev, uint16_t id, char* message);
    void packMessageTeam(Event* ev, uint16_t id, char* message);
    
private:

    pthread_mutex_t   mutex;
    std::queue<Event> fifo;
};

#endif
