#ifndef _SCORE_H_
#define _SCORE_H_

typedef struct score_t
{
    
} Score;

Score* score_create();
void   score_destroy(Score* score);
void   score_start(Score* score);
void   score_stop(Score* score);
void*  score_thread(void* param);

#endif
