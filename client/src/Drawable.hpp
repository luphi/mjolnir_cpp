#ifndef _DRAWABLE_H_
#define _DRAWABLE_H_

#include <glm/glm.hpp>

/**
 * 
 * A Drawable is any object which can be displayed on the video surface.
 *  
 */

class Drawable
{
	
public:

	virtual ~Drawable() { }

	virtual void draw() = 0;
    
    glm::vec2 position; /* X and Y coordinates */
    glm::vec2 dimensions; /* width and height */
};

#endif
