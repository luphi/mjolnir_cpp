#ifndef _PHYSICAL_H_
#define _PHYSICAL_H_

class Physical
{
public:

    Physical();
    ~Physical();
    
    void step(int dt);
    
    float x; /* X position */
    float y; /* Y position */
    float xVel; /* X velocity */
    float yVel; /* Y velocity */
    float xAcc; /* X acceleration */
    float yAcc; /* Y acceleration */
    bool  frozen; /* physics cease if true */

private:
    
    float acceleration(float px, float pv, float a, int dt);
};

#endif
