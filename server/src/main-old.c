#include <stddef.h> /* for NULL */
#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include <SDL2/SDL_thread.h>

#include "types.h"

#define WINDOW_WIDTH  240
#define WINDOW_HEIGHT 320
#define PORT          1134
#define BUFFER_SIZE   1048576 /* 2^20 ~= 1.05 MB */

Player*       players[8];
SDL_Window*   window;
SDL_Renderer* renderer;
SDL_Rect      startButton;
SDL_Rect      stopButton;
SDL_Thread*   serverThread;
SDL_mutex*    mutex;
int           listening;
int           quit;

void send_all(int ignore, const void* data, int len)
{
    int i;
    
    if (data == NULL || len <= 0)
    {
        return;
    }
    
    for (i = 0; i < 8; i++)
    {
        if (players[i] != NULL && players[i]->id != ignore)
        {
            SDLNet_TCP_Send(players[i]->socket, data, len);
        }
    }
}

static int server_thread(void* data)
{
    IPaddress         serverIP;
    TCPsocket         serverSocket;
    SDLNet_SocketSet  socketSet;
    char              buffer[BUFFER_SIZE];
    int               i;
    
    for (i = 0 ; i < 8; i++)
    {
        players[i] = NULL;
    }
    
    if (SDLNet_Init() < 0)
    {
        printf("SDL network initialization failed\n");
        SDL_Quit();
        return 1;
    }
    
    socketSet = SDLNet_AllocSocketSet(8);
    if (socketSet == NULL)
    {
        printf("Failed to allocate memory for socket sets\n");
        SDLNet_Quit();
        SDL_Quit();
        return 1;
    }
    
    if (SDLNet_ResolveHost(&serverIP, NULL, PORT) < 0)
    {
        printf("Failed to listen on port %d\n", PORT);
        SDLNet_Quit();
        SDL_Quit();
        return 1;
    }
    
    serverSocket = SDLNet_TCP_Open(&serverIP);
    if (serverSocket == NULL)
    {
        printf("Failed to bind port %d\n", PORT);
        SDLNet_Quit();
        SDL_Quit();
        return 1;
    }
    
    SDLNet_TCP_AddSocket(socketSet, serverSocket);
    listening = 1;
    
    printf("Server started\n");
    
    while (listening == 1)
    {
        int check;
        
        /* CheckSockets will return the number of sockets with activity */
        check = SDLNet_CheckSockets(socketSet, 0);
        
        /* At least one socket (including the server socket) is active */
        /* The number with activity is unimportant as we'll be cycling */
        /* through them all in this block anyway */
        if (check > 0)
        {
            if (SDLNet_SocketReady(serverSocket) != 0)
            {
                int added;
                
                added = 0;
                for (i = 0; i < 8; i++)
                {
                    if (players[i] == NULL)
                    {
                        players[i] = (Player*) malloc(sizeof(Player));
                        players[i]->socket = SDLNet_TCP_Accept(serverSocket);
                        SDLNet_TCP_AddSocket(socketSet, players[i]->socket);
                        added = 1;
                        printf("added new socket at index %d\n", i);
                        break; /* exit the loop */
                    }
                }
                
                if (added == 0)
                {
                    TCPsocket reject; /* is this name a bit harsh? */
                    
                    reject = SDLNet_TCP_Accept(serverSocket);
                    /* TODO: send a message to indicate we're full */
                    SDLNet_TCP_Close(reject);
                }
            }
            
            for (i = 0; i < 8; i++)
            {
                int ready;
                
                ready = 0;
                if (players[i] != NULL)
                {
                    ready = SDLNet_SocketReady(players[i]->socket);
                }
                
                /* if the player exists and its socket has activity */
                if (ready > 0)
                {
                    int bytes;
                    
                    bytes = SDLNet_TCP_Recv(players[i]->socket, buffer,
                        BUFFER_SIZE);
                    
                    printf("received: %s\n", buffer);
                    
                    /* if there was an error or a disconnect */
                    if (bytes <= 0)
                    {
                        /* the distinction between an error and a disconnect */
                        /* is unimportant - we'll close the socket either way */
                        SDLNet_TCP_DelSocket(socketSet, players[i]->socket);
                        SDLNet_TCP_Close(players[i]->socket);
                        free(players[i]);
                        players[i] = NULL;
                    }
                }
            }
        }
        else if (check == -1) /* CheckSockets returns -1 for errors */
        {
            printf("An error occurred while the server was running: %s\n",
                SDLNet_GetError());
            listening = 0;
            break;
        }
    }
    
    printf("Server stopped\n");
    
    SDLNet_FreeSocketSet(socketSet);
    SDLNet_TCP_Close(serverSocket);
    SDLNet_Quit();
    
    return 0;
}

int main()
{
    window        = NULL;
    renderer      = NULL;
    serverThread  = NULL;
    quit          = 0;
    startButton.x = WINDOW_WIDTH / 6;
    startButton.y = WINDOW_HEIGHT / 7;
    startButton.w = WINDOW_WIDTH * 2 / 3;
    startButton.h = WINDOW_HEIGHT * 2 / 7;
    stopButton.x  = WINDOW_WIDTH / 6;
    stopButton.y  = WINDOW_HEIGHT * 4 / 7;
    stopButton.w  = WINDOW_WIDTH * 2 / 3;
    stopButton.h  = WINDOW_HEIGHT * 2 / 7;
    
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("Generic SDL initialization failed\n");
        return 1;
    }
    
    window = SDL_CreateWindow("Mjolnir Server",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        printf("Failed to create window\n");
        SDLNet_Quit();
        SDL_Quit();
        return 1;
    }
    
    renderer = SDL_CreateSoftwareRenderer(SDL_GetWindowSurface(window));
    if (renderer == NULL)
    {
        printf("Failed to create renderer\n");
        SDL_DestroyWindow(window);
        SDLNet_Quit();
        SDL_Quit();
        return 1;
    }
    
    while (quit == 0)
    {
        SDL_Event e;
        
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_MOUSEBUTTONDOWN)
            {
                int x, y;
                
                SDL_GetMouseState(&x, &y);
                
                if (x >= startButton.x &&
                    x <= startButton.x + startButton.w &&
                    y >= startButton.y &&
                    y <= startButton.y + startButton.h)
                {
                    serverThread = SDL_CreateThread(server_thread, "", NULL);
                    if (serverThread == NULL)
                    {
                        printf("Server thread creation failed\n");
                    }
                }
                else if (x >= stopButton.x &&
                    x <= stopButton.x + stopButton.w &&
                    y >= stopButton.y &&
                    y <= stopButton.y + stopButton.h)
                {
                    listening = 0;
                    if (serverThread != NULL)
                    {
                        SDL_WaitThread(serverThread, NULL);
                    }
                    serverThread = NULL;
                }
            }
            else if (e.type == SDL_QUIT)
            {
                quit = 1;
            }
        }
        
        SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x00, 0xFF);
        SDL_RenderFillRect(renderer, &startButton);
        SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0x00, 0xFF);
        SDL_RenderFillRect(renderer, &stopButton);
        SDL_UpdateWindowSurface(window);
    }
    
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    return 0;
}
