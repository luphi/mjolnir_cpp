#include <cstddef> /* for NULL */

#include "HUD.hpp"

HUD::HUD()
{
    globals     = NULL;
    windSprite  = NULL;
    topBar      = NULL;
    bottomBar   = NULL;
    windArrow   = NULL;
    powerBar    = NULL;
    powerMarker = NULL;
}

HUD::~HUD()
{
    if (windSprite)
    {
        delete windSprite;
    }
    if (topBar)
    {
        delete topBar;
    }
    if (bottomBar)
    {
        delete bottomBar;
    }
    if (windArrow)
    {
        delete windArrow;
    }
    if (powerBar)
    {
        delete powerBar;
    }
    if (powerMarker)
    {
        delete powerMarker;
    }
}

bool HUD::init()
{
    globals = Globals::Instance();
    
    windSprite = new Sprite();
    
    if (windSprite->init("media/hud/wind.png"))
    {
        printf("Warning: HUD initialization failed\n");
    }
    
    topBar               = new Primitive();
    topBar->init(PRIMITIVE_QUAD);
    topBar->position.x   = 0.0f;
    topBar->position.y   = 0.0f;
    topBar->dimensions.x = globals->vidWidth;
    topBar->dimensions.y = globals->vidHeight * 0.05f;
    topBar->color        = glm::vec3(0.3f, 0.6f, 1.0f);
    
    bottomBar               = new Primitive();
    bottomBar->init(PRIMITIVE_QUAD);
    bottomBar->position.x   = 0.0f;
    bottomBar->position.y   = globals->vidHeight - globals->vidHeight * 0.05f;
    bottomBar->dimensions.x = globals->vidWidth;
    bottomBar->dimensions.y = globals->vidHeight * 0.05f;
    bottomBar->color        = glm::vec3(0.3f, 0.6f, 1.0f);

    return true;
}

void HUD::draw()
{
    topBar->draw();
    bottomBar->draw();
}
