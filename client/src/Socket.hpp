#ifndef _SOCKET_H_
#define _SOCKET_H_

#include <string>

#include <pthread.h>

#include "Event.hpp"

class Socket
{
public:

    Socket();
    ~Socket();
    bool join(std::string host, int port);
    void leave();
    void sendEvent(Event ev);

    int fd; /* socket file descriptor */

private:

    pthread_t       thread;
    pthread_mutex_t mutex;
    bool            receiving;
    
    static void* receiveThread(void* param); /* thread */
    static void  serialize(Event ev, char* buf, int* len);
    static void  unserialize(Event* ev, char* buf);

};

#endif
