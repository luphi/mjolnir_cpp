#include <cstddef> /* for NULL */

#include "Globals.hpp"

Globals* Globals::instance = NULL;

Globals* Globals::Instance()
{
	/* if no instance has been created yet */
	if (instance == NULL)
	{
		/* create the instance */
		instance = new Globals();
	}
	
	return instance;
}

Globals::Globals()
{
    window             = NULL;
    vidWidth           = 1280;
    vidHeight          = 720;
    mouse              = glm::vec2(0.0f);
    glContext          = NULL;
    glSpriteVShader    = 0;
    glSpriteFShader    = 0;
    glSpriteProgram    = 0;
    glPrimitiveVShader = 0;
    glPrimitiveFShader = 0;
    glPrimitiveProgram = 0;
    glUBO              = 0;
    projectionMatrix   = glm::mat4(1.0f);
    termVel            = 1000.0f; /* arbitrarily chosen */
    gAcc               = 25.0f; /* arbitrarily chosen */
    windAccX           = 0;
    windAccY           = 0;
    font               = NULL;
    for (int i = 0; i < 8; i++)
    {
		vikings[i]     = NULL;
	}
    terrain            = NULL;
    camera             = new Camera();
    terrain            = new Terrain();
    collisions         = new Collisions();
    fifo               = new EventFIFO();
    socket             = new Socket();
    input              = new Input();
    textInput          = new TextInput();
    strcpy(name, "Name");
    debug              = false;
}

Globals::~Globals()
{
	delete     camera;
	camera     = NULL;
	delete    terrain;
	terrain    = NULL;
	delete collisions;
	collisions = NULL;
	delete       fifo;
	fifo       = NULL;
	delete     socket;
	socket     = NULL;
	delete      input;
	input      = NULL;
	delete  textInput;
	textInput  = NULL;
}
