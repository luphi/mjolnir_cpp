#ifndef _SERVERSTATE_H_
#define _SERVERSTATE_H_

#include "State.hpp"

class ServerState : public State
{
public:

    ServerState();
    ~ServerState();
    
    void draw();
    void resize();
    void poll();
    void step(int dt);
    int  nextState();
    
    int stateChange;
};

#endif
