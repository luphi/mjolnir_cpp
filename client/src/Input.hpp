#ifndef _INPUT_H_
#define _INPUT_H_

enum InputCodes
{
    NO_INPUT,        RESIZE,
    CHAR_A_DOWN,     CHAR_A_UP,
    CHAR_B_DOWN,     CHAR_B_UP,
    CHAR_C_DOWN,     CHAR_C_UP,
    CHAR_D_DOWN,     CHAR_D_UP,
    CHAR_E_DOWN,     CHAR_E_UP,
    CHAR_F_DOWN,     CHAR_F_UP,
    CHAR_G_DOWN,     CHAR_G_UP,
    CHAR_H_DOWN,     CHAR_H_UP,
    CHAR_I_DOWN,     CHAR_I_UP,
    CHAR_J_DOWN,     CHAR_J_UP,
    CHAR_K_DOWN,     CHAR_K_UP,
    CHAR_L_DOWN,     CHAR_L_UP,
    CHAR_M_DOWN,     CHAR_M_UP,
    CHAR_N_DOWN,     CHAR_N_UP,
    CHAR_O_DOWN,     CHAR_O_UP,
    CHAR_P_DOWN,     CHAR_P_UP,
    CHAR_Q_DOWN,     CHAR_Q_UP,
    CHAR_R_DOWN,     CHAR_R_UP,
    CHAR_S_DOWN,     CHAR_S_UP,
    CHAR_T_DOWN,     CHAR_T_UP,
    CHAR_U_DOWN,     CHAR_U_UP,
    CHAR_V_DOWN,     CHAR_V_UP,
    CHAR_W_DOWN,     CHAR_W_UP,
    CHAR_X_DOWN,     CHAR_X_UP,
    CHAR_Y_DOWN,     CHAR_Y_UP,
    CHAR_Z_DOWN,     CHAR_Z_UP,
    BACKSPACE_DOWN,  BACKSPACE_UP,
    ENTER_DOWN,      ENTER_UP,
    ESCAPE_DOWN,     ESCAPE_UP,
    SHIFT_DOWN,      SHIFT_UP,
    RIGHT_DOWN,      RIGHT_UP,
    LEFT_DOWN,       LEFT_UP,
    UP_DOWN,         UP_UP,
    DOWN_DOWN,       DOWN_UP,
    SPACE_DOWN,      SPACE_UP,
    F1_DOWN,         F1_UP,
    F2_DOWN,         F2_UP,
    F3_DOWN,         F3_UP,
    F4_DOWN,         F4_UP,
    N1_DOWN,         N1_UP,
    N2_DOWN,         N2_UP,
    N3_DOWN,         N3_UP,
    TILDE_DOWN,      TILDE_UP,
    MOUSELEFT_DOWN,  MOUSELEFT_UP,
    MOUSERIGHT_DOWN, MOUSERIGHT_UP,
    MOUSEMOVED,      QUIT_EVENT
};

class Input
{
public:

    Input();
    ~Input();

    int poll();
};

#endif
