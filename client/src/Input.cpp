#include <SDL2/SDL.h>

#include "Globals.hpp"
#include "Input.hpp"

Input::Input()
{
    
}

Input::~Input()
{
    
}

int Input::poll()
{
    SDL_Event event;
    Globals*  globals;
    
    globals = Globals::Instance();
    if (SDL_PollEvent(&event) == 0)
    {
        return NO_INPUT;
    }
    
    if (event.type == SDL_QUIT)
    {
        return QUIT_EVENT;
    }
    else if (event.type == SDL_TEXTINPUT)
    {
        if (globals->textInput != NULL)
        {
            globals->textInput->addChars(event.text.text);
        }
    }
    else if (event.type == SDL_MOUSEMOTION)
    {
        globals->mouse.x = event.motion.x;
        globals->mouse.y = event.motion.y;
        return MOUSEMOVED;
    }
    else if (event.type == SDL_MOUSEBUTTONDOWN)
    {
        switch (event.button.button)
        {
            case SDL_BUTTON_LEFT:
                return MOUSELEFT_DOWN;
            case SDL_BUTTON_RIGHT:
                return MOUSERIGHT_DOWN;
            default:
                break;
        }
    }
    else if (event.type == SDL_MOUSEBUTTONUP)
    {
        switch (event.button.button)
        {
            case SDL_BUTTON_LEFT:
                return MOUSELEFT_UP;
            case SDL_BUTTON_RIGHT:
                return MOUSERIGHT_UP;
            default:
                break;
        }
    }
    else if (event.type == SDL_KEYDOWN)
    {
        switch (event.key.keysym.sym) {
            case SDLK_BACKSPACE:
                if (globals->textInput != NULL)
                {
                    globals->textInput->keyPress(BACKSPACE_DOWN);
                }
                return BACKSPACE_DOWN;
            case SDLK_RETURN:
                if (globals->textInput != NULL)
                {
                    globals->textInput->keyPress(ENTER_DOWN);
                }
                return ENTER_DOWN;
            case SDLK_ESCAPE:
                return ESCAPE_DOWN;
            case SDLK_LSHIFT:
            case SDLK_RSHIFT:
                return SHIFT_DOWN;
                break;
            case SDLK_LCTRL:
            case SDLK_RCTRL:
                break;
            case SDLK_RIGHT:
                if (globals->textInput != NULL)
                {
                    globals->textInput->keyPress(RIGHT_DOWN);
                }
                return RIGHT_DOWN;
            case SDLK_LEFT:
                if (globals->textInput != NULL)
                {
                    globals->textInput->keyPress(LEFT_DOWN);
                }
                return LEFT_DOWN;
            case SDLK_UP:
                if (globals->textInput != NULL)
                {
                    globals->textInput->keyPress(UP_DOWN);
                }
                return UP_DOWN;
            case SDLK_DOWN:
                return DOWN_DOWN;
            case SDLK_SPACE:
                return SPACE_DOWN;
            case SDLK_F1:
                return F1_DOWN;
            case SDLK_F2:
                return F2_DOWN;
            case SDLK_F3:
                return F3_DOWN;
            case SDLK_F4:
                return F4_DOWN;
            case SDLK_1:
                return N1_DOWN;
            case SDLK_2:
                return N2_DOWN;
            case SDLK_3:
                return N3_DOWN;
            case SDLK_BACKQUOTE:
                return TILDE_DOWN;
            default:
                break;
        }
    }
    else if (event.type == SDL_KEYUP)
    {
        switch (event.key.keysym.sym)
        {
            case SDLK_BACKSPACE:
                return BACKSPACE_UP;
            case SDLK_RETURN:
                return ENTER_UP;
            case SDLK_ESCAPE:
                return ESCAPE_UP;
            case SDLK_LSHIFT:
            case SDLK_RSHIFT:
                return SHIFT_UP;
                break;
            case SDLK_LCTRL:
            case SDLK_RCTRL:
                break;
            case SDLK_RIGHT:
                return RIGHT_UP;
            case SDLK_LEFT:
                return LEFT_UP;
            case SDLK_UP:
                return UP_UP;
            case SDLK_DOWN:
                return DOWN_UP;
            case SDLK_SPACE:
                return SPACE_UP;
            case SDLK_F1:
                return F1_UP;
            case SDLK_F2:
                return F2_UP;
            case SDLK_F3:
                return F3_UP;
            case SDLK_F4:
                return F4_UP;
            case SDLK_1:
                return N1_UP;
            case SDLK_2:
                return N2_UP;
            case SDLK_3:
                return N3_UP;
            case SDLK_BACKQUOTE:
                return TILDE_UP;
            default:
                break;
        }
    }
    else if (event.type == SDL_WINDOWEVENT)
    {
        switch (event.window.event)
        {
            case SDL_WINDOWEVENT_RESIZED:
            {
                Globals* globals   = Globals::Instance();
                globals->vidWidth  = event.window.data1;
                globals->vidHeight = event.window.data2;
                return RESIZE;
            }
            default:
                break;
        }
    }
    
    return NO_INPUT;
}
