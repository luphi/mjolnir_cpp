#include "ScoreState.hpp"

ScoreState::ScoreState()
{
    stateChange = NO_CHANGE;
}

ScoreState::~ScoreState()
{

}

void ScoreState::draw()
{

}

void ScoreState::resize()
{
    
}

void ScoreState::poll()
{

}

void ScoreState::step(int dt)
{

}

int ScoreState::nextState()
{
    return stateChange;
}
