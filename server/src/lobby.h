#ifndef _LOBBY_H_
#define _LOBBY_H_

/* additional libraries */
#include <pthread.h>

typedef struct lobby_t
{
    int             running;
    pthread_mutex_t mutex;
    pthread_t       thread;
} Lobby;

Lobby* lobby_create();
void   lobby_destroy(Lobby* lobby);
void   lobby_start(Lobby* lobby);
void   lobby_stop(Lobby* lobby);
void*  lobby_thread(void* param);

#endif
