#include <cstdio> /* for printf */
#include <cstring> /* for memcpy, strlen */

#include "Event.hpp"
#include "Globals.hpp"

EventFIFO::EventFIFO()
{
    pthread_mutex_init(&mutex, NULL);
}

EventFIFO::~EventFIFO()
{
	pthread_mutex_destroy(&mutex);
}

void EventFIFO::pushEvent(Event ev)
{
    pthread_mutex_lock(&mutex);
    fifo.push(ev);
    pthread_mutex_unlock(&mutex);
}

Event EventFIFO::popEvent()
{
    Event ev;
    
    ev.op = NO_OP;
    
    pthread_mutex_lock(&mutex);
    if (fifo.empty() == false)
    {
        ev = fifo.front();
        fifo.pop();
    }
    pthread_mutex_unlock(&mutex);
    
    return ev;
}

/******************************************************************************/

void EventFIFO::packPong(Event* ev)
{
	(*ev).op  = PONG;
	(*ev).id  = 0;
	(*ev).len = 0;
	memset((*ev).buf, 0, 512);
}

void EventFIFO::packVikingName(Event* ev, uint16_t id, char* name)
{
    uint16_t len;
	
	/* buffer size limits strings to 255 characters */
	len = strlen(name);
	if (len > 255)
	{
		len = 255;
	}
	
	(*ev).op  = VIKING_NAME;
	(*ev).id  = id;
	(*ev).len = len;
	memset((*ev).buf, 0, 512);
	memcpy((*ev).buf, name, len);
}

void EventFIFO::packVikingChoice(Event* ev, uint16_t id, uint16_t choice)
{
    (*ev).op  = VIKING_CHOICE;
	(*ev).id  = id;
	(*ev).len = sizeof(uint16_t);
	memset((*ev).buf, 0, 512);
    memcpy((*ev).buf, &choice, sizeof(uint16_t));
}

void EventFIFO::packWeaponSubtype(Event* ev, uint16_t id, uint16_t subtype)
{
    (*ev).op  = WEAPON_SUBTYPE;
	(*ev).id  = id;
	(*ev).len = sizeof(uint16_t);
	memset((*ev).buf, 0, 512);
    memcpy((*ev).buf, &subtype, sizeof(uint16_t));
}

void EventFIFO::packMessageAll(Event* ev, uint16_t id, char* message)
{
    uint16_t len;
	
	/* buffer size limits strings to 255 characters */
	len = strlen(message);
	if (len > 255)
	{
		len = 255;
	}
	
	(*ev).op  = MESSAGE_ALL;
	(*ev).id  = id;
	(*ev).len = len;
	memset((*ev).buf, 0, 512);
	memcpy((*ev).buf, message, len);
}

void EventFIFO::packMessageTeam(Event* ev, uint16_t id, char* message)
{
    uint16_t len;
	
	/* buffer size limits strings to 255 characters */
	len = strlen(message);
	if (len > 255)
	{
		len = 255;
	}
	
	(*ev).op  = MESSAGE_TEAM;
	(*ev).id  = id;
	(*ev).len = len;
	memset((*ev).buf, 0, 512);
	memcpy((*ev).buf, message, len);
}

/******************************************************************************/
