#ifndef _GLOBALS_H_
#define _GLOBALS_H_

/* additional libraries */
#include <pthread.h>

#include "event.h"
#include "log.h"
#include "net.h"
#include "viking.h"
#include "state.h"
#include "ui.h"

typedef struct globals_t
{
    Log*            log;
    
    int             state;
    pthread_mutex_t state_mutex;
    
    Viking*         vikings[8];
    int             viking_count;
    int             viking_max;
    pthread_mutex_t vikings_mutex;

    UI*             ui;

    ServerSocket*   server_socket;
    EventFIFO*      event_fifo;
    int             server_running;
} Globals;

#endif
