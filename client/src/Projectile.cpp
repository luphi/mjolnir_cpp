#include <cmath> /* for atan2 */
#include <cstddef> /* for NULL */
#include <cstdio> /* for printf */

#include "Globals.hpp"
#include "Projectile.hpp"

Projectile::Projectile()
{
    physical = NULL;
    sprite   = NULL;
    circle   = NULL;
    active   = false;
}

Projectile::~Projectile()
{
    if (physical != NULL)
    {
        delete physical;
    }
    if (sprite != NULL)
    {
        delete sprite;
    }
    if (circle != NULL)
    {
        delete circle;
    }
}

void Projectile::step(int dt)
{
    Globals* globals;
    float    x; /* the old X */
    float    y; /* the old y */
    
    if (physical == NULL || active == false)
    {
        return;
    }
    
    globals = Globals::Instance();
    x       = physical->x;
    y       = physical->y;
    physical->step(dt);
    
    if (globals->collisions->areCircleTerrainColliding(circle))
    {
        glm::vec2 point;
        
        point.x = x + circle->r;
        point.y = y + circle->r;
        
        /* globals->fifo->pushEvent(EXPLOSION, point.x, point.y, 25); */
        active = false;
#if 0
        glm::vec2 point;
        glm::vec2 velocity;
        
        /* because projectiles move in steps and the collision detection    */
        /* used so far does not determine the exact location of the         */
        /* collision, we're going to do a raytrace from the center of the   */
        /* projectile to the terrain in the direction of the projectile's   */
        /* movement in order to find an exact pixel it would hit and create */
        /* a hole at that location                                          */
        point.x    = x + circle->r;
        point.y    = y + circle->r;
        velocity.x = physical->xVel;
        velocity.y = physical->yVel;
        if (globals->collisions->raycastTerrain(point, velocity))
        {
            /* the parameter, point, is passed by reference and is now the */
            /* point of collision rather than the starting point           */
            /* globals->fifo->pushEvent(EXPLOSION, point.x, point.y, 25); */
        }
        else
        {
            /* the raytrace did not collide with the terrain so we'll make */
            /* our best guess and create a hole at the center point        */
            /* globals->fifo->pushEvent(EXPLOSION, point.x, point.y, 25); */
        }
        
        active = false;
#endif
    }
    
    sprite->position.x = physical->x;
    sprite->position.y = physical->y;
    circle->position.x = physical->x;
    circle->position.y = physical->y;
    position           = sprite->position;
    dimensions         = sprite->dimensions;
}

void Projectile::draw()
{
    if ((sprite) == NULL || (active == false))
    {
        return;
    }
    
    sprite->draw();
    
    if (Globals::Instance()->debug)
    {
        circle->draw();
    }
}

/*******************************************************************************
 *                          ProjectileBigfoot1
 ******************************************************************************/

bool ProjectileBigfoot1::init()
{
    sprite   = new Sprite();
    physical = new Physical();
    circle   = new Circle();
    if (sprite->init("media/players/bigfoot/projectile_1.png") == false)
    {
        printf("Bigfoot projectile sprite load failed\n");
        return false;
    }
    circle->init(sprite->dimensions.x / 2.0f);
    
    return true;
}

void ProjectileBigfoot1::step(int dt)
{
    Projectile::step(dt);
    
    sprite->glRotation = atan2(physical->yVel, physical->xVel) +
        3.14159f / 2.0f;
}

/*******************************************************************************
 *                          ProjectileBigfoot2
 ******************************************************************************/
 
bool ProjectileBigfoot2::init()
{
    sprite   = new Sprite();
    physical = new Physical();
    circle   = new Circle();
    if (sprite->init("media/bomb.png") == false)
    {
        printf("Bigfoot projectile sprite load failed\n");
        return false;
    }
    circle->init(sprite->dimensions.x / 2.0f);
    
    return true;
}
