/* C standard library */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Mjolnir server stuff */
#include "event.h"
#include "globals.h"
#include "log.h"
#include "net.h"
#include "ui.h"

extern Globals* g;

EventFIFO* fifo_create()
{
    EventFIFO* ef;
    
    ef          = (EventFIFO*) malloc(sizeof(EventFIFO));
    ef->first   = NULL;
    ef->last    = NULL;
    pthread_mutex_init(&ef->mutex, NULL);
    
    return ef;
}

void fifo_destroy(EventFIFO* fifo)
{
    EventNode* curr;
    EventNode* next;
    
    /* error case */
    if (fifo == NULL)
    {
        return;
    }
    
    curr = fifo->first;
    while (curr != NULL)
    {
        next = curr->next;
        free(curr);
        curr = next;
    }
    
    pthread_mutex_destroy(&fifo->mutex);
    free(fifo);
}

void fifo_push_event(EventFIFO* fifo, Event event)
{
    EventNode* node;
    
    /* error case */
    if (fifo == NULL)
    {
        return;
    }
    
    node        = (EventNode*) malloc(sizeof(EventNode));
    node->event = event;
    node->next  = NULL;
    
    pthread_mutex_lock(&fifo->mutex);
    if (fifo->first == NULL)
    {
        fifo->first = node;
        fifo->last  = node;
    }
    else /* fifo->first != NULL */
    {
        fifo->last->next = node;
        fifo->last       = node;
    }
    pthread_mutex_unlock(&fifo->mutex);
}

Event fifo_pop_event(EventFIFO* fifo)
{
    Event      ev;
    EventNode* node;
    
    /* if there are no pending events, or error case */
    if ((fifo == NULL) || (fifo->first == NULL))
    {
        /* return a "no operation" */
        ev.op = NO_OP;
        return ev;
    }
    
    pthread_mutex_lock(&fifo->mutex);
    ev          = fifo->first->event;
    node        = fifo->first;
    fifo->first = fifo->first->next;
    free(node);
    pthread_mutex_unlock(&fifo->mutex);
    
    return ev;
}

/******************************************************************************/

void event_pack_ping(Event* ev)
{
	log_enter(g->log, __func__);
	
	(*ev).op  = PING;
	(*ev).id  = 0;
	(*ev).len = 0;
	memset((*ev).buf, 0, 512);
	
	log_exit(g->log, __func__);
}

void event_pack_viking_connect(Event* ev, uint16_t id)
{
	log_enter(g->log, __func__);
	
	(*ev).op  = VIKING_CONNECT;
	(*ev).id  = id;
	(*ev).len = 0;
	memset((*ev).buf, 0, 512);
	
	log_exit(g->log, __func__);
}

void event_pack_viking_name(Event* ev, uint16_t id, char* name)
{
	uint16_t len;
	
	log_enter(g->log, __func__);
	
	/* buffer size limits strings to 256 characters */
	len = strlen(name);
	if (len > 256)
	{
		len = 256;
	}
	
	(*ev).op  = VIKING_NAME;
	(*ev).id  = id;
	(*ev).len = len;
	memset((*ev).buf, 0, 512);
	memcpy((*ev).buf, name, len);
	
	log_exit(g->log, __func__);
}

/******************************************************************************/

void event_unpack_pong(Event ev, uint16_t* id)
{
	log_enter(g->log, __func__);
	
	*id = ev.id;
	
	log_exit(g->log, __func__);
}

void event_unpack_viking_name(Event ev, uint16_t* id, char* name)
{
	log_enter(g->log, __func__);
	
	*id = ev.id;	
	memcpy(name, ev.buf, ev.len);
	
	log_exit(g->log, __func__);
}

void event_unpack_viking_choice(Event ev, uint16_t* id, uint16_t* choice)
{
	log_enter(g->log, __func__);
	
	*id     = ev.id;	
	*choice = (uint16_t) ev.buf[0];
	
	log_exit(g->log, __func__);
}

void event_unpack_weapon_subtype(Event ev, uint16_t* id, uint16_t* subtype)
{
	log_enter(g->log, __func__);
	
	*id      = ev.id;	
	*subtype = (uint16_t) ev.buf[0];
	
	log_exit(g->log, __func__);
}

void event_unpack_message_all(Event ev, uint16_t* id, char* message)
{
	log_enter(g->log, __func__);
	
	*id = ev.id;	
	memcpy(message, ev.buf, ev.len);
	
	log_exit(g->log, __func__);
}

void event_unpack_message_team(Event ev, uint16_t* id, char* message)
{
	log_enter(g->log, __func__);
	
	*id = ev.id;	
	memcpy(message, ev.buf, ev.len);
	
	log_exit(g->log, __func__);
}
