#ifndef _STATE_H_
#define _STATE_H_

enum StateChanges
{
    NO_CHANGE,
    MENU_STATE,
    SERVER_STATE,
    LOBBY_STATE,
    GAME_STATE,
    SCORE_STATE,
    DEV_STATE,
    OPTIONS_STATE,
    EXIT
};

class State
{
public:

    virtual ~State() { }
    
    virtual void draw()       = 0;
    virtual void resize()     = 0;
    virtual void poll()       = 0;
    virtual void step(int dt) = 0;
    virtual int  nextState()  = 0;
};

#endif
