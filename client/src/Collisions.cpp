#include <cmath> /* for sqrt */
#include <cstddef> /* for NULL */

#include <glm/glm.hpp>

#include "Collisions.hpp"
#include "Globals.hpp"

Collisions::Collisions()
{
    
}

Collisions::~Collisions()
{
    
}

bool Collisions::isPointInDrawable(glm::vec2 point, Drawable* draw)
{
    /* error case */
    if (draw == NULL)
    {
        return false;
    }
    
    if (point.x < draw->position.x)
    {
        return false;
    }
    
    if (point.x > draw->position.x + draw->dimensions.x)
    {
        return false;
    }
    
    if (point.y < draw->position.y)
    {
        return false;
    }
    
    if (point.y > draw->position.y + draw->dimensions.y)
    {
        return false;
    }
    
    return true;
}

bool Collisions::isVikingOnTerrain(Viking* viking)
{
    Terrain* terrain;
    
    terrain = Globals::Instance()->terrain;
    
    /* error case */
    if (terrain == NULL)
    {
        return false;
    }
    
    /* we'll check a point at the center of the bottom border for a collision */
    if (terrain->isTransparent(viking->sprite->position.x +
                               viking->sprite->dimensions.x / 2,
                               viking->sprite->position.y +
                               viking->sprite->dimensions.y) == false)
    {
        return true;
    }
    
    return false;
}

bool Collisions::areVikingTerrainColliding(Viking* viking)
{
    Terrain* terrain;
    
    terrain = Globals::Instance()->terrain;
    
    /* error cases */
    if ((viking == NULL) || (terrain == NULL))
    {
        return false;
    }
    
    /* four points will be tested for collision with the terrain */
    /* points run down the middle of the viking, distanced one fifth of */
    /* the viking's height apart */
    for (int i = 0; i < 4; i++)
    {
        /* if one of the points is solid */
        if (!terrain->isTransparent(viking->sprite->position.x +
                                    viking->sprite->dimensions.x / 2.0f,
                                    viking->sprite->position.y +
                                    i * viking->sprite->dimensions.y / 5.0f))
        {
            /* the viking is colliding with the terrain */
            return true;
        }
    }
    
    return false;
}

bool Collisions::areCircleTerrainColliding(Circle* circle)
{
    Terrain* terrain;
    int      x;
    int      y;
    int      r;
    int      cx; /* center of circle, X coordinate */
    int      cy; /* center of circle, Y coordinate */
    
    terrain = Globals::Instance()->terrain;
    
    /* error cases */
    if ((circle == NULL) || (terrain == NULL))
    {
        return false;
    }
    
    x       = (int) circle->position.x;
    y       = (int) circle->position.y;
    r       = (int) circle->r;
    cx      = x + r;
    cy      = y + r;
    
    /* cycle through the pixels surrounding the circle */
    for (int i = x; i <= x + r * 2; i++)
    {
        for (int j = y; j <= y + r * 2; j++)
        {
            /* if the pixel is solid and within the circle */
            /* the second condition is simply the pythagorean theorem */
            if (!terrain->isTransparent(i, j) &&
                (cx - i) * (cx - i) + (cy - j) * (cy - j) <= r * r)
            {
                return true;
            }
        }
    }
    
    return false;
}

bool Collisions::raycastTerrain(glm::vec2& point, glm::vec2 ray)
{
    Terrain* terrain;
    int      x, y, dx, sx, dy, sy, err, derr;
    
    /* this is basically Bresenham's line algorithm */
    terrain = Globals::Instance()->terrain;
    x       = (int) point.x;
    y       = (int) point.y;
    dx      = (int) abs((int) ray.x);
    dy      = (int) abs((int) ray.y);
    sx      = (ray.x < 0 ? -1 : 1);
    sy      = (ray.y < 0 ? -1 : 1);
    err     = 0;
    derr    = 0;
    
    /* error case */
    if (terrain == NULL)
    {
        return false;
    }
    
    /* while x and y are within the terrain */
    while (x >= 0                     && y >= 0                  &&
           x <= terrain->dimensions.x && y <= terrain->dimensions.y)
    {
        /* if the pixel is solid, we've collided with the terrain */
        if (!terrain->isTransparent(x, y))
        {
            point.x = x;
            point.y = y;
            return true;
        }
        
        derr = err;
        if (derr > -dx)
        {
            err -= dy;
            x   += sx;
        }
        
        if (derr <  dy)
        {
            err += dx;
            y   += sy;
        }
    }
    
    /* if we reached here, the ray does not collide with the terrain */
    return false;
}

bool Collisions::areDrawablesColliding(Drawable* one, Drawable* two)
{
    /* error cases */
    if ((one == NULL) || (two == NULL))
    {
        return false;
    }
    
    if (one->position.x + one->dimensions.x < two->position.x)
    {
        return false;
    }
    if (one->position.x > two->position.x + two->dimensions.x)
    {
        return false;
    }
    if (one->position.y + one->dimensions.y < two->position.y)
    {
        return false;
    }
    if (one->position.y > two->position.y + two->dimensions.y)
    {
        return false;
    }
    
    return true;
}
