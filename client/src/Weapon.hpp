#ifndef _WEAPON_H_
#define _WEAPON_H_

#include <list>

#include "Drawable.hpp"
#include "Projectile.hpp"

class Weapon : public Drawable
{
public:
    
    Weapon();
    ~Weapon();
    
    virtual void step(int dt);
    void draw();

    virtual bool init() { return true; }
    virtual void type(int t);
    virtual void fire(float x, float y, float radians, int power) { }

    bool      active;
    int       damage;
    Drawable* focus; /* where the camera should look */

    /* inherited from Drawable */
    /* glm::vec2 position;       X and Y coordinates */
    /* glm::vec2 dimensions;     width and height */

protected:

    std::list<Projectile*> projs;
    int                    subtype; /* 1, 2, or 3 */
};

class WeaponArmour : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponMage : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponNakmachine : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponTrico : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponBigfoot : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
private:
    int                              ms;
    std::list<Projectile*>::iterator bfIt;
};

class WeaponBoomer : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponRaonlauncher : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponLightning : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponJd : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponAsate : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponIce : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponTurtle : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponGrub : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponAduka : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponKalsiddon : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

class WeaponJfrog : public Weapon
{
public:
    void step(int dt);
    bool init();
    void fire(float x, float y, float radians, int power);
};

#endif
