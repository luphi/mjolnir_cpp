#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "Drawable.hpp"

class Camera
{
public:

    Camera();
    ~Camera();
    
    void lookAt(Drawable* draw);
    void resize();
    void identityView();
};

#endif
