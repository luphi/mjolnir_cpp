#include "Globals.hpp"
#include "ServerState.hpp"

ServerState::ServerState()
{
    stateChange = LOBBY_STATE;
}

ServerState::~ServerState()
{

}

void ServerState::draw()
{

}

void ServerState::resize()
{
    
}

void ServerState::poll()
{

}

void ServerState::step(int dt)
{

}

int ServerState::nextState()
{
    return stateChange;
}
