#ifndef _NET_H_
#define _NET_H_

/* additional libraries */
#include <pthread.h>

/* Mjolnir server stuff */
#include "event.h"
#include "viking.h"

extern EventFIFO* event_fifo;

typedef struct ServerSocket_t
{
    fd_set          socket_set;
    int             socket_listener;
    int             running;
    pthread_t       thread;
    pthread_mutex_t mutex;
} ServerSocket;

ServerSocket* net_server_socket_create();
void          net_server_socket_destroy(ServerSocket* server);
void          net_server_socket_close(ServerSocket* server);
int           net_server_socket_start(ServerSocket* server, char* port);
void          net_server_socket_stop(ServerSocket* server);

void          net_client_socket_close(Viking* viking);
void          net_send_event(Viking* viking, Event event);
void          net_send_event_all(Event event);
void          net_send_event_all_but(Event event, int id_to_exclude);

void*         net_server_thread(void* param);

#endif
