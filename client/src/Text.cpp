#include <cstddef> /* for NULL */

#include "Globals.hpp"
#include "Text.hpp"

Text::Text()
{
    position   = glm::vec2(0.0f);
    dimensions = glm::vec2(0.0f);
    font       = NULL;
    color.r    = 255;
    color.g    = 255;
    color.b    = 255;
    color.a    = 255;
    str        = "";
}

Text::~Text()
{
    
}

void Text::draw()
{
    /* updateTexture() will fail on an empty string so, as an alernative to */
    /* creating a sprite for no text, just leave it as is and not draw at all */
    
    /* if there's text */
    if (str.size() > 0)
    {
        /* draw the text */
        Sprite::draw();
    }
}

void Text::init(std::string text, int r, int g, int b)
{
    GLfloat glVertices[] = {
        /* position */   /* texcoords */
        0.5f,  0.5f,     1.0f, 1.0f, /* top right */
        0.5f, -0.5f,     1.0f, 0.0f, /* bottom right */
       -0.5f, -0.5f,     0.0f, 0.0f, /* bottom left */
       -0.5f,  0.5f,     0.0f, 1.0f  /* top left  */
    };
    
    GLuint glIndices[] = {
        0, 1, 3,
        1, 2, 3
    };
    
    str     = text;
    color.r = r;
    color.g = g;
    color.b = b;
    
    glGenVertexArrays(1, &glVAO);
    glBindVertexArray(glVAO);

    glGenBuffers(1, &glVBO);
    glBindBuffer(GL_ARRAY_BUFFER, glVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glVertices), glVertices,
        GL_STATIC_DRAW);
    
    glGenBuffers(1, &glEBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glIndices), glIndices,
        GL_STATIC_DRAW);
        
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat),
        (GLvoid*) (2 * sizeof(GLfloat)));

    glGenTextures(1, &glTex);
    glBindTexture(GL_TEXTURE_2D, glTex);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    updateTexture();
}

void Text::setText(std::string text)
{
    str = text;
    updateTexture();
}

void Text::updateTexture()
{
    Globals*     globals;
    SDL_Surface* initial;
    SDL_Surface* surface;
    
    globals = Globals::Instance();
    initial = NULL;
    surface = NULL;
    initial = TTF_RenderText_Blended(globals->font, str.c_str(), color);
    
    if (initial == NULL)
    {
        return;
    }
    
    surface = SDL_CreateRGBSurface(0, initial->w, initial->h, 32,    
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    	0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff); /* RGBA */
#else
        0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000); /* also RGBA */
#endif
    if (surface == NULL)
    {
        return;
    }
	SDL_BlitSurface(initial, 0, surface, 0);

    dimensions = glm::vec2((float) surface->w, (float) surface->h);
    position   = glm::vec2(0.0f, 0.0f);
    
    glBindVertexArray(glVAO);
    glBindBuffer(GL_ARRAY_BUFFER, glVBO);
    glBindTexture(GL_TEXTURE_2D, glTex);
    
    /* TODO: free the old texture */
    /* glDeleteTextures(1, &glTex); */
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA,
        GL_UNSIGNED_BYTE, surface->pixels);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    SDL_FreeSurface(initial);
    SDL_FreeSurface(surface);
}
