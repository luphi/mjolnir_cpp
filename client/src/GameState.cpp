#include <cmath> /* for atan2, cos, sin, sqrt */
#include <cstddef> /* for NULL */

#include "GameState.hpp"
#include "Input.hpp"

/* used for readability when calculating a viking's orientation */
#define LEFT   0
#define RIGHT  1
#define CENTER 2

GameState::GameState()
{
	globals    = NULL;
	fifo       = NULL;
	camera     = NULL;
	terrain    = NULL;
	collisions = NULL;
	socket     = NULL;
	input      = NULL;
	textInput  = NULL;
	
    globals    = Globals::Instance();
    fifo       = globals->fifo;    
    camera     = globals->camera;
    terrain    = globals->terrain;
    collisions = globals->collisions;
    socket     = globals->socket;
    input      = globals->input;
    textInput  = globals->textInput;
    
    hud  = new HUD();
    text = new Text();
    
    hud->init();
    terrain->init("2r2m0cg.txt");
    
    textInput->position.x = 100.0f;
    textInput->position.y = 200.0f;
    
    stateChange = NO_CHANGE;
    
    viking1     = NULL;
    onTerrain   = false;
    movingRight = false;
    movingLeft  = false;
}

GameState::~GameState()
{
    delete  hud;
    hud  = NULL;
    delete text;
    text = NULL;
}

void GameState::draw()
{
    std::list<Viking*>::iterator it;
    
    if (viking1 != NULL)
    {
        if ((viking1->weapon->active) && (viking1->weapon->focus != NULL))
        {
            camera->lookAt(viking1->weapon->focus);
        }
        else
        {
            camera->lookAt(viking1->sprite);
        }
    }
    
    terrain->draw();
    
    for (it = vikings.begin(); it != vikings.end(); it++)
    {
        (*it)->draw();
    }
    
    camera->identityView();
    hud->draw();
    textInput->draw();
}

void GameState::resize()
{
	
}

void GameState::poll()
{
    int code;
    
    if (input == NULL)
    {
        return;
    }
    
    while ((code = input->poll()) != NO_INPUT)
    {
        switch (code)
        {
            case RESIZE:
                camera->resize();
                break;
            case ESCAPE_DOWN:
                stateChange = MENU_STATE;
                break;
            case ESCAPE_UP:
                break;
            case RIGHT_DOWN:
                movingRight = true;
                movingLeft  = false;
                viking1->face(RIGHT);
                break;
            case RIGHT_UP:
                movingRight = false;
                break;
            case LEFT_DOWN:
                movingRight = false;
                movingLeft  = true;
                viking1->face(LEFT);
                break;
            case LEFT_UP:
                movingLeft  = false;
                break;
            case UP_DOWN:
                break;
            case UP_UP:
                break;
            case DOWN_DOWN:
                break;
            case DOWN_UP:
                break;
            case SPACE_DOWN:
                if (viking1 != NULL)
                {
                    viking1->fire();
                }
                break;
            case SPACE_UP:
                break;
            case F1_DOWN:
                globals->debug = !globals->debug;
                break;
            case N1_DOWN:
                if (viking1 != NULL)
                {
                    viking1->weapon->type(1);
                }
                break;
            case N2_DOWN:
                if (viking1 != NULL)
                {
                    viking1->weapon->type(2);
                }
                break;
            case N3_DOWN:
                if (viking1 != NULL)
                {
                    viking1->weapon->type(3);
                }
                break;
        }
    }
}

void GameState::step(int dt)
{
    std::list<Viking*>::iterator it;
    float                        oldX;
    float                        oldY;
    
    handleEvents();
    
    if (viking1 == NULL)
    {
        return;
    }
    
    oldX    = viking1->physical->x;
    oldY    = viking1->physical->y;
    
    if (movingRight)
    {
        viking1->physical->xVel =  100.0f;
    }
    else if (movingLeft)
    {
        viking1->physical->xVel = -100.0f;
    }
    else /* !movingRight && !movingLeft */
    {
        viking1->physical->xVel =  0.0f;
    }
    
    for (it = vikings.begin(); it != vikings.end(); it++)
    {
        (*it)->step(dt);
    }
    
    /* constrain the viking to within the bounds of the terrain */
    if (viking1->sprite->position.x < terrain->position.x)
    {
        viking1->move(terrain->position.x, viking1->sprite->position.y);
    }
    else if (viking1->sprite->position.x + viking1->sprite->dimensions.x >
             terrain->position.x + terrain->dimensions.x)
    {
        viking1->move(terrain->position.x + terrain->dimensions.x -
                      viking1->sprite->dimensions.x,
                      viking1->sprite->position.y);
    }
    
    if (onTerrain)
    {
        int  dmax; /* allowable movement in the Y direction, up or down */
        int  dy; /* amount moved up or down, if applied */
        bool revert; /* if true, revert the viking to its old location */
        bool fall; /* if true, indicates the viking is now falling */
        
        dmax   = 15; /* magic number; yes, I know it's evil */
        dy     = 0;
        revert = false;
        fall   = false;
        
        /* if climbing a hill or moving on flat ground*/
        if (collisions->isVikingOnTerrain(viking1))
        {
            /* move up to walk uphill or stop when we've tried hard enough */
            while (dy < dmax && collisions->isVikingOnTerrain(viking1))
            {
                viking1->translate(0.0f, -1.0f);
                dy += 1;
            }
            
            if (dy < dmax)
            {
                /* move down one pixel so we're just touching the terrain */
                viking1->translate(0.0f, 1.0f);
            }
            else
            {
                /* went too far; viking1 can't move */
                revert = true;
            }
        }
        /* if walking downhill or falling */
        else
        {
            /* continually shift the viking down until we hit the terrain */
            while (dy < dmax && !collisions->isVikingOnTerrain(viking1))
            {
                viking1->translate(0.0f, 1.0f);
                dy += 1;
            }
            
            if (dy >= dmax)
            {
                /* went too far; viking went over a cliff (or steep hill) */
                fall = true;
            }
        }
        
        /* if the viking1 collided with an overhang or other obstacle */
        if (collisions->areVikingTerrainColliding(viking1))
        {
            revert = true;
        }
        
        /* could not move for one reason or another, so move back */
        if (revert)
        {
            viking1->move(oldX, oldY);
        }
        
        /* the terrain has disappeared from beneath the viking1 or they fell */
        if (fall)
        {
            viking1->orientation(3.14159f / 2.0f); /* standing straight up */
            viking1->unfreeze();
            onTerrain = false;
        }
        
        /* if the movement is unobstructed and we're still on the terrain */
        if (!revert && !fall)
        {
            /* now we need to figure out the viking's orientation */
            glm::vec2 points[3]; /* collision points, explained below */
            glm::vec2 slope; /* the slope to the left or right */
            float     radians; /* radians of the slope */
            bool      col[2]; /* indicates if a point is on the terrain */
            float     y[2]; /* relative Y values */
            
            points[LEFT].x   = viking1->sprite->position.x +
                               viking1->sprite->dimensions.x * 0.35f;
            points[LEFT].y   = viking1->sprite->position.y +
                               viking1->sprite->dimensions.y;
            points[RIGHT].x  = viking1->sprite->position.x +
                               viking1->sprite->dimensions.x * 0.65f;
            points[RIGHT].y  = viking1->sprite->position.y +
                               viking1->sprite->dimensions.y;
            points[CENTER].x = viking1->sprite->position.x +
                               viking1->sprite->dimensions.x * 0.5f;
            points[CENTER].y = viking1->sprite->position.y +
                               viking1->sprite->dimensions.y;
            slope            = glm::vec2(0.0f);
            radians          = 0.0f;
            col[LEFT]        = false;
            col[RIGHT]       = false;
            y[LEFT]          = 0.0f;
            y[RIGHT]         = 0.0f;
            
            /* check <dmax> pixels above and below from collision point */
            for (float j = (float) -dmax + 1; j < (float) dmax; j += 1.0f)
            {
                /* if pixel is solid */
                if (!terrain->isTransparent(points[LEFT].x,
                                            points[LEFT].y + j))
                {
                    col[LEFT] = true;
                    y[LEFT]   = j;
                    j         = dmax; /* exit the inner loop */
                }
            }
            for (float j = (float) -dmax + 1; j < (float) dmax; j += 1.0f)
            {
                /* if pixel is solid */
                if (!terrain->isTransparent(points[RIGHT].x,
                                            points[RIGHT].y + j))
                {
                    col[RIGHT] = true;
                    y[RIGHT]   = j;
                    j          = dmax; /* exit the inner loop */
                }
            }
            
            
            /* if both points to the left and right are valid */
            if (col[LEFT] && col[RIGHT])
            {
                /* we will take the slope of the left and right points to */
                /* determine the orientation of the viking */
                glm::vec2 p[2];
                
                /* make copies of the collision points with Y adjustments */
                p[0] = glm::vec2(points[LEFT].x,  points[LEFT].y  + y[LEFT]);
                p[1] = glm::vec2(points[RIGHT].x, points[RIGHT].y + y[RIGHT]);
                
                /* this slope is the slope of two of the collision points */
                /* pointing leftward */
                slope    = p[1] - p[0];
                radians  = atan2(slope.y, slope.x);
                radians += 3.14159f / 2.0f; /* pi / 2 */
            }
            /* if neither left nor right points are valid */
            else if (!col[LEFT] && !col[RIGHT])
            {
                /* we'll use the terrain object's averaging method to get an */
                /* approximate slope of the terrain at the center point */
                slope   = terrain->getNormalAt(points[CENTER].x,
                                               points[CENTER].y + 1.0f);
                radians = atan2(slope.y, slope.x);
            }
            /* if just the left or just the right point is valid */
            else
            {
                glm::vec2 p[2];
                
                if (col[LEFT])
                {
                    /* the left collision point is valid */
                    p[0] = glm::vec2(points[LEFT].x,
                                     points[LEFT].y + y[LEFT]);
                    p[1] = points[CENTER];
                }
                else /* col[RIGHT] */
                {
                    /* the right collision point is valid */
                    p[0] = points[CENTER];
                    p[1] = glm::vec2(points[RIGHT].x,
                                     points[RIGHT].y + y[RIGHT]);
                }
                
                slope    = p[1] - p[0];
                radians  = atan2(slope.y, slope.x);
                radians += 3.14159f / 2.0f;
            }
            
            viking1->orientation(radians);
        }
    }
    else /* !onTerrain, meaning the viking was/is falling */
    {
        /* if the viking landed on the terrain just now */
        if (collisions->isVikingOnTerrain(viking1))
        {
            /* shift the viking up in case we penetrated too far */
            do
            {
                viking1->translate(0.0f, -1.0f);
            } while (collisions->isVikingOnTerrain(viking1));
            viking1->translate(0.0f, 1.0f);
            
            viking1->freeze();
            onTerrain = true;
        }
    }
}

int GameState::nextState()
{
    return stateChange;
}

void GameState::handleEvents()
{
    Event ev;
    
    if (fifo == NULL)
    {
        return;
    }
    
    do
    {
        ev = fifo->popEvent();
        
        switch (ev.op)
        {
            default:
                break;
        }
    }
    while (ev.op != NO_OP);
}
