#ifndef _HUD_H_
#define _HUD_H_

#include "Drawable.hpp"
#include "Globals.hpp"
#include "Primitive.hpp"
#include "Sprite.hpp"
#include "Text.hpp"

class HUD : public Drawable
{
public:

    HUD();
    ~HUD();
    
    bool init();
    void draw();
    
    /* inherited from Drawable */
    /* glm::vec2 position;       X and Y coordinates */
    /* glm::vec2 dimensions;     width and height */
    
private:

    Globals*   globals;
    
    Sprite*    windSprite;
    
    Primitive* topBar;
    Primitive* bottomBar;
    Primitive* windArrow;
    Primitive* powerBar;
    Primitive* powerMarker;
};

#endif
