#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Circle.hpp"
#include "Drawable.hpp"
#include "Physical.hpp"
#include "Sprite.hpp"
#include "Weapon.hpp"

enum VikingChoices
{
    ARMOUR,
    MAGE,
    NAKMACHINE,
    TRICO,
    BIGFOOT,
    BOOMER,
    RAONLAUNCHER,
    LIGHTNING,
    JD,
    ASATE,
    ICE,
    TURTLE,
    GRUB,
    ADUKA,
    KALSIDDON,
    JFROG
};

enum Directions
{
    LEFT,
    RIGHT
};

class Viking : public Drawable
{
public:

    Viking();
    ~Viking();
    
    bool init(int mobile);
    void step(int dt);
    void draw();
    
    void move(float x, float y);
    void translate(float dx, float dy);
    void orientation(float radians);
    void face(int dir);
    void freeze();
    void unfreeze();
    void fire();
    void hurt(int damage);
    
    int       id; /* unique identifier */
    char      name[24]; /* name chosen by the user */
    int       health; /* remaining health */
    int       shield; /* current shield health (MAGE, LIGHTING, JD, ASATE) */
    int       maxShield; /* maximum shield health */

    int       direction; /* direction facing, left or right (-1 or 1) */
    float     standRadians; /* standing angle */
    float     fireRadians; /* firing angle */

    Circle*   terCircle; /* circle for collisions with terrain overhangs */
    Circle*   colCircle; /* circle for collisions with projectiles */
    Sprite*   sprite;
    Physical* physical;
    Weapon*   weapon;
};

#endif
