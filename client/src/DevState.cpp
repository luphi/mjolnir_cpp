#include <cmath> /* for atan2, cos, sin, sqrt */
#include <cstddef> /* for NULL */
#include <cstring> /* for strstr, strtok */

#include "DevState.hpp"
#include "Event.hpp"
#include "Input.hpp"

/* used for readability when calculating a viking's orientation */
#define LEFT   0
#define RIGHT  1
#define CENTER 2

DevState::DevState()
{
	globals    = NULL;
	fifo       = NULL;
	camera     = NULL;
	terrain    = NULL;
	collisions = NULL;
	input      = NULL;
	socket     = NULL;
	
    globals    = Globals::Instance();
    fifo       = globals->fifo;
    camera     = globals->camera;
    terrain    = globals->terrain;
    collisions = globals->collisions;
    input      = globals->input;
    socket     = globals->socket;
    
    viking = new Viking();
    hud    = new HUD();
    text   = new Text();
    
    viking->init(BIGFOOT);
    viking->move(500.0f, 500.0f);
    hud->init();
    terrain->init("2r2m0cg.txt");
    text->init("PING #", 255, 255, 255);
    text->position = glm::vec2(1100.0f, 50.0f);
    socket->join("127.0.0.1", 1134);
    
    {
		Event event;
		char  str[128];
		
		fifo->packVikingName(&event, -1, viking->name);
        socket->sendEvent(event);
        fifo->packVikingChoice(&event, -1, BIGFOOT);
        socket->sendEvent(event);
        fifo->packWeaponSubtype(&event, -1, 0);
        socket->sendEvent(event);
        sprintf(str, "To all");
        fifo->packMessageAll(&event, -1, str);
        socket->sendEvent(event);
        sprintf(str, "To teammates");
        fifo->packMessageTeam(&event, -1, str);
        socket->sendEvent(event);
	}
    
    textInput = globals->textInput;
    textInput->position.x = 100.0f;
    textInput->position.y = 200.0f;
    
    stateChange = NO_CHANGE;
}

DevState::~DevState()
{
    delete  viking;
    viking  = NULL;
    delete     hud;
    hud     = NULL;
    delete    text;
    text    = NULL;
}

void DevState::draw()
{
    terrain->draw();
    
    viking->draw();
    text->draw();
    
    camera->identityView();
    hud->draw();
    textInput->draw();
}

void DevState::resize()
{
	
}

void DevState::poll()
{
    int code;
    
    if (input == NULL)
    {
        return;
    }
    
    while ((code = input->poll()) != NO_INPUT)
    {
        switch (code)
        {
            case RESIZE:
                camera->resize();
                break;
            case ESCAPE_DOWN:
                stateChange = MENU_STATE;
                break;
            case ENTER_DOWN:
                handleInput((char*) textInput->flush().c_str());
                break;
            case RIGHT_DOWN:
                break;
            case RIGHT_UP:
                break;
            case LEFT_DOWN:
                break;
            case LEFT_UP:
                break;
            case UP_DOWN:
                break;
            case UP_UP:
                break;
            case DOWN_DOWN:
                break;
            case DOWN_UP:
                break;
            case SPACE_DOWN:
                break;
            case SPACE_UP:
                break;
            case F1_DOWN:
                globals->debug = !globals->debug;
                break;
            case N1_DOWN:
                if (viking != NULL)
                {
                    viking->weapon->type(1);
                }
                break;
            case N2_DOWN:
                if (viking != NULL)
                {
                    viking->weapon->type(2);
                }
                break;
            case N3_DOWN:
                if (viking != NULL)
                {
                    viking->weapon->type(3);
                }
                break;
        }
    }
}

void DevState::step(int dt)
{    
    handleEvents();
    //viking->step(dt);
}

int DevState::nextState()
{
    return stateChange;
}

void DevState::handleEvents()
{
    Event ev;
    
    if (fifo == NULL)
    {
        return;
    }
    
    do
    {
        ev = fifo->popEvent();
        
        switch (ev.op)
        {
            default:
                break;
        }
    }
    while (ev.op != NO_OP);
}

void DevState::handleInput(char* input)
{
    char upper[128];
    int  i;
    
    for (i = 0; input[i] != '\0'; i++)
    {
        upper[i] = toupper(input[i]);
    }
    upper[i] = '\0';
    
    if (strstr(upper, "SEND_EVENT") == upper)
    {
        Event event;
        char* opcode;
        
        opcode = strtok(upper, " ");
        opcode = strtok(NULL,  " ");
        if (opcode == NULL)
        {
            return;
        }
        
        if (strcmp(opcode, "PONG") == 0)
        {
            event.op  = PONG;
            event.id  = 0;
            socket->sendEvent(event);
            printf("wrote PONG\n");
        }
    }
    else if (strstr(upper, "POST_EVENT") == upper)
    {
        
    }
}
