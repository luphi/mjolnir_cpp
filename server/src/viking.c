/* C standard library */
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* additional libraries */
#include <arpa/inet.h>
#include <netinet/in.h>

/* Mjolnir server stuff */
#include "globals.h"
#include "log.h"
#include "net.h"
#include "viking.h"
#include "ui.h"

extern Globals* g;

Viking* viking_create(int socket_fd)
{
    Viking*            viking;
    struct sockaddr_in addr;
    socklen_t          len;
    
    log_enter(g->log, __func__);
    
    viking = (Viking*) malloc(sizeof(Viking));
    if (viking == NULL)
    {
        ui_print_error(g->ui, "Could not allocate memory for a viking object - "
            "something went terribly wrong");
        return NULL;
    }
    viking->id        = -1;
    viking->socket_fd = socket_fd;
    viking->last_pong = 0;
    strcpy(viking->ip_address, "0.0.0.0");
    viking->x         = 0;
    viking->y         = 0;
    memset(viking->name, '\0', 16);
    
    /* get the IP address as a string from the socket descriptor */
    /* not actually needed by this server but users like to know these things */
    if (getpeername(socket_fd, (struct sockaddr*) &addr, &len) < 0)
    {
        /* there's something wrong with this socket, ignore the connection */
        free(viking);
        ui_print_error(g->ui, strerror(errno));
        log_exit(g->log, __func__);
        return NULL;
    }
    else /* getpeername() == 0 */
    {
        strcpy(viking->ip_address, inet_ntoa(addr.sin_addr));
    }
    
    log_exit(g->log, __func__);
    
    return viking;
}

void viking_destroy(Viking* viking)
{
    /* error case */
    if (viking == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    free(viking);
    
    log_exit(g->log, __func__);
}

int viking_add(int socket_fd)
{
	Viking* viking;
	Event   ev;
    int     i;
    
    /* error case */
    if (socket_fd < 0)
    {
        return -1;
    }
    
    log_enter(g->log, __func__);
    
    /* create the viking object */
    viking = viking_create(socket_fd);
    
    /* if the viking can't be added (probably a bad socket) */
    if (viking == NULL)
    {
        ui_print_error(g->ui, "A client attempted to connnect but could not be "
            "added to the viking database.");
        return -2;
    }
    
    /* if we already have the maximum number of vikings */
    if (g->viking_count == g->viking_max)
    {
        Event ev;
        ev.op = SERVER_FULL;
        net_send_event(viking, ev);
        viking_destroy(viking);
        viking = NULL;
        ui_print_warning(g->ui, "A client attempted to connect but the "
            "server is already full.");
        return -3;
    }
    
    /* find a slot to add the viking too */
    for (i = 0; i < g->viking_max; i++)
    {
        if (g->vikings[i] == NULL)
        {
            /* found an empty slot, add the viking here */
            pthread_mutex_lock(&g->vikings_mutex);
            g->vikings[i] = viking;
            viking->id    = i;
            pthread_mutex_unlock(&g->vikings_mutex);
            break; /* don't want to add any more */
        }
    }
    
    ev.op = VIKING_CONNECT;
    ev.id = viking->id;
    net_send_event_all_but(ev, viking->id);
    
    g->viking_count += 1;
    ui_print_success(g->ui, "Added a new viking to the database");
    
    log_exit(g->log, __func__);
    
    return 0;
}

void viking_remove(Viking* viking)
{
	Event ev;
	int   id;
	
    /* error case */
    if (viking == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    id    = viking->id;
    ev.op = VIKING_DISCONNECT;
    ev.id = id;
    net_send_event_all_but(ev, viking->id);
    
    net_client_socket_close(viking);
    pthread_mutex_lock(&g->vikings_mutex);
    viking_destroy(viking);
    g->vikings[id]   = NULL;
    g->viking_count -= 1;
    pthread_mutex_unlock(&g->vikings_mutex);
    ui_print_success(g->ui, "Removed a viking from the database");
    
    log_exit(g->log, __func__);
}

void viking_remove_all()
{
    int i;
    
    log_enter(g->log, __func__);
    
    for (i = 0; i < g->viking_max; i++)
    {
		if (g->vikings[i] != NULL)
		{
			viking_remove(g->vikings[i]);
		}
    }
    
    g->viking_count = 0;
    
    log_exit(g->log, __func__);
}
