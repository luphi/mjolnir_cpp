#include <cstddef> /* for NULL */

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.hpp"
#include "Globals.hpp"
#include "Terrain.hpp"

Camera::Camera()
{

}

Camera::~Camera()
{

}

void Camera::lookAt(Drawable* draw)
{
    Globals*  globals;
    Terrain*  terrain;
    glm::mat4 view;
    glm::vec3 target;
    
    /* error case */
    if (draw == NULL)
    {
        return;
    }
    
    globals   = Globals::Instance();
    target.x  = draw->position.x + draw->dimensions.x / 2.0f;
    target.y  = draw->position.y + draw->dimensions.y / 2.0f;
    target.x -= (float) globals->vidWidth  / 2.0f;
    target.y -= (float) globals->vidHeight / 2.0f;
    target.z  = 0.0f;
    
    /* the camera will be constrained to the terrain texture with the */
    /* exception of the upward Y dimension, which will not be constrained */
    terrain   = globals->terrain;
    if (terrain != NULL)
    {
        if (target.x > terrain->dimensions.x - globals->vidWidth)
        {
            target.x = terrain->dimensions.x - globals->vidWidth;
        }
        else if (target.x < 0)
        {
            target.x = 0;
        }
        
        if (target.y > terrain->dimensions.y - globals->vidHeight)
        {
            target.y = terrain->dimensions.y - globals->vidHeight;
        }
    }
    
    view = glm::lookAt(target, target + glm::vec3(0.0f, 0.0f, -1.0f),
        glm::vec3(0.0f, 1.0f, 0.0f));
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) /* offset */,
        sizeof(glm::mat4), glm::value_ptr(view));
}

void Camera::resize()
{
    Globals*  globals;
    glm::mat4 projectionMatrix;
    
    globals = Globals::Instance();
    glViewport(0, 0, globals->vidWidth, globals->vidHeight);

    globals->projectionMatrix = glm::ortho(0.0f, (GLfloat) globals->vidWidth,
        (GLfloat) globals->vidHeight, 0.0f, -1.0f, 1.0f);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4),
        glm::value_ptr(globals->projectionMatrix));
}

void Camera::identityView()
{
    glm::mat4 view;
    
    
    view = glm::mat4(1.0f);
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) /* offset */,
        sizeof(glm::mat4), glm::value_ptr(view));
}
