#include <cstddef> /* for NULL */

#include <SDL2/SDL.h>

#include "Input.hpp"
#include "TextInput.hpp"

TextInput::TextInput()
{
    position    = glm::vec2(0.0f);
    dimensions  = glm::vec2(0.0f);
    str         = "";
    ticks       = SDL_GetTicks();
    text        = NULL;
    cursor      = NULL;
    cursorIndex = 0;
    text        = NULL;
}

TextInput::~TextInput()
{
    if (text != NULL)
    {
        delete text;
        text = NULL;
    }
    
    if (cursor != NULL)
    {
        delete cursor;
        cursor = NULL;
    }
}

void TextInput::draw()
{
    int currTicks;

	if (text != NULL)
	{
		text->position = position;
		text->draw();
	}
    
    if (cursor != NULL)
    {
        currTicks = SDL_GetTicks();
        if (currTicks - ticks >= 500)
        {
            /* draw the cursor */
            cursor->draw();
        }
        else if (currTicks - ticks >= 1000)
        {
            /* don't draw the cursor but reset the counter */
            ticks = currTicks;
        }
    }
}

void TextInput::addChars(const char* cat)
{
    /* error case */
    if (text == NULL)
    {
        return;
    }
    
    str += cat;
    text->setText(str);
}

void TextInput::keyPress(int key)
{
    if (text == NULL)
    {
		text = new Text();
		text->init("", 255, 255, 255);
	}
    
    if (key == BACKSPACE_DOWN)
    {
        /* if there are characters in the string */
        if (str.size() > 0)
        {
            /* remove the last character */
            str.pop_back();
            text->setText(str);
        }
    }
    else if (key == RIGHT_DOWN) /* right arrow */
    {
        
    }
    else if (key == LEFT_DOWN) /* left arrow */
    {
        
    }
    else if (key == UP_DOWN) /* up arrow */
    {
        
    }
}

std::string TextInput::flush()
{
    /* error case */
    if (text == NULL)
    {
        return std::string("");
    }
    
    std::string val;
    
    val = str;
    str.clear();
    text->setText("");
    
    return val;
}
