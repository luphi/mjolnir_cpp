#ifndef _LOBBYSTATE_H_
#define _LOBBYSTATE_H_

#include "Camera.hpp"
#include "Globals.hpp"
#include "Input.hpp"
#include "Primitive.hpp"
#include "Sprite.hpp"
#include "State.hpp"

/**
 * Lobby contains:
 * 
 * Viking selection
 * Map selection or display, depending on who has been in the game longest
 * Messages
 * Message all/team selection
 * Start button
 * Voice chat indicator
 */

enum Choices
{
    LEIF,
    SIGURD,
    SIGMUND,
    BRUNHILD,
    THORFINN,
    GUDRID,
    SNORRI,
    ERIK,
    RAGNAR,
    LAGERTHA,
    ASLAUG,
    BJORN,
    IVAR,
    HALFDAN,
    HVITSERK,
    UBBA
};

class LobbyState : public State
{
public:

    LobbyState();
    ~LobbyState();
    
    void draw();
    void resize();
    void poll();
    void step(int dt);
    int  nextState();
    
    Globals* globals;
    Camera*  camera;
    Input*   input;
    int      stateChange;
    
    /* Viking selection variables */
    Primitive* vikingBoxes[16];
    Sprite*    vikingSprites[16];
    
};

#endif
