/* C standard library */
#include <stdint.h>
#include <stdlib.h>
#include <string.h> /* for strcpy */
#include <time.h>
#include <unistd.h>

/* additional libraries */
#include <sys/time.h>

/* Mjolnir server stuff */
#include "globals.h"
#include "lobby.h"
#include "log.h"
#include "ui.h"

extern Globals* g;

Lobby* lobby_create()
{
    Lobby* lobby;
    
    log_enter(g->log, __func__);
    
    lobby = (Lobby*) malloc(sizeof(Lobby));
    lobby->running = 0;
    pthread_mutex_init(&lobby->mutex, NULL);
    
    log_exit(g->log, __func__);
    
    return lobby;
}

void lobby_destroy(Lobby* lobby)
{
    /* error case */
    if (lobby == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    lobby_stop(lobby);
    pthread_mutex_destroy(&lobby->mutex);
    free(lobby);
    
    log_exit(g->log, __func__);
}

void lobby_start(Lobby* lobby)
{
    /* error case */
    if (lobby == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    pthread_mutex_lock(&lobby->mutex);
    lobby->running = 1;
    pthread_mutex_unlock(&lobby->mutex);
    pthread_create(&lobby->thread, NULL, lobby_thread, lobby);
    
    log_exit(g->log, __func__);
}

void lobby_stop(Lobby* lobby)
{
    /* error case */
    if (lobby == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    pthread_mutex_lock(&lobby->mutex);
    lobby->running = 0;
    pthread_mutex_unlock(&lobby->mutex);
    pthread_join(lobby->thread, NULL);
    
    log_exit(g->log, __func__);
}

void* lobby_thread(void* param)
{
    Lobby*         lobby;
    Event          ev;
    struct timeval time;
    int            ticks; /* current ticks */
    int            dt; /* delta time, in milliseconds/ticks */
    int            last_ping; /* last time a ping was sent to all players */
    int            i; /* generic iterator */
    
    /* error case */
    if (param == NULL)
    {
        return NULL;
    }
    
    log_enter(g->log, __func__);
    
    /* initial values */
    lobby     = (Lobby*) param;
    gettimeofday(&time, NULL);
    ticks     = (time.tv_sec * 1000) + (time.tv_usec / 1000);
    dt        = 0;
    last_ping = ticks;
    i         = 0;
    
    /* artificially declare all players sent a pong just now to avoid */
    /* disconnecting anyone due to load times */
    for (i = 0; i < g->viking_max; i++)
    {
        if (g->vikings[i] != NULL)
        {
            g->vikings[i]->last_pong = ticks;
        }
    }
    
    ui_print_success(g->ui, "Lobby thread started");
    
    while (lobby->running)
    {
        do
        {
            uint16_t id;
            
            ev = fifo_pop_event(g->event_fifo);
            
            switch (ev.op)
            {
                case PONG:
                    event_unpack_pong(ev, &id);
                    g->vikings[id]->last_pong = ticks;
                    break;
                case VIKING_CONNECT:
                    ui_print_success(g->ui, "New connection");
                    break;
                case VIKING_DISCONNECT:
                    ui_print_error(g->ui, "Connection dropped");
                    break;
                case VIKING_NAME:
                    {
                        char name[32];
                        char str[128];
                        
                        event_unpack_viking_name(ev, &id, name);
                        sprintf(str, "Received name \"%s\" from %d", name, id);
                        ui_print(g->ui, str);
                        strcpy(g->vikings[id]->name, name);
                    }
                    break;
                case VIKING_CHOICE:
                    {
                        char str[128];
                        uint16_t choice;
                        
                        event_unpack_viking_choice(ev, &id, &choice);
                        sprintf(str, "viking choice %d", choice);
                        ui_print(g->ui, str);
                    }
                    break;
                case WEAPON_SUBTYPE:
                    {
                        char str[128];
                        uint16_t subtype;
                        
                        event_unpack_weapon_subtype(ev, &id, &subtype);
                        sprintf(str, "weapon subtype %d", subtype);
                        ui_print(g->ui, str);
                    }
                    break;
                case MESSAGE_ALL:
                    {
                        char message[256];
                        char str[512];
                        
                        event_unpack_message_all(ev, &id, message);
                        sprintf(str, "[all] %s", message);
                        ui_print(g->ui, str);
                    }
                    break;
                case MESSAGE_TEAM:
                    {
                        char message[256];
                        char str[512];
                        
                        event_unpack_message_team(ev, &id, message);
                        sprintf(str, "[team] %s", message);
                        ui_print(g->ui, str);
                    }
                    break;
                default:
                    break;
            }
        }
        while (ev.op != NO_OP);
        
        /* calculate the duration of the last loop in milliseconds */
        gettimeofday(&time, NULL);
        dt    = ((time.tv_sec * 1000) + (time.tv_usec / 1000)) - ticks;
        ticks = ticks + dt;
        
        if (g->viking_count > 0)
        {
            /* if 3 seconds have passed since the last ping */
            if (ticks - last_ping >= 3000)
            {
                event_pack_ping(&ev);
                net_send_event_all(ev);
                last_ping = ticks;
            }
            
            /* check players for pong responses */
            for (i = 0; i < g->viking_max; i++)
            {
                /* if 5 second shave passed since the last pong */
                if ((g->vikings[i] != NULL) &&
                    (ticks - g->vikings[i]->last_pong >= 5000))
                {
                    /* the player hasn't responded, assume the connection is dead */
                }
            }
        }
        
        usleep(10000); /* sleep 10 milliseconds to spare some CPU cycles */
    }
    
    ui_print_success(g->ui, "Lobby thread stopped");
    
    log_exit(g->log, __func__);
    
    return NULL;
}
