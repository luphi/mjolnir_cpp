#ifndef _CIRCLE_H_
#define _CIRCLE_H_

#include "Primitive.hpp"

class Circle : public Primitive
{
public:

    Circle();
    ~Circle();
    
    /* void draw(); */ /* possible future debug feature */
    void init(int radius);
    
    /* inherited from Drawable */
    /* glm::vec2 position;       X and Y coordinates */
    /* glm::vec2 dimensions;     width and height */
    
    int   r;
};

#endif
