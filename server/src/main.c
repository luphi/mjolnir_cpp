/* C standard library */
#include <ctype.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* additional libraries */
#include <pthread.h>
#include <ncurses.h>

/* Mjolnir server stuff */
#include "event.h"
#include "game.h"
#include "globals.h"
#include "lobby.h"
#include "log.h"
#include "viking.h"
#include "net.h"
#include "score.h"
#include "state.h"
#include "ui.h"

/*
 * TODO:
 * 
 * Use two listening sockets: one local, one remote
 * Implement a ping/pong scheme
 * Write a proper serializer
 * Keep the game thread to 100 Hz (use the client's method)
 * 
 */

Globals* g;

void exit_gracefully()
{
    log_enter(g->log, __func__);
    
    viking_remove_all();
    net_server_socket_destroy(g->server_socket);
    g->server_socket = NULL;
    fifo_destroy(g->event_fifo);
    g->event_fifo = NULL;
    ui_destroy(g->ui);
    g->ui = NULL;
    log_destroy(g->log);
    g->log = NULL;
    free(g);
    exit(0);
}

void exit_crudely()
{
    log_enter(g->log, __func__);
    
    log_write(g->log);
    
    exit(0);
}

void handle_interrupt(int signal)
{
    log_enter(g->log, __func__);
    
    exit_gracefully();
}

void handle_abort(int signal)
{
    log_enter(g->log, __func__);
    
    exit_crudely();
}

void handle_resize(int signal)
{
    log_enter(g->log, __func__);
    
    if (g->ui == NULL)
    {
        return;
    }
    
    ui_handle_resize(g->ui);
    
    log_exit(g->log, __func__);
}

void user_input(char* input)
{
    int i;
    
    log_enter(g->log, __func__);
    
    for (i = 0; input[i] != '\0'; i++)
    {
        input[i] = toupper(input[i]);
    }
    
    if (strcmp(input, "START") == 0)
    {
        if (g->server_running)
        {
            ui_print_warning(g->ui, "Server is already running");
            return;
        }
        
        pthread_mutex_lock(&g->state_mutex);
        g->state = TRANSITION_TO_LOBBY;
        pthread_mutex_unlock(&g->state_mutex);
    }
    else if (strcmp(input, "STOP") == 0)
    {
        if (g->server_running)
        {
            pthread_mutex_lock(&g->state_mutex);
            g->state = TRANSITION_TO_IDLE;
            pthread_mutex_unlock(&g->state_mutex);
        }
        else
        {
            ui_print_warning(g->ui, "Server not running");
        }
    }
    else if (strcmp(input, "REMOVE") == 0)
    {
        viking_remove_all();
        ui_print_success(g->ui, "Removed all players");
    }
    else if (strcmp(input, "QUIT") == 0)
    {
        exit_gracefully();
    }
    else if (strcmp(input, "EXIT") == 0)
    {
        exit_gracefully();
    }
    
    log_exit(g->log, __func__);
}

int main(int argc, char** argv)
{
    int    quit;
    int    i;
    char   keyboard_buffer[128];
    Lobby* lobby;
    Game*  game;
    Score* score;
    
    
    /* g, the global variable structure, is declared at the top */
    g = (Globals*) malloc(sizeof(Globals));
    
    /* initial values */
    g->log            = log_create();
    for (i = 0; i < 8; i++)
    {
        g->vikings[i] = NULL;
    }
    g->state          = IDLE_STATE;
    pthread_mutex_init(&g->state_mutex, NULL);
    g->viking_count   = 0;
    g->viking_max     = 8;
    pthread_mutex_init(&g->vikings_mutex, NULL);
    g->ui             = NULL; /* creation done below, it *might* fail */
    g->server_socket  = net_server_socket_create();
    g->event_fifo     = fifo_create();
    g->server_running = 0;
    
    g->ui = ui_create();
    if (g->ui == NULL)
    {
        perror("Failed to initialize curses\n");
        exit_gracefully();
    }
    ui_print_status_bad(g->ui, "Server not yet started");
    ui_refresh(g->ui);
    
    signal(SIGINT,   handle_interrupt);
    signal(SIGABRT,  handle_abort);
    signal(SIGWINCH, handle_resize);
    
    quit  = 0;
    i     = 0;
    lobby = lobby_create();
    game  = game_create();
    score = score_create();
    
    while (quit == 0)
    {
        int input;
        
        switch (g->state)
        {
            case IDLE_STATE:
                break;
            case TRANSITION_TO_IDLE:
                viking_remove_all();
                net_server_socket_stop(g->server_socket);
                lobby_stop(lobby);
                game_stop(game);
                score_stop(score);
                pthread_mutex_lock(&g->state_mutex);
                g->state = IDLE_STATE;
                pthread_mutex_unlock(&g->state_mutex);
                ui_print_status_bad(g->ui, "Server stopped");
                g->server_running = 0;
                break;
            case LOBBY_STATE:
                break;
            case TRANSITION_TO_LOBBY:
                score_stop(score);
                /* starting the socket can fail for several reasons */
                if (net_server_socket_start(g->server_socket, "1134") < 0)
                {
                    ui_print_status_bad(g->ui, "Server failed to start");
                    pthread_mutex_lock(&g->state_mutex);
                    g->state = TRANSITION_TO_IDLE;
                    pthread_mutex_unlock(&g->state_mutex);
                    break;
                }
                lobby_start(lobby);
                pthread_mutex_lock(&g->state_mutex);
                g->state = LOBBY_STATE;
                pthread_mutex_unlock(&g->state_mutex);
                ui_print_status_good(g->ui, "Server running");
                g->server_running = 1;
                break;
            case GAME_STATE:
                break;
            case TRANSITION_TO_GAME:
                break;
            case SCORE_STATE:
                break;
            case TRANSITION_TO_SCORE:
                break;
        }
        
        /* ui_keyboard_input is non-blocking, modifies keyboard_buffer, and */
        /* returns 1 if there's input available (0 otherwise) */
        input = ui_keyboard_input(g->ui, keyboard_buffer);
        
        ui_refresh(g->ui);
        
        if (input)
        {
            ui_print(g->ui, keyboard_buffer);
            
            /* respond to the input */
            user_input(keyboard_buffer);
            memset(keyboard_buffer, '\0', 128);
        }
        
        usleep(10000); /* sleep 10000 microseconds/10 milliseconds */
    }
    
    lobby_destroy(lobby);
    lobby = NULL;
    game_destroy(game);
    game  = NULL;
    score_destroy(score);
    score = NULL;
    
    exit_gracefully();
}
