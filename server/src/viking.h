#ifndef _PLAYER_H_
#define _PLAYER_H_

/* additional libraries */
#include <pthread.h>
#include <sys/socket.h>

enum VikingChoices
{
    ARMOUR,
    MAGE,
    NAKMACHINE,
    TRICO,
    BIGFOOT,
    BOOMER,
    RAONLAUNCHER,
    LIGHTNING,
    JD,
    ASATE,
    ICE,
    TURTLE,
    GRUB,
    ADUKA,
    KALSIDDON,
    JFROG
};

typedef struct viking_t
{
	/* general info */
    int  id;
    
    /* network info */
    int  socket_fd;
    int  last_pong;
    char ip_address[16];
    
    /* game info */
    int  x;
    int  y;
    char name[32];
} Viking;

Viking* viking_create(int socket_fd);
void    viking_destroy(Viking* viking);
int     viking_add(int socket_fd);
void    viking_remove(Viking* viking);
void    viking_remove_all();

#endif
