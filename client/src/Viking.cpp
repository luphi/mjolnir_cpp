#include <cmath> /* for cos, sin */
#include <cstddef> /* for NULL */
#include <cstdio> /* for printf */
#include <cstring> /* for strcpy */

#include "Event.hpp"
#include "Globals.hpp"
#include "Viking.hpp"

Viking::Viking()
{
    id           = -1;
    strcpy(name, "Ragnar");
    health       = 0;
    shield       = 0;
    maxShield    = 0;
    direction    = RIGHT;
    standRadians = 3.14159f / 2.0f; /* 90 degrees */
    fireRadians  = 3.14159f / 4.0f; /* 45 degrees */
    terCircle    = NULL;
    colCircle    = NULL;
    sprite       = NULL;
    physical     = NULL;
    weapon       = NULL;
}

Viking::~Viking()
{
    if (terCircle != NULL)
    {
        delete terCircle;
    }
    if (colCircle != NULL)
    {
        delete colCircle;
    }
    if (sprite != NULL)
    {
        delete sprite;
    }
    if (physical != NULL)
    {
        delete physical;
    }
    if (weapon != NULL)
    {
        delete weapon;
    }
}

bool Viking::init(int mobile)
{
    bool spriteStatus;
    bool weaponStatus;
    
    spriteStatus = true;
    weaponStatus = true;
    switch (mobile)
    {
        case ARMOUR:
            health       = 1000;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponArmour();
            break;
        case MAGE:
            health       = 760;
            shield       = 220;
            sprite       = new Sprite();
            weapon       = new WeaponMage();
            break;
        case NAKMACHINE:
            health       = 1100;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponNakmachine();
            break;
        case TRICO:
            health       = 1100;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponTrico();
            break;
        case BIGFOOT:
            health       = 1100;
            shield       = 0;
            sprite       = new Sprite();
            spriteStatus = sprite->init("media/vikings/thorfinn/thorfinn.bmp");
            weapon       = new WeaponBigfoot();
            break;
        case BOOMER:
            health       = 1000;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponBoomer();
            break;
        case RAONLAUNCHER:
            health       = 1000;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponRaonlauncher();
            break;
        case LIGHTNING:
            health       = 760;
            shield       = 220;
            sprite       = new Sprite();
            weapon       = new WeaponLightning();
            break;
        case JD:
            health       = 750;
            shield       = 250;
            sprite       = new Sprite();
            weapon       = new WeaponJd();
            break;
        case ASATE:
            health       = 760;
            shield       = 220;
            sprite       = new Sprite();
            weapon       = new WeaponAsate();
            break;
        case ICE:
            health       = 1200;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponIce();
            break;
        case TURTLE:
            health       = 950;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponTurtle();
            break;
        case GRUB:
            health       = 1000;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponGrub();
            break;
        case ADUKA:
            health       = 1000;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponAduka();
            break;
        case KALSIDDON:
            health       = 1100;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponKalsiddon();
            break;
        case JFROG:
            health       = 930;
            shield       = 0;
            sprite       = new Sprite();
            weapon       = new WeaponJfrog();
            break;
    }
    
    /* the shield values are initialized as the maximum values */
    maxShield = shield;
        
    /* have the weapons set their respective values if needed */
    weaponStatus = weapon->init();
    
    /* if an error occurred for any reason */
    if (spriteStatus == false)
    {
        printf("Viking sprite initialization failed\n");
        
        if (sprite != NULL)
        {
            delete sprite;
            sprite = NULL;
        }
        
        return false;
    }
    
    if (weaponStatus == false)
    {
        printf("Viking weapon initialization failed\n");
        
        if (weapon != NULL)
        {
            delete weapon;
            weapon = NULL;
        }
        
        return false;
    }
    
    terCircle = new Circle();
    terCircle->init(15.0f);
    terCircle->color = glm::vec3(1.0f, 0.0f, 0.0f); /* red */
    colCircle = new Circle();
    colCircle->init(25.0f);
    colCircle->color = glm::vec3(0.0f, 0.0f, 1.0f); /* blue */
    physical  = new Physical();
    
    return true;
}

void Viking::step(int dt)
{
    /* error cases */
    if ((weapon == NULL) || (physical == NULL))
    {
        return;
    }
    
    physical->step(dt);
    
    move(physical->x, physical->y);
    
    if (weapon->active)
    {
        weapon->step(dt);
    }
}

void Viking::draw()
{
    /* error cases */
    if ((sprite == NULL) || (weapon == NULL))
    {
        return;
    }
    
    sprite->draw();
    
    if (weapon->active)
    {
        weapon->draw();
    }
    
    if (Globals::Instance()->debug)
    {
        /* error cases */
        if ((terCircle == NULL) || (colCircle == NULL))
        {
            return;
        }
        
        terCircle->draw();
        colCircle->draw();
    }
}

void Viking::move(float x, float y)
{
    /* error cases */
    if ((sprite == NULL) || (physical == NULL) || (terCircle == NULL) ||
        (colCircle == NULL))
    {
        return;
    }
    
    sprite->position.x    = x;
    sprite->position.y    = y;
    physical->x           = x;
    physical->y           = y;
    terCircle->position.x = x + sprite->dimensions.x / 2 - terCircle->r;
    terCircle->position.y = y;
    colCircle->position.x = x + sprite->dimensions.x / 2 - colCircle->r;
    colCircle->position.y = y + sprite->dimensions.y / 6;
}

void Viking::translate(float dx, float dy)
{
    /* error cases */
    if ((sprite == NULL) || (physical == NULL) || (terCircle == NULL) ||
        (colCircle == NULL))
    {
        return;
    }
    
    sprite->position.x    += dx;
    sprite->position.y    += dy;
    physical->x           += dx;
    physical->y           += dy;
    terCircle->position.x += dx;
    terCircle->position.y += dy;
    colCircle->position.x += dx;
    colCircle->position.y += dy;
}

void Viking::orientation(float radians)
{    
    float dtheta;
    
    /* error case */
    if (sprite == NULL)
    {
        return;
    }
    
    dtheta       = standRadians - radians;
    standRadians = radians;
    fireRadians += dtheta;
    
    /* GLM's rotation is performed around the Z axis given a radian value   */
    /* between pi and -pi where 0 is up (traditionally what we refer to     */
    /* as 90 degrees) whereas standRadians and fireRadians are in line with */
    /* the standard unit circle                                             */
    sprite->glRotation = standRadians - 3.14159f / 2.0f;
}

void Viking::face(int dir)
{
    /* error case */
    if (sprite == NULL)
    {
        return;
    }
    
    /* if changing from one direction to the other */
    if ((direction == LEFT && dir == RIGHT) ||
        (direction == RIGHT && dir == LEFT))
    {
        direction = dir;
        sprite->mirror();
        
        if (direction == RIGHT)
        {
            fireRadians -= 3.14159 / 2.0f;
        }
        else /* direction == LEFT */
        {
            fireRadians += 3.14159 / 2.0f;
        }
    }
}

void Viking::freeze()
{
    /* error case */
    if (physical == NULL)
    {
        return;
    }
    
    physical->frozen = true;
}

void Viking::unfreeze()
{
    /* error case */
    if (physical == NULL)
    {
        return;
    }
    
    physical->frozen = false;
}

void Viking::fire()
{
    float x;
    float y;
    
    /* error cases */
    if ((sprite == NULL) || (weapon == NULL))
    {
        return;
    }
    
    if (weapon == NULL)
    {
        return;
    }
    
    /* first, get the center of the player */
    x  = sprite->position.x + sprite->dimensions.x / 2.0f;
    y  = sprite->position.y + sprite->dimensions.y / 2.0f;
    
    /* then, move the point outside of the player in the firing direction */
    x += sprite->dimensions.x * cos(fireRadians);
    y -= sprite->dimensions.y * sin(fireRadians);
    
    weapon->fire(x, y, fireRadians, 50);
}

void Viking::hurt(int damage)
{
    int remaining;
    
    remaining = damage;
    
    if (shield > 0)
    {
        shield -= remaining;
        
        if (shield < 0)
        {
            remaining += -shield;
            shield    += -shield;
        }
    }
    
    health -= remaining;
    
    if (health <= 0)
    {
        /* Globals::Instance()->fifo->pushEvent(DEATH, id); */
    }
    
    printf("player %d health = %d\n", id, health);
}
