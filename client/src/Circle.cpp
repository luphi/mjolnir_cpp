#include <glm/glm.hpp>

#include "Circle.hpp"

Circle::Circle()
{
    position   = glm::vec2(0.0f);
    dimensions = glm::vec2(0.0f);
    r          = 0;
}

Circle::~Circle()
{
    
}

void Circle::init(int radius)
{
    r            = radius;
    dimensions.x = radius * 2;
    dimensions.y = radius * 2;
    
    Primitive::init(PRIMITIVE_CIRCLE);
}
