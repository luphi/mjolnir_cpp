#include "Globals.hpp"
#include "Physical.hpp"

Physical::Physical()
{
    x      = 0.0f;
    y      = 0.0f;
    xVel   = 0.0f;
    yVel   = 0.0f;
    xAcc   = 0.0f;
    yAcc   = 0.0f;
    frozen = false;
}

Physical::~Physical()
{
    
}

void Physical::step(int dt)
{
	Globals* g;
    
	g         = Globals::Instance();
	float dt1 = dt / 1000.0f; /* milliseconds to seconds */
	
	float xA = xAcc + g->windAccX;
	float yA = yAcc + g->windAccY + g->gAcc;
	
	/* this is an application of a fourth-order Runge-Kutta method */
	float x1  = x;
	float v1  = xVel;
	float a1  = acceleration(x1, v1, xA, 0);
	float x2  = x + 0.5 * v1 * dt1;
	float v2  = xVel + 0.5 * a1 * dt1;
	float a2  = acceleration(x2, v2, xA, dt / 2.0);
	float x3  = x + 0.5 * v2 * dt1;
	float v3  = xVel + 0.5 * a2 * dt1;
	float a3  = acceleration(x3, v3, xA, dt1 / 2.0);
	float x4  = x + v3 * dt1;
	float v4  = xVel + a3 * dt1;
	float a4  = acceleration(x4, v4, xA, dt1);
	x        += (dt1 / 6.0) * (v1 + 2 * v2 + 2 * v3 + v4);
	xVel     += (dt1 / 6.0) * (a1 + 2 * a2 + 2 * a3 + a4);

    if (frozen)
    {
        return;
    }

	float y1  = y;
	v1        = yVel;
	a1        = acceleration(y1, v1, yA, 0);
	float y2  = x + 0.5 * v1 * dt1;
	v2        = yVel + 0.5 * a1 * dt1;
	a2        = acceleration(y2, v2, yA, dt / 2.0);
	float y3  = y + 0.5 * v2 * dt1;
	v3        = yVel + 0.5 * a2 * dt1;
	a3        = acceleration(y3, v3, yA, dt1 / 2.0);
	float y4  = y + v3 * dt1;
	v4        = yVel + a3 * dt1;
	a4        = acceleration(y4, v4, yA, dt1);
	y        += (dt1 / 6.0) * (v1 + 2 * v2 + 2 * v3 + v4);
	yVel     += (dt1 / 6.0) * (a1 + 2 * a2 + 2 * a3 + a4);
	
	/* keep velocity within reason */
	if (abs(xVel) > g->termVel)
		xVel = (xVel / abs(xVel)) * g->termVel;
	if (abs(yVel) > g->termVel)
		yVel = (yVel / abs(yVel)) * g->termVel;
}

float Physical::acceleration(float px, float pv, float a, int dt)
{
    /* this will probably be changed in the future */
    return a * dt * dt;
}
