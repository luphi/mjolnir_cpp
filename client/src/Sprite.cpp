#include <cstddef> /* for NULL */
#include <cstdio>

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "Globals.hpp"
#include "Sprite.hpp"

Sprite::Sprite()
{
    position   = glm::vec2(0.0f);
    dimensions = glm::vec2(0.0f);
    pixels     = NULL;
    pixelsLen  = 0;
    glRotation = 0.0f;
    glCount    = 6;
    glMode     = GL_TRIANGLES;
    glVAO      = 0;
    glVBO      = 0;
    glEBO      = 0;
    glTex      = 0;
    mirrored   = false;
}

Sprite::~Sprite()
{
    glDeleteTextures(1,     &glTex);
    glDeleteBuffers(1,      &glEBO);
    glDeleteBuffers(1,      &glVBO);
    glDeleteVertexArrays(1, &glVAO);
}

bool Sprite::init(const char* file)
{
    SDL_Surface* surface;
    GLint        bpp; /* bytes per pixel */
    GLenum       colorspace; /* RGB, BGRA, etc. */
    
    GLfloat glVertices[] = {
        /* position */   /* texcoords */
        0.5f,  0.5f,     1.0f, 1.0f, /* top right */
        0.5f, -0.5f,     1.0f, 0.0f, /* bottom right */
       -0.5f, -0.5f,     0.0f, 0.0f, /* bottom left */
       -0.5f,  0.5f,     0.0f, 1.0f  /* top left  */
    };
    
    GLuint glIndices[] = {
        0, 1, 3,
        1, 2, 3
    };
    
    surface    = NULL;
    bpp        = 0;
    colorspace = GL_RGBA;
    
    glGenVertexArrays(1, &glVAO);
    glBindVertexArray(glVAO);

    glGenBuffers(1, &glVBO);
    glBindBuffer(GL_ARRAY_BUFFER, glVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glVertices), glVertices,
        GL_STATIC_DRAW);
    
    glGenBuffers(1, &glEBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glIndices), glIndices,
        GL_STATIC_DRAW);
        
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat),
        (GLvoid*) (2 * sizeof(GLfloat)));

    glGenTextures(1, &glTex);
    glBindTexture(GL_TEXTURE_2D, glTex);
    
    surface = IMG_Load(file);
    if (surface != NULL)
	{
		if ((surface->w & (surface->w - 1)) != 0)
		{
        	printf("Warning: %s\'s width is not a power of two: %d\n",
                file, surface->w);
		}
        if ((surface->h & (surface->h - 1)) != 0)
		{
        	printf("Warning: %s\'s height is not a power of two: %d\n",
                file, surface->h);
		}
        dimensions = glm::vec2((float) surface->w, (float) surface->h);
		bpp        = surface->format->BytesPerPixel;
		if (bpp == 4)
		{
			if (surface->format->Rmask == 0x000000ff)
            {
				colorspace = GL_RGBA;
			}
            else
			{
            	colorspace = GL_BGRA;
            }
		}
		else if (bpp == 3)
		{
			if (surface->format->Rmask == 0x000000ff)
            {
				colorspace = GL_RGB;
			}
            else
			{
            	colorspace = GL_BGR;
            }
        }
		else
        {
			printf("Warning: %s is not true color\n", file);
        }
        
        glTexImage2D(GL_TEXTURE_2D, 0, colorspace, surface->w, surface->h, 0,
			         colorspace, GL_UNSIGNED_BYTE, surface->pixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		SDL_FreeSurface(surface);
    }
    else
	{
        printf("Failed to load image: %s\n", file);
        return false;
    }

    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return true;
}

bool Sprite::initBMP(const char* file)
{
    GLfloat verts[] = {
        /* position */   /* texcoords */
        0.5f, -0.5f,     1.0f, 1.0f, /* top right */
        0.5f,  0.5f,     1.0f, 0.0f, /* bottom right */
       -0.5f,  0.5f,     0.0f, 0.0f, /* bottom left */
       -0.5f, -0.5f,     0.0f, 1.0f  /* top left  */
    };
    
    GLuint inds[] = {
        0, 1, 3,
        1, 2, 3
    };
    
    loadBMP(file);
    
    if (pixels == NULL)
    {
        return false;
    }
    
    glGenVertexArrays(1, &glVAO);
    glBindVertexArray(glVAO);

    glGenBuffers(1, &glVBO);
    glBindBuffer(GL_ARRAY_BUFFER, glVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    
    glGenBuffers(1, &glEBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(inds), inds, GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat),
        (GLvoid*) (2 * sizeof(GLfloat)));
    
    glGenTextures(1, &glTex);
    glBindTexture(GL_TEXTURE_2D, glTex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int) dimensions.x,
                 (int) dimensions.y, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8,
                 pixels);
	
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    free(pixels);
    pixels = NULL;
    
    return true;
}

void Sprite::draw()
{
    Globals*  globals;
    glm::mat4 model;

    globals = Globals::Instance();

    model = glm::translate(model, glm::vec3(position, 0.0f));
    /* the next translation is due to a difference in coordinate systems */
    /* between what this game uses and OpenGL - we consider the top left */
    /* corner to be (0, 0) but the sprite is centered in normalized */
    /* device coordinates such that (0, 0) would be the center of the sprite */
    /* if not compensated for with this translation */
    model = glm::translate(model, glm::vec3(dimensions / 2.0f, 0.0f));
    model = glm::rotate(model, glRotation, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::scale(model, glm::vec3(dimensions, 1.0f));

    /* if this sprite is mirrored across the X axis */
    if (mirrored)
    {
        /* do one more scale on a negative X axis to flip it */
        model = glm::scale(model, glm::vec3(-1.0f, 1.0f, 1.0f));
    }

    glUseProgram(globals->glSpriteProgram);
    glUniformMatrix4fv(glGetUniformLocation(globals->glSpriteProgram,
        "spriteModel"), 1, GL_FALSE, glm::value_ptr(model));

    glBindTexture(GL_TEXTURE_2D, glTex);
    glBindVertexArray(glVAO);
    glDrawElements(glMode, glCount, GL_UNSIGNED_INT, 0);
    
    glBindVertexArray(0);
    glUseProgram(0);
}

void Sprite::mirror()
{
    mirrored = !mirrored;
}

void Sprite::loadBMP(const char* file)
{
    FILE*                handle;
    struct BMPFileHeader fileHeader;
    struct BMPInfoHeader infoHeader;

    handle = NULL;

    /* open the image in read binary mode */
    handle = fopen(file, "rb");
    if (handle == NULL)
    {
        printf("Failed to open BMP file %s\n", file);
        return;
    }

    /* read the file/primary header into the matching struct */
    if (fread(&fileHeader, sizeof(struct BMPFileHeader), 1, handle) != 1)
    {
        printf("Cannot read primary header of BMP file %s\n", file);
        fclose(handle);
        return;
    }
    /* read the info/secondary header into the matching struct */
    if (fread(&infoHeader, sizeof(struct BMPInfoHeader), 1, handle) != 1)
    {
        printf("Cannot read info header of BMP file %s\n", file);
        fclose(handle);
        return;
    }

    if (fileHeader.signature != 0x4d42)
    {
        /* the 4D42 signiture indicates the file is a BMP */
        printf("BMP file %s does not contain a BMP signature and may not be an "
               "actual BMP file - cannot load it\n", file);
        return;
    }
    
    if (infoHeader.compression != 3)
    {
        /* A compression value of 3 indicates RGB with a mask (an alpha */
        /* channel in this case) and we're assuming ABGR */
        printf("BMP file %s either uses a compression method (no compression "
               "is supported) or the wrong colorspace (only ABGR is supported) "
               "- cannot load it\n", file);
        return;
    }

    /* move to the location in the file where pixels begin */
    fseek(handle, fileHeader.offsetToPixels, SEEK_SET);

    /* allocate the memory needed to hold the pixel data */
    pixels = (unsigned char*) malloc(infoHeader.imageSize);
    if (pixels == NULL)
    {
        printf("Memory allocation for BMP pixel data failed\n");
        fclose(handle);
        return;
    }

    /* read the pixel data */
    if ((fread(pixels, infoHeader.imageSize, 1, handle)) != 1)
    {
        printf("Failed to read pixel data of BMP file %s - is it corrupted?",
               file);
        fclose(handle);
        return;
    }

    /* we're done with the file */
    fclose(handle);

    dimensions.x = infoHeader.width;
    dimensions.y = infoHeader.height;
    pixelsLen    = infoHeader.imageSize;
}
