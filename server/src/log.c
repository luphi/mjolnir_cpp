/* C standard library */
#include <stdlib.h>
#include <string.h>

/* additional libraries */
#include <pthread.h>

/* Mjolnir server stuff */
#include "log.h"

Log* log_create()
{
    Log* log;
    
    log            = (Log*) malloc(sizeof(Log));
    memset(log->user_text,  '\0', 2056);
    memset(log->debug_text, '\0', 2056);
    log->user_len  = 0;
    log->debug_len = 0;
    log->user_fd   = NULL;
    log->debug_fd  = NULL;
    pthread_mutex_init(&log->mutex, NULL);
    
    /* fopen can return NULL for various reasons but log_write checks */
    /* before attempting to write to the files so there's no need to check it */
    log->user_fd   = fopen("user_log.txt", "w");
    log->debug_fd  = fopen("debug_log.txt", "w");
    
    log_exit(log, __func__);
    
    return log;
}

void log_destroy(Log* log)
{
    /* error case */
    if (log == NULL)
    {
        return;
    }
    
    log_enter(log, __func__);
    
    log_write(log);
    
    if (log->user_fd != NULL)
    {
        fclose(log->user_fd);
    }
    if (log->debug_fd != NULL)
    {
        fclose(log->debug_fd);
    }
    
    pthread_mutex_destroy(&log->mutex);
    free(log);
}

void log_enter(Log* log, const char* function)
{
    char text[128];
    
    /* error case */
    if (log == NULL)
    {
        return;
    }
    
    sprintf(text, "ENTER %s", function);
    log_append_debug(log, text);
}

void log_exit(Log* log, const char* function)
{
    char text[128];
    
    /* error case */
    if (log == NULL)
    {
        return;
    }
    
    sprintf(text, "EXIT %s", function);
    log_append_debug(log, text);
}

void log_append_user(Log* log, char* text)
{
    char buf[512];
    
    /* error cases */
    if ((log == NULL) || (log->user_fd == NULL) || (text == NULL))
    {
        return;
    }
    
    sprintf(buf, "%s\n", text);
    pthread_mutex_lock(&log->mutex);
    strcat(log->user_text, buf);
    log->user_len += strlen(buf);
    pthread_mutex_unlock(&log->mutex);
    
    if (log->user_len >= 1024)
    {
        log_write(log);
    }
}

void log_append_debug(Log* log, char* text)
{
    char buf[128];
    
    /* error cases */
    if ((log == NULL) || (log->debug_fd == NULL) || (text == NULL))
    {
        return;
    }
    
    sprintf(buf, "%s\n", text);
    pthread_mutex_lock(&log->mutex);
    strcat(log->debug_text, buf);
    log->debug_len += strlen(buf);
    pthread_mutex_unlock(&log->mutex);
    
    if (log->debug_len >= 1024)
    {
        log_write(log);
    }
}

void log_write(Log* log)
{
    /* error case */
    if (log == NULL)
    {
        return;
    }
    
    pthread_mutex_lock(&log->mutex);
    if (log->user_fd != NULL)
    {
        fprintf(log->user_fd, "%s", log->user_text);
        fflush(log->user_fd);
    }
    if (log->debug_fd != NULL)
    {
        fprintf(log->debug_fd, "%s", log->debug_text);
        fflush(log->debug_fd);
    }
    
    memset(log->user_text,  '\0', 2056);
    memset(log->debug_text, '\0', 2056);
    log->user_len  = 0;
    log->debug_len = 0;
    pthread_mutex_unlock(&log->mutex);
}
