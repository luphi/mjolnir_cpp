#ifndef _TEXTINPUT_H_
#define _TEXTINPUT_H_

#include <map>
#include <string>

#include "Drawable.hpp"
#include "Primitive.hpp"
#include "Text.hpp"

class TextInput : public Drawable
{
public:
    
    TextInput();
    ~TextInput();
    
    void        draw();
    void        addChars(const char* cat);
    void        keyPress(int key);
    std::string flush();
    
    /* inherited from Drawable */
    /* glm::vec2 position;       X and Y coordinates */
    /* glm::vec2 dimensions;     width and height */
    
private:
    
    std::string         str;
    int                 ticks;
    Text*               text;
    Primitive*          cursor;
    int                 cursorIndex;
    
};

#endif
