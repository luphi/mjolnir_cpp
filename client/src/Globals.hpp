#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <pthread.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "Camera.hpp"
#include "Collisions.hpp"
#include "Event.hpp"
#include "Input.hpp"
#include "Socket.hpp"
#include "Terrain.hpp"
#include "TextInput.hpp"
#include "Viking.hpp"

/* This is a basic singleton object */
class Globals
{
public:

	Globals();
	~Globals();

	static Globals* Instance();	
    
    /* SDL/window/surface variables */
    SDL_Window*   window; /* window, caption, holds dimensions, etc. */
    int           vidWidth; /* width of window in pixels */
    int           vidHeight; /* height of window in pixels */
    glm::vec2     mouse; /* instantaneous mouse/cursor position */

    /* OpenGL variables */
    SDL_GLContext glContext; /* OpenGL context */
    GLuint        glSpriteVShader; /* sprite vertex shader */
    GLuint        glSpriteFShader; /* sprite fragment shader */
    GLuint        glSpriteProgram; /* sprite shader program */
    GLuint        glPrimitiveVShader; /* primitive vertex shader */
    GLuint        glPrimitiveFShader; /* primitive fragment shader */
    GLuint        glPrimitiveProgram; /* primitive shader program */
    GLuint        glUBO; /* uniform buffer object for projection and view */
    glm::mat4     projectionMatrix; /* orthographic projection matrix */
    
    /* physical constants */
    float         termVel; /* terminal velocity */
    float         gAcc; /* gravitational acceleration */
    int           windAccX; /* wind acceleration X component */
    int           windAccY; /* wind acceleration Y component */
    
    /* theme-related variables */
    TTF_Font*     font;
    
    /* single-instance game objects */
    Viking*       vikings[8];
    Camera*       camera;
    Terrain*      terrain;
    Collisions*   collisions;
    EventFIFO*    fifo;
    Socket*       socket;
    Input*        input;
    TextInput*    textInput;
    
    /* local player info */
    char          name[32];
    
    /* debug, or otherwise interesting, information */
    bool          debug;

private:

	static Globals* instance;

};

#endif
