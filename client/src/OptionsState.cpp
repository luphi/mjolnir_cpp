#include <cstddef> /* for NULL */

#include "OptionsState.hpp"

OptionsState::OptionsState()
{
	globals     = NULL;
	camera      = NULL;
	input       = NULL;
    stateChange = NO_CHANGE;

	globals     = Globals::Instance();
	camera      = globals->camera;
	input       = globals->input;
}

OptionsState::~OptionsState()
{
    
}

void OptionsState::draw()
{
    
}

void OptionsState::resize()
{
	
}

void OptionsState::poll()
{
    int code;
    
    if (input == NULL)
    {
        return;
    }
    
    while ((code = input->poll()) != NO_INPUT)
    {
        switch (code)
        {
            case RESIZE:
                camera->resize();
                break;
            case ESCAPE_DOWN:
                stateChange = MENU_STATE;
                break;
            case ENTER_DOWN:
                break;
            case RIGHT_DOWN:
                break;
            case RIGHT_UP:
                break;
            case LEFT_DOWN:
                break;
            case LEFT_UP:
                break;
            case UP_DOWN:
                break;
            case UP_UP:
                break;
            case DOWN_DOWN:
                break;
            case DOWN_UP:
                break;
            case SPACE_DOWN:
                break;
            case SPACE_UP:
                break;
            case F1_DOWN:
                break;
            case N1_DOWN:
                break;
            case N2_DOWN:
                break;
            case N3_DOWN:
                break;
        }
    }
}

void OptionsState::step(int dt)
{
    
}

int OptionsState::nextState()
{
    
    
    return stateChange;
}
