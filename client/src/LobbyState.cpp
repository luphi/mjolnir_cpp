#include <cstddef> /* for NULL */
#include <cstdio>

#include <glm/glm.hpp>

#include "LobbyState.hpp"

#define UNSELECTED_GRAY 0.05f
#define SELECTED_BLUE 39.0f, 69.0f, 76.0f

LobbyState::LobbyState()
{
    globals     = NULL;
    camera      = NULL;
    input       = NULL;
    stateChange = NO_CHANGE;
    
    for (int i = 0; i < 16; i++)
    {
        vikingBoxes[i]   = NULL;
        vikingSprites[i] = NULL;
    }
    
    globals     = Globals::Instance();
    camera      = globals->camera;
    input       = globals->input;
    
    for (int i = 0; i < 16; i++)
    {
        vikingBoxes[i] = new Primitive();
        vikingBoxes[i]->init(PRIMITIVE_QUAD);
        vikingBoxes[i]->color = glm::vec3(UNSELECTED_GRAY);
    }
    
    resize();
}

LobbyState::~LobbyState()
{
    for (int i = 0; i < 16; i++)
    {
        if (vikingBoxes[i] != NULL)
        {
            delete vikingBoxes[i];
            vikingBoxes[i] = NULL;
        }
        
        if (vikingSprites[i] != NULL)
        {
            delete vikingSprites[i];
            vikingSprites[i] = NULL;
        }
    }
}

void LobbyState::draw()
{
    for (int i = 0; i < 16; i++)
    {
        if (vikingBoxes[i] != NULL)
        {
            vikingBoxes[i]->draw();
        }
        
        if (vikingSprites[i] != NULL)
        {
            vikingSprites[i]->draw();
        }
    }
}

void LobbyState::resize()
{
    int hSpacing;   /* horizontal spacing between elements */
    int vSpacing;   /* vertical spacing between elements */
    int quadWidth;  /* width of current quadrant */
    int quadHeight; /* height of current quadrant */
    int elemWidth;  /* ideal width of each individual element */
    int elemHeight; /* ideal height of each individual element */
    
    /* starting with the viking selection quadrant */
    hSpacing   = 10;
    vSpacing   = 10;
    quadWidth  = globals->vidWidth  * 2 / 3;
    quadHeight = globals->vidHeight * 2 / 3;
    
    /* if the width:height aspect ratio of the combined elements is greater */
    /* than the aspect ratio of the quadrant */
    if (4.0f > ((float) quadWidth / (float) quadHeight))
    {
        int i;
        
        /* the height is limiting */
        elemHeight = (quadHeight - 3 * vSpacing) / 2;
        /* find the nearest power of 2 */
        for (i = 2; i < elemHeight; i *= 2);
        /* i is currently one power of 2 too high */
        elemHeight = i / 2;
        /* a viking's width is half of its height */
        elemWidth  = elemHeight / 2;
        
        printf("case 1:\n"
               "    elemWidth  = %d\n"
               "    elemHeight = %d\n", elemWidth, elemHeight);
    }
    else /* the maximum height is greater than the maximum width */
    {
        int i;
        
        /* the width is limiting */
        elemWidth = (quadWidth - 9 * hSpacing) / 8;
        /* find the nearest power of 2 */
        for (i = 2; i < elemWidth; i *= 2);
        /* i is currently one power of 2 too high */
        elemWidth = i / 2;
        /* a viking's height is twice its width */
        elemHeight = elemWidth * 2;
        
        printf("case 2:\n"
               "    elemWidth  = %d\n"
               "    elemHeight = %d\n", elemWidth, elemHeight);
    }
    
    /* recalculate spacing */
    hSpacing = (quadWidth  - (8 * elemWidth))  / 9;
    vSpacing = (quadHeight - (2 * elemHeight)) / 3;
    
    for (int i = 0; i < 16; i++)
    {
        glm::vec2 position = glm::vec2(0.0f);
        position.x = (GLfloat) ((i % 8) + 1) * hSpacing +
                     (i % 8) * elemWidth;
        position.y = (GLfloat) ((i / 8) + 1) * vSpacing +
                     (i / 8) * elemHeight;
        vikingBoxes[i]->position   = position;
        vikingBoxes[i]->dimensions = glm::vec2((GLfloat) elemWidth,
                                               (GLfloat) elemHeight);
    }
}

void LobbyState::poll()
{
    int code;
    
    if (input == NULL)
    {
        return;
    }
    
    while ((code = input->poll()) != NO_INPUT)
    {
        switch (code)
        {
            case RESIZE:
                resize();
                camera->resize(); /* recalculate projection matrix */
                break;
            case QUIT_EVENT:
                stateChange = EXIT;
            case ESCAPE_DOWN:
                stateChange = MENU_STATE;
                break;
            case ENTER_DOWN:
                break;
            case RIGHT_DOWN:
                break;
            case RIGHT_UP:
                break;
            case LEFT_DOWN:
                break;
            case LEFT_UP:
                break;
            case UP_DOWN:
                break;
            case UP_UP:
                break;
            case DOWN_DOWN:
                break;
            case DOWN_UP:
                break;
            case SPACE_DOWN:
                break;
            case SPACE_UP:
                break;
            case F1_DOWN:
                break;
            case N1_DOWN:
                break;
            case N2_DOWN:
                break;
            case N3_DOWN:
                break;
        }
    }
}

void LobbyState::step(int dt)
{
    
}

int LobbyState::nextState()
{
    return stateChange;
}
