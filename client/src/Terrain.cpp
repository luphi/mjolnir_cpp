#include <cstddef> /* for NULL */
#include <cstdio> /* for FILE, fopen, fclose, fgets */
#include <cstdlib> /* for strtol */
#include <cstring> /* for strlen */
#include <string> /* for std::string */

#include <GL/glew.h>
#include <SDL2/SDL_image.h>

#include "Terrain.hpp"

Terrain::Terrain()
{
    surface    = NULL;
    colorspace = GL_RGBA;
}

Terrain::~Terrain()
{
    SDL_FreeSurface(surface);
}

bool Terrain::init(const char* file)
{
    GLint       bpp; /* bytes per pixel */
    FILE*       handle; /* file handle */
    char        line[128]; /* buffer for a line in handle */
    int         count; /* line count */
    int         len; /* length of the current line */
    std::string path; /* path + file name */
    std::string image; /* image file */
    
    GLfloat glVertices[] = {
        /* position */   /* texcoords */
        0.5f,  0.5f,     1.0f, 1.0f, /* top right */
        0.5f, -0.5f,     1.0f, 0.0f, /* bottom right */
       -0.5f, -0.5f,     0.0f, 0.0f, /* bottom left */
       -0.5f,  0.5f,     0.0f, 1.0f  /* top left  */
    };
    
    GLuint glIndices[] = {
        0, 1, 3,
        1, 2, 3
    };
    
    bpp    = 0;
    handle = NULL;
    count  = 0;
    
    path   = "media/terrains/";
    path  += file;
    handle = fopen(path.c_str(), "r");
    if (handle == NULL)
    {
        printf("Error: cannot open terrain file %s\n", path.c_str());
        return false;
    }
    
    while (fgets(line, sizeof(line) - 1, handle) != NULL)
    {
        len = strlen(line) - 1;
        if ((len > 0) && (line[len] == '\n'))
        {
            /* remove the newline */
            line[len] = '\0';
        }
        
        if (count == 0)
        {
            /* the first line will be the filename of the image to use, in */
            /* the same directory as the info file */
            image  = "media/terrains/";
            image += line;
        }
        else /* count >= 1 */
        {
            glm::vec2 position;
            char*     end;
            
            /* the following eight lines are the drop points (where players */
            /* will start/drop from) */
            position.x = strtol(line, &end, 10);
            position.y = strtol(end,  &end, 10);
            
            dropPoints.push_back(position);
        }
        
        count++;
    }
    
    if (count < 9)
    {
        /* one line for the image file and eight drop points are expected */
        printf("Terrain info file %s is too short\n", file);
        return false;
    }
    
    glGenVertexArrays(1, &glVAO);
    glBindVertexArray(glVAO);

    glGenBuffers(1, &glVBO);
    glBindBuffer(GL_ARRAY_BUFFER, glVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glVertices), glVertices,
        GL_STATIC_DRAW);
    
    glGenBuffers(1, &glEBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glIndices), glIndices,
        GL_STATIC_DRAW);
        
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat),
        (GLvoid*) (2 * sizeof(GLfloat)));

    glGenTextures(1, &glTex);
    glBindTexture(GL_TEXTURE_2D, glTex);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    surface = IMG_Load(image.c_str());
    if (surface != NULL)
	{
		if ((surface->w & (surface->w - 1)) != 0)
		{
        	printf("Warning: terrain bitmap %s\'s width is not a power of two "
                "(%d)\n", image.c_str(), surface->w);
		}
        if ((surface->h & (surface->h - 1)) != 0)
		{
        	printf("Warning: terrain bitmap %s\'s height is not a power of two "
                "(%d)\n", image.c_str(), surface->h);
		}
        dimensions = glm::vec2((float) surface->w, (float) surface->h);
		bpp        = surface->format->BytesPerPixel;
		if (bpp == 4)
		{
			if (surface->format->Rmask == 0x000000ff)
            {
				colorspace = GL_RGBA;
			}
            else
			{
            	colorspace = GL_BGRA;
            }
		}
		else
        {
			printf("Warning: %s is not true color or does not contain "
                   "an alpha channel\n", file);
        }
        
        updateTexture();
    }
    else
	{
        printf("Failed to load terrain image: %s\n", image.c_str());
        return false;
    }
    
    position = glm::vec2(0.0f, 0.0f);
    
    return true;
}

bool Terrain::isTransparent(int x, int y)
{
    Uint8* pixels;
    Uint32 pixel;
    Uint8  r, g, b, a;
    
    if (SDL_MUSTLOCK(surface))
    {
        SDL_LockSurface(surface);
    }
    
    if (x >= surface->w || y >= surface->h || x < 0 || y < 0)
    {
        if (SDL_MUSTLOCK(surface))
        {
            SDL_UnlockSurface(surface);
        }
        
        return true;
    }
    
    pixels = (Uint8*) surface->pixels + y * surface->pitch + x *
             surface->format->BytesPerPixel;
    pixel  = *(Uint32*) pixels;
    SDL_GetRGBA(pixel, surface->format, &r, &g, &b, &a);
    
    if (SDL_MUSTLOCK(surface))
    {
        SDL_UnlockSurface(surface);
    }
    
    return (a <= 0x16);
}

glm::vec2 Terrain::getNormalAt(int x, int y)
{
    glm::vec2 average;
    
    average = glm::vec2(0.0f);
    for(int i = -3; i <= 3; i++)
    {
        for (int j = -3; j <= 3; j++)
        {
            if (isTransparent(x + i, y + j) == false)
            {
                average.x -= i;
                average.y += j;
            }
        }
    }
    
    return average;
}

void Terrain::hole(int x, int y, int radius)
{
    Uint32* pixels;
    int     width;
    int     height;
    
    pixels = NULL;
    width  = (int) ((float) radius) * 1.5f;
    height = radius;
    
    if (radius <= 0)
    {
		return;
	}
    
    if (SDL_MUSTLOCK(surface))
    {
		SDL_LockSurface(surface);
	}
    
    pixels  = (Uint32*) surface->pixels;
	for (int i = -width; i <= width; i++)
	{
		for (int j = -height; j <= height; j++)
		{
			if ((i * i * height * height + j * j * width * width <=
                width * width * height * height) &&
                ((j * surface->w + i) < dimensions.x * dimensions.y))
            {
                pixels[((y + j) * surface->w) + (x + i)] = 0x00000000;
            }
		}
	}
	if (SDL_MUSTLOCK(surface))
    {
		SDL_UnlockSurface(surface);
    }
    
    updateTexture();
}

void Terrain::updateTexture()
{
    static bool first = true;
    
    glBindVertexArray(glVAO);
    glBindBuffer(GL_ARRAY_BUFFER, glVBO);
    glBindTexture(GL_TEXTURE_2D, glTex);
    
    if (first) /* allocate VRAM for the texture */
    {
        glTexImage2D(GL_TEXTURE_2D, 0, colorspace, surface->w, surface->h, 0,
                     colorspace, GL_UNSIGNED_BYTE, surface->pixels);
        first = false;
    }
    else /* reuse the allocated memory - new texture has same parameters */
    {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, surface->w, surface->h,
                        colorspace, GL_UNSIGNED_BYTE, surface->pixels);
    }
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}
