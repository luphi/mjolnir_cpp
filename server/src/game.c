/* C standard library */
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/* Mjolnir server stuff */
#include "game.h"
#include "globals.h"

#define GET_TICKS() clock() / ( CLOCKS_PER_SEC / 1000)

extern Globals* g;

Game* game_create()
{
    Game* game;
    
    game = (Game*) malloc(sizeof(Game));
    
    return game;
}

void game_destroy(Game* game)
{
    /* error case */
    if (game == NULL)
    {
        return;
    }
    
    game_stop(game);
    free(game);
}

void game_start(Game* game)
{
    
}
void game_stop(Game* game)
{
    
}

void* game_thread(void* param)
{
    
    
    return NULL;
}
