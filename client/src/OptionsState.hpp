#ifndef _OPTIONSSTATE_H_
#define _OPTIONSSTATE_H_

#include "Camera.hpp"
#include "Globals.hpp"
#include "Input.hpp"
#include "State.hpp"

class OptionsState : public State
{
public:

    OptionsState();
    ~OptionsState();
    
    void draw();
    void resize();
    void poll();
    void step(int dt);
    int  nextState();
    
private:

	Globals* globals;
	Camera*  camera;
	Input*   input;
    int      stateChange;
    
};

#endif
