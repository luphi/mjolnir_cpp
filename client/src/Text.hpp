#ifndef _TEXT_H_
#define _TEXT_H_

#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "Sprite.hpp"

class Text : public Sprite
{
public:
    
    Text();
    ~Text();
    
    void draw();
    void init(std::string text, int r, int g, int b);
    void setText(std::string text);
    
private:

    void updateTexture();
    
    TTF_Font*   font;
    SDL_Color   color;
    std::string str;
};

#endif
