#ifndef _UI_H_
#define _UI_H_

/* additional libraries */
#include <pthread.h>
#include <ncurses.h>

/* due to the unique way curses works, the UI is run from a single thread */
/* which takes instructions from a FIFO shared by other threads */

typedef struct ui_event_t
{
    unsigned char op;
    unsigned char buf[256];
    unsigned char len;
} UIEvent;

enum ui_opcodes
{
    UI_NO_OP,
    PRINT,
    PRINT_SUCCESS,
    PRINT_ERROR,
    PRINT_WARNING,
    PRINT_STATUS_GOOD,
    PRINT_STATUS_BAD
};

typedef struct ui_node_t UINode;
typedef struct ui_node_t
{
    UINode* next;
    UIEvent event;
} UINode;

typedef struct ui_fifo_t
{
    UINode*         first;
    UINode*         last;
    pthread_mutex_t mutex;
} UIFIFO;

typedef struct ui_t
{
    WINDOW* main_win;
    WINDOW* output_win;
    WINDOW* input_win;
    WINDOW* status_win;
    WINDOW* output_win_box;
    WINDOW* input_win_box;
    
    UIFIFO* fifo;
} UI;

UI*     ui_create();
void    ui_destroy(UI* ui);
void    ui_create_output_win(UI* ui);
void    ui_create_input_win(UI* ui);
void    ui_create_status_win(UI* ui);
void    ui_window_too_small(UI* ui);
void    ui_print_timestamp(UI* ui);
void    ui_print(UI* ui, char* str);
void    ui_print_success(UI* ui, char* str);
void    ui_print_error(UI* ui, char* str);
void    ui_print_warning(UI* ui, char* str);
void    ui_print_status_good(UI* ui, char* str);
void    ui_print_status_bad(UI* ui, char* str);
void    ui_print_splash(UI* ui);
void    ui_handle_resize(UI* ui);
int     ui_keyboard_input(UI* ui, char* buffer);
void    ui_refresh(UI* ui);

UIFIFO* ui_fifo_create(UI* ui);
void    ui_fifo_destroy(UIFIFO* fifo);
void    ui_fifo_push_event(UIFIFO* fifo, UIEvent event);
UIEvent ui_fifo_pop_event(UIFIFO* fifo);

#endif
