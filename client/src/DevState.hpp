#ifndef _DEVSTATE_H_
#define _DEVSTATE_H_

#include "Camera.hpp"
#include "Collisions.hpp"
#include "Event.hpp"
#include "Globals.hpp"
#include "HUD.hpp"
#include "Input.hpp"
#include "Socket.hpp"
#include "Sprite.hpp"
#include "State.hpp"
#include "Terrain.hpp"
#include "Text.hpp"
#include "TextInput.hpp"
#include "Viking.hpp"

class DevState : public State
{
public:

    DevState();
    ~DevState();
    
    void draw();
    void resize();
    void poll();
    void step(int dt);
    int  nextState();

    Globals*    globals;
    Viking*     viking;
    EventFIFO*  fifo;
    HUD*        hud;
    Camera*     camera;
    Terrain*    terrain;
    Collisions* collisions;
    Input*      input;
    Text*       text;
    Socket*     socket;
    TextInput*  textInput;
    int         stateChange;

private:

    void handleEvents();
    void handleInput(char* input);
};

#endif
