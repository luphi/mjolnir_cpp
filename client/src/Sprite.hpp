#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

#include "Drawable.hpp"

#pragma pack(push, 1)
struct BMPFileHeader
{
    unsigned short signature;
    unsigned int   fileSize;
    unsigned short reserved1;
    unsigned short reserved2;
    unsigned int   offsetToPixels;
};

struct BMPInfoHeader
{
    unsigned int   headerSize;
    unsigned int   width;
    unsigned int   height;
    unsigned short planes;
    unsigned short bpp;
    unsigned int   compression;
    unsigned int   imageSize;
    unsigned int   xPixelsPerMeter;
    unsigned int   yPixelsPerMeter;
    unsigned int   colorsUsed;
    unsigned int   colorsImportant;
};
#pragma pack(pop)

class Sprite : public Drawable
{
public:

    Sprite();
    ~Sprite();
    
    bool init(const char* file);
    bool initBMP(const char* file);
    virtual void draw();
    void mirror(); /* flip the X axis */
    
    /* inherited from Drawable */
    /* glm::vec2 position;       X and Y coordinates */
    /* glm::vec2 dimensions;     width and height */
    
    GLfloat   glRotation; /* rotation in degrees */
    
protected:

    unsigned char* pixels; /* actual pixel data */
    int            pixelsLen; /* length of pixels in bytes */
    GLsizei        glCount; /* number of verties */
    GLenum         glMode; /* draw mode (line, triangle, quad, etc.) */
    GLuint         glVAO; /* vertex array object */
    GLuint         glVBO; /* vertex buffer object */
    GLuint         glEBO; /* element buffer object */
    GLuint         glTex; /* texture ID */
    bool           mirrored;
    
    void loadBMP(const char* file);
};

#endif
