#ifndef _EVENT_H_
#define _EVENT_H_

/* C standard library */
#include <stdint.h>

/* additional libraries */
#include <pthread.h>

typedef struct event_t
{
    uint16_t op;       /* operation code */
    uint16_t id;       /* player ID, originator or recipient of */
    uint16_t len;      /* length of data in bytes */
    uint8_t  buf[512]; /* data/payload */
} Event;

enum event_opcodes
{
    NO_OP,
    PING,
    PONG,
    VIKING_CONNECT,
    VIKING_DISCONNECT,
    VIKING_NAME,
    VIKING_CHOICE,
    WEAPON_SUBTYPE,
    MESSAGE_ALL,
    MESSAGE_TEAM,
    SERVER_FULL,
    SERVER_SHUTDOWN
};

typedef struct event_node_t EventNode;
typedef struct event_node_t
{
    EventNode* next;
    Event      event;
} EventNode;

typedef struct Eventfifo_t
{
    EventNode*      first;
    EventNode*      last;
    pthread_mutex_t mutex;
} EventFIFO;

EventFIFO* fifo_create();
void       fifo_destroy(EventFIFO* fifo);
void       fifo_push_event(EventFIFO* fifo, Event event);
Event      fifo_pop_event(EventFIFO* fifo);

void event_pack_ping(Event* ev);
void event_pack_viking_connect(Event* ev, uint16_t id);
void event_pack_viking_name(Event* ev, uint16_t id, char* name);

void event_unpack_pong(Event ev, uint16_t* id);
void event_unpack_viking_name(Event ev, uint16_t* id, char* name);
void event_unpack_viking_choice(Event ev, uint16_t* id, uint16_t* choice);
void event_unpack_weapon_subtype(Event ev, uint16_t* id, uint16_t* subtype);
void event_unpack_message_all(Event ev, uint16_t* id, char* message);
void event_unpack_message_team(Event ev, uint16_t* id, char* message);

#endif
