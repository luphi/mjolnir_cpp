#include <cstddef> /* for NULL */

#include "MenuState.hpp"

MenuState::MenuState()
{
    globals    = NULL;
    collisions = NULL;
    camera     = NULL;
    background = NULL;
    input      = NULL;
    for (int i = 0; i < OPTION_COUNT; i++)
    {
        options[i]            = NULL;
        optionsHighlighted[i] = NULL;
    }
    index       = 0;
    stateChange = NO_CHANGE;
    
    globals     = Globals::Instance();
    collisions  = globals->collisions;
    camera      = globals->camera;
    input       = globals->input;
    /* if we're coming from a game, server, lobby, etc. state, the socket */
    /* will have been connected but we don't need it now */
    globals->socket->leave();
    
    for (int i = 0; i < OPTION_COUNT; i++)
    {
        options[i]            = new Text();
        optionsHighlighted[i] = new Text();
    }
    
    options[FIND_SERVERS]->init("FIND SERVERS", 255, 255, 255);
    optionsHighlighted[FIND_SERVERS]->init("FIND SERVERS", 81, 187, 206);
    options[DEVMODE]->init("DEVMODE", 255, 255, 255);
    optionsHighlighted[DEVMODE]->init("DEVMODE", 81, 187, 206);
    options[OPTIONS]->init("OPTIONS", 255, 255, 255);
    optionsHighlighted[OPTIONS]->init("OPTIONS", 81, 187, 206);
    options[QUIT]->init("QUIT", 255, 255, 255);
    optionsHighlighted[QUIT]->init("QUIT", 81, 187, 206);
    
    background = new Sprite();
    if (background->init("media/misc/hammer.png") == false)
    {
        background = NULL;
    }
    
    resize();
}

MenuState::~MenuState()
{
    if (background != NULL)
    {
        delete background;
        background = NULL;
    }
    
    for (int i = 0; i < OPTION_COUNT; i++)
    {
        if (options[i] != NULL)
        {
            delete options[i];
            options[i] = NULL;
        }
        
        if (optionsHighlighted[i] != NULL)
        {
            delete optionsHighlighted[i];
            optionsHighlighted[i] = NULL;
        }
    }
}

void MenuState::draw()
{
    if (background != NULL)
    {
        background->draw();
    }
    
    for (int i = 0; i < OPTION_COUNT; i++)
    {
        if ((i == index) && (optionsHighlighted[i] != NULL))
        {
            optionsHighlighted[i]->draw();
        }
        else if (options[i] != NULL)
        {
            options[i]->draw();
        }
    }
}

void MenuState::resize()
{
	float vSpacing;
    float hSpacing;
    
    vSpacing = globals->vidHeight / 16.0f;
    hSpacing = globals->vidWidth  / 16.0f;
    
    background->dimensions.x = globals->vidHeight / 3.0f;
    background->dimensions.y = globals->vidHeight / 3.0f;
    background->position.x   = globals->vidWidth  - background->dimensions.x
                               - hSpacing;
    background->position.y   = globals->vidHeight - background->dimensions.y
                               - vSpacing;
    
    for (int i = 0; i < OPTION_COUNT; i++)
    {
        options[i]->position.x            = hSpacing;
        optionsHighlighted[i]->position.x = hSpacing;
        options[i]->position.y            = vSpacing * (float) (i + 1);
        optionsHighlighted[i]->position.y = vSpacing * (float) (i + 1);
    }
    
    camera->resize();
}

void MenuState::poll()
{
    int code;
    
    if (input == NULL)
    {
        return;
    }
    
    while ((code = input->poll()) != NO_INPUT)
    {
        switch (code)
        {
            case MOUSEMOVED:
                for (int i = 0; i < OPTION_COUNT; i++)
                {
                    if (collisions->isPointInDrawable(globals->mouse,
                                                      options[i]))
                    {
                        index = i;
                        break;
                    }
                }
                break;
            case MOUSELEFT_DOWN:
                for (int i = 0; i < OPTION_COUNT; i++)
                {
                    if (collisions->isPointInDrawable(globals->mouse,
                                                      options[i]))
                    {
                        switch (index)
                        {
                            case FIND_SERVERS:
                                stateChange = SERVER_STATE;
                                break;
                            case DEVMODE:
                                stateChange = DEV_STATE;
                                break;
                            case OPTIONS:
                                stateChange = OPTIONS_STATE;
                                break;
                            case QUIT:
                                stateChange = EXIT;
                                break;
                        }
                    }
                }
                break;
            case UP_DOWN:
                if (index >= 1)
                {
                    index--;
                }
                break;
            case DOWN_DOWN:
                if (index < OPTION_COUNT - 1)
                {
                    index++;
                }
                break;
            case ENTER_DOWN:
                switch (index)
                {
                    case FIND_SERVERS:
                        stateChange = SERVER_STATE;
                        break;
                    case DEVMODE:
                        stateChange = DEV_STATE;
                        break;
                    case OPTIONS:
                        stateChange = OPTIONS_STATE;
                        break;
                    case QUIT:
                        stateChange = EXIT;
                        break;
                }
                break;
            case RESIZE:
                resize();
                break;
            case ESCAPE_DOWN:
            case QUIT_EVENT:
                stateChange = EXIT;
                break;
            default:
                break;
        }
    }
}

void MenuState::step(int dt)
{
    
}

int MenuState::nextState()
{
    return stateChange;
}
