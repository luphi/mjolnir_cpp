#include <cstddef> /* for NULL */
#include <cstdint> /* for fixed-width types */
#include <cstring> /* for bzero, bcopy, memcpy */

#include <arpa/inet.h> /* for inet_aton */
#include <netdb.h> /* for hosten */
#include <pthread.h>
#include <sys/socket.h> /* for socket */

#include "Event.hpp"
#include "Globals.hpp"
#include "Socket.hpp"

Socket::Socket()
{
    fd        = -1;
    receiving = false;
    
    pthread_mutex_init(&mutex, NULL);
}

Socket::~Socket()
{
    leave();
    pthread_mutex_destroy(&mutex);
}

bool Socket::join(std::string host, int port)
{
    struct sockaddr_in serv_addr;
    struct hostent*    resolv;
    
    /* error case */
    if (Globals::Instance()->socket == NULL)
    {
        return false;
    }
    
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0)
    {
        printf("Failed to create socket\n");
        return false;
    }
    
    resolv = gethostbyname(host.c_str());
    if (resolv == NULL)
    {
        printf("Failed to resolve remote host\n");
        return false;
    }
    
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(resolv->h_addr, &serv_addr.sin_addr.s_addr, resolv->h_length);
    serv_addr.sin_port = htons(port);
    
    if (connect(fd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) != 0)
    {
        printf("Failed to connect to remote host\n");
        fd = -1;
        return false;
    }
    
    receiving = true;
    pthread_create(&thread, NULL, receiveThread, Globals::Instance());
    
    return true;
}

void Socket::leave()
{
    if (fd != -1)
    {
        shutdown(fd, 2);
    }
    
    pthread_mutex_lock(&mutex);
    receiving = false;
    pthread_mutex_unlock(&mutex);
    pthread_join(thread, NULL);
}

void Socket::sendEvent(Event ev)
{
	char* buf;
    int   len;
    
    /* Size of opcode, ID, length, and twice the buffer.  Why twice?  Strings */
    /* are sent as 16-bit integers rather than 8-bit ASCII so the extra room */
    /* is needed. */
    len = sizeof(ev.op)  + sizeof(ev.id) + sizeof(ev.len) + ev.len * 2;
    buf = (char*) malloc(len);
    serialize(ev, buf, &len);    
    len = send(fd, buf, len, MSG_NOSIGNAL);
    free(buf);
    buf = NULL;
    
    //SDL_Delay(100);
}

void* Socket::receiveThread(void* param)
{
    Globals* g;
    Event    event;
    char     buffer[1024];
    int      bytes;
    
    if (param == NULL)
    {
        return NULL;
    }
    
    g     = (Globals*) param;
    bytes = 0;
    
    while (g->socket->receiving)
    {
        bytes = recv(g->socket->fd, buffer, 1024, 0);
        if (bytes > 0)
        {
            unserialize(&event, buffer);
            
            if (event.op == PING)
            {
                g->fifo->packPong(&event);
                g->socket->sendEvent(event);
            }
            else
            {
				printf("Received something other than PING\n");
                g->fifo->pushEvent(event);
            }
        }
        /* if an error occurred */
        else if (bytes < 0)
        {
            /* exit this loop and thread */
            break;
        }
        
        /* sleep so we don't eat up all of the CPU */
        SDL_Delay(10); /* sleep 10 milliseconds */
    }
    
    return NULL;
}

void Socket::serialize(Event ev, char* buf, int* len)
{
	uint16_t op;
	uint16_t id;
	uint16_t data_len;
	
	op       = htons(ev.op);
	memcpy(buf, &op, sizeof(uint16_t));
	buf     += sizeof(uint16_t);
	id       = htons(ev.id);
	memcpy(buf, &id, sizeof(uint16_t));
	buf     += sizeof(uint16_t);
	data_len = htons(ev.len);
	memcpy(buf, &data_len, sizeof(uint16_t));
	buf     += sizeof(uint16_t);
	*len     = sizeof(op) + sizeof(id) + sizeof(data_len) + ev.len;
    
    switch (ev.op)
    {
		case VIKING_NAME:
		case MESSAGE_ALL:
		case MESSAGE_TEAM:
			{
				char*     byte_string; /* the host order string as bytes */
				uint16_t* word_string; /* the resulting string as words */
				int       i; /* index in word_string */
				
				/* same length of characters but twice the bits */
				word_string = (uint16_t*) malloc(sizeof(uint16_t) * ev.len);
				/* buf is the payload string */
				byte_string = (char*) ev.buf;
				
				for (i = 0; i < ev.len; i++)
				{
					/* convert the host order 8-bit integer (letter) at */
					/* location byte_string to a network order word (16-bit) */
					word_string[i] = htons((uint16_t) byte_string[i]);
				}
				
				memcpy(buf, word_string, sizeof(uint16_t) * ev.len);
				*len += ev.len;
				free(word_string);
				word_string = NULL;
			}
			break;
		case VIKING_CHOICE:
			{
				uint16_t net_order;
				
				net_order = htons(*ev.buf);
				memcpy(buf, &net_order, ev.len);
			}
			break;
		case NO_OP:
		default:
			break;
	}
}

void Socket::unserialize(Event* ev, char* buf)
{
	uint16_t op;
	uint16_t id;
	uint16_t len;
	
	memcpy(&op, buf, sizeof(uint16_t));
	op   = ntohs(op);
	buf += sizeof(uint16_t);
	memcpy(&id, buf, sizeof(uint16_t));
	id   = ntohs(id);
	buf += sizeof(uint16_t);
	memcpy(&len, buf, sizeof(uint16_t));
	len  = ntohs(len);
	buf += sizeof(uint16_t);
	
	(*ev).op  = op;
	(*ev).id  = id;
	(*ev).len = len;
	
	switch (op)
	{
		case VIKING_NAME:
		case MESSAGE_ALL:
		case MESSAGE_TEAM:
			{
				char*     byte_string; /* the resulting string as bytes */
				uint16_t* word_string; /* the network order string as words */
				int       i; /* index in byte_string */
				
				/* same length of characters but half the bits */
				byte_string = (char*) malloc(sizeof(char) * len);
				/* buf is currently pointing at the payload */
				word_string = (uint16_t*) buf;
				
				for (i = 0; i < len; i++)
				{
					/* convert the network order 16-bit integer (letter) at */
					/* location word_string to a host order byte (char) */
					byte_string[i] = (char) ntohs(*word_string);
					word_string++; /* move the pointer to the next character */
				}
				
				memcpy((*ev).buf, byte_string, len);
				free(byte_string);
				byte_string = NULL;
			}
			break;
		case VIKING_CHOICE:
			
			break;
		case NO_OP:
		default:
			break;
	}
}
