/* C standard library */
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/* Mjolnir server stuff */
#include "globals.h"
#include "score.h"

#define GET_TICKS() clock() / ( CLOCKS_PER_SEC / 1000)

extern Globals* g;

Score* score_create()
{
    Score* score;
    
    score = (Score*) malloc(sizeof(Score));
    
    return score;
}

void score_destroy(Score* score)
{
    /* error case */
    if (score == NULL)
    {
        return;
    }
    
    score_stop(score);
    free(score);
}

void score_start(Score* score)
{
    
}
void score_stop(Score* score)
{
    
}

void* score_thread(void* param)
{
    
    
    return NULL;
}
