#ifndef _PROJECTILE_H_
#define _PROJECTILE_H_

#include "Circle.hpp"
#include "Drawable.hpp"
#include "Physical.hpp"
#include "Sprite.hpp"

class Projectile : public Drawable
{
public:
    
    Projectile();
    ~Projectile();
    
    virtual bool init() { return true; }
    virtual void step(int dt);
    void draw();
    
    Physical* physical;
    Sprite*   sprite;
    Circle*   circle;
    bool      active;
    
    /* inherited from Drawable */
    /* glm::vec2 position;       X and Y coordinates */
    /* glm::vec2 dimensions;     width and height */
};

class ProjectileBigfoot1 : public Projectile
{
public:
    bool init();
    void step(int dt);
};

class ProjectileBigfoot2 : public Projectile
{
public:
    bool init();
};

#endif
