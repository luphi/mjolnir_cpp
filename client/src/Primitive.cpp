#include <cstddef> /* for NULL */
#include <cstdio>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Globals.hpp"
#include "Primitive.hpp"

Primitive::Primitive()
{
    /* default values */
    glCount    = 0;
    glMode     = GL_TRIANGLES;
    glVAO      = 0;
    glVBO      = 0;
    position   = glm::vec2(0.0f);
    dimensions = glm::vec2(0.0f);
    color      = glm::vec3(1.0f);
}

Primitive::~Primitive()
{
    glDeleteVertexArrays(1, &glVAO);
    glDeleteBuffers(1, &glVBO);
}

void Primitive::init(int shape)
{
    GLfloat* glVertices; /* vertices */
    
    glVertices = NULL;
    switch (shape)
    {
        case PRIMITIVE_QUAD:
            /* This is a square made from two triangles */
            glVertices     = new GLfloat[12];
            glVertices[0]  = -0.5f;  glVertices[1]  = -0.5f;
            glVertices[2]  = -0.5f;  glVertices[3]  =  0.5f;
            glVertices[4]  =  0.5f;  glVertices[5]  =  0.5f;
            glVertices[6]  = -0.5f;  glVertices[7]  = -0.5f;
            glVertices[8]  =  0.5f;  glVertices[9]  = -0.5f;
            glVertices[10] =  0.5f;  glVertices[11] =  0.5f;
            glMode         = GL_TRIANGLES;
            glCount        = 6;
            break;
        case PRIMITIVE_CIRCLE:
            glVertices     = new GLfloat[32];
            for (int i = 0; i < 32; i += 2)
            {
                glVertices[i]     = 0.5f * cos(i * 3.14159 / 16.0f);
                glVertices[i + 1] = 0.5f * sin(i * 3.14159 / 16.0f);
            }
            glMode         = GL_LINE_LOOP;
            glCount        = 16;
            break;
    }
    
    glGenVertexArrays(1, &glVAO);
    glBindVertexArray(glVAO);

    glGenBuffers(1, &glVBO);
    glBindBuffer(GL_ARRAY_BUFFER, glVBO);
    glBufferData(GL_ARRAY_BUFFER, glCount * sizeof(GLfloat) * 2, glVertices,
        GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
    delete[] glVertices;
    glVertices = NULL;
}

void Primitive::draw()
{
    Globals*  globals;
    glm::mat4 model;

    globals  = Globals::Instance();

    model = glm::translate(model, glm::vec3(position, 0.0f));
    /* the next translation is due to a difference in coordinate systems */
    /* between what this game uses and OpenGL - we consider the top left */
    /* corner to be (0, 0) but the sprite is centered in normalized */
    /* device coordinates such that (0, 0) would be the center of the sprite */
    /* if not compensated for with this translation */
    model = glm::translate(model, glm::vec3(dimensions / 2.0f, 0.0f));
    model = glm::scale(model, glm::vec3(dimensions, 1.0f));

    glUseProgram(globals->glPrimitiveProgram);
    glUniformMatrix4fv(glGetUniformLocation(globals->glPrimitiveProgram,
        "primitiveModel"), 1, GL_FALSE, glm::value_ptr(model));
    glUniform3fv(glGetUniformLocation(globals->glPrimitiveProgram, "color"), 1,
        glm::value_ptr(color));

    glBindVertexArray(glVAO);
    glDrawArrays(glMode, 0, glCount);
    glUseProgram(0);
}
