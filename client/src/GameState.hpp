#ifndef _GAMESTATE_H_
#define _GAMESTATE_H_

#include "Camera.hpp"
#include "Collisions.hpp"
#include "Event.hpp"
#include "Globals.hpp"
#include "HUD.hpp"
#include "Input.hpp"
#include "Socket.hpp"
#include "State.hpp"
#include "Terrain.hpp"
#include "Text.hpp"
#include "TextInput.hpp"
#include "Viking.hpp"

class GameState : public State
{
public:

    GameState();
    ~GameState();
    
    void draw();
    void resize();
    void poll();
    void step(int dt);
    int  nextState();
    
    Globals*    globals;
    EventFIFO*  fifo;
    HUD*        hud;
    Camera*     camera;
    Terrain*    terrain;
    Collisions* collisions;
    Input*      input;
    Text*       text;
    Socket*     socket;
    TextInput*  textInput;
    int         stateChange;
    
    std::list<Viking*> vikings;
    Viking*            viking1; /* the local user */
    bool               onTerrain;
    bool               movingRight;
    bool               movingLeft;

private:

    void handleEvents();
};

#endif
