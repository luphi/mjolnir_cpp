#ifndef _MENUSTATE_H_
#define _MENUSTATE_H_

#include "Camera.hpp"
#include "Collisions.hpp"
#include "Globals.hpp"
#include "Input.hpp"
#include "Sprite.hpp"
#include "State.hpp"
#include "Text.hpp"

enum menu_options
{
    FIND_SERVERS,
    DEVMODE,
    OPTIONS,
    QUIT,
    OPTION_COUNT
};

class MenuState : public State
{
public:

    MenuState();
    ~MenuState();
    
    void draw();
    void resize();
    void poll();
    void step(int dt);
    int  nextState();

private:

    Globals*    globals;
    Collisions* collisions;
    Camera*     camera;
    Sprite*     background;
    Input*      input;
    Text*       options[OPTION_COUNT];
    Text*       optionsHighlighted[OPTION_COUNT];
    int         index;
    int         stateChange;
};

#endif
