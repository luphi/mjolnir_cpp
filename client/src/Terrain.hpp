#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include <vector>

#include <glm/glm.hpp>
#include <SDL2/SDL.h>

#include "Sprite.hpp"

class Terrain : public Sprite
{
public:

    Terrain();
    ~Terrain();
    
    bool init(const char* file);
    
    bool      isTransparent(int x, int y);
    glm::vec2 getNormalAt(int x, int y);
    void      hole(int x, int y, int radius);
    
    std::vector<glm::vec2> dropPoints;
    
    /* inherited from Drawable */
    /* glm::vec2 position;       X and Y coordinates */
    /* glm::vec2 dimensions;     width and height */

private:

    void updateTexture();

    SDL_Surface* surface;
    GLenum       colorspace;
};

#endif
