#ifndef _COLLISIONS_H_
#define _COLLISIONS_H_

#include <glm/glm.hpp>

#include "Drawable.hpp"
#include "Viking.hpp"

class Collisions
{
public:

    Collisions();
    ~Collisions();
    
    static bool isPointInDrawable(glm::vec2 point, Drawable* draw);
    static bool isVikingOnTerrain(Viking* viking);
    static bool areVikingTerrainColliding(Viking* viking);
    static bool areCircleTerrainColliding(Circle* circle);
    static bool raycastTerrain(glm::vec2& point, glm::vec2 ray);

private:

    static bool areDrawablesColliding(Drawable* one, Drawable* two);
};

#endif
