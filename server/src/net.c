/* C standard library */
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/* additional libraries */
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

/* Mjolnir server stuff */
#include "event.h"
#include "globals.h"
#include "log.h"
#include "viking.h"
#include "net.h"
#include "ui.h"

extern Globals* g;

void net_serialize(Event ev, char* buffer, int* len);
void net_unserialize(Event* ev, char* buffer, int* len, char* buffer_end);

/* returns the file descriptor of the server socket, or -1 on error */
ServerSocket* net_server_socket_create(char* port)
{
    ServerSocket* server;
    
    log_enter(g->log, __func__);
    
    server = (ServerSocket*) malloc(sizeof(ServerSocket));
    FD_ZERO(&server->socket_set);
    server->socket_listener = 0;
    server->running         = 0;
    pthread_mutex_init(&server->mutex, NULL);
    
    log_exit(g->log, __func__);
    
    return server;
}

void net_server_socket_destroy(ServerSocket* server)
{
    /* error case */
    if (server == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    net_server_socket_stop(server);
    net_server_socket_close(server);
    
    pthread_mutex_destroy(&server->mutex);
    free(server);
    
    log_exit(g->log, __func__);
}

void net_server_socket_close(ServerSocket* server)
{
    /* error cases */
    if ((server == NULL) || (server->socket_listener < 0))
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    close(server->socket_listener);
    
    log_exit(g->log, __func__);
}

int net_server_socket_start(ServerSocket* server, char* port)
{
    struct addrinfo  hints;
    struct addrinfo* ai;
    struct addrinfo* p;
    int              ret;
    
    /* error case */
    if (server == NULL)
    {
        return -1;
    }
    
    log_enter(g->log, __func__);
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family         = AF_UNSPEC;
    hints.ai_socktype       = SOCK_STREAM;
    hints.ai_flags          = AI_PASSIVE;
    if ((ret = getaddrinfo(NULL, port, &hints, &ai)) != 0)
    {
        char str[64];
        sprintf(str, "Failed to translate host info (i.e. port) into a computer"
            "-readable structure: %s", gai_strerror(ret));
        ui_print_error(g->ui, str);
        free(server);
        return -2;
    }
    
    if (ai == NULL)
    {
        ui_print_error(g->ui, "Could not create any socket structures to bind");
        free(server);
        return -3;
    }
    
    /* cycle through all the possible structures and try 'em all */
    for (p = ai; p != NULL; p = p->ai_next)
    {
        int yes;
        
        /* create the socket, if possible */
        server->socket_listener = socket(p->ai_family, p->ai_socktype,
            p->ai_protocol);
        if (server->socket_listener < 0)
        {
            /* socket() returns -1 on error, so move onto the next one */
            continue;
        }
        
        setsockopt(server->socket_listener, SOL_SOCKET, SO_REUSEADDR, &yes,
            sizeof(int));
        
        /* attempt to bind the socket */
        if (bind(server->socket_listener, p->ai_addr, p->ai_addrlen) < 0)
        {
            /* bind() returns -1 on error, meaning it could not be bound */
            close(server->socket_listener);
            continue;
        }
        
        /* if we get here, we found one that works */
        break;
    }
    
    freeaddrinfo(ai);
    
    /* start listening with a backlog of 10 possible waiting connections */
    if (listen(server->socket_listener, 10) == -1)
    {
        ui_print_error(g->ui, "Socket failed to listen");
        close(server->socket_listener);
        server->socket_listener = -1;
        return -4;
    }
    
    /* add the listening (server) socket to the set of sockets so we can */
    /* monitor it in the server's thread along with client sockets */
    FD_SET(server->socket_listener, &server->socket_set);
    
    pthread_mutex_lock(&server->mutex);
    server->running = 1;
    pthread_mutex_unlock(&server->mutex);
    pthread_create(&server->thread, NULL, net_server_thread, NULL);
    
    log_exit(g->log, __func__);
    
    return 0;
}

void net_server_socket_stop(ServerSocket* server)
{
    /* error case */
    if (server == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    pthread_mutex_lock(&server->mutex);
    server->running = 0;
    pthread_mutex_unlock(&server->mutex);
    pthread_join(server->thread, NULL);
    
    net_server_socket_close(server);
    
    log_exit(g->log, __func__);
}

void net_client_socket_close(Viking* viking)
{
    /* error case */
    if (viking == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    close(viking->socket_fd);
    
    log_exit(g->log, __func__);
}

void net_send_event(Viking* viking, Event event)
{
	char* buf;
	int   len;
	
	/* error case */
    if (viking == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    /* Size of opcode, ID, length, and twice the buffer.  Why twice?  Strings */
    /* are sent as 16-bit integers rather than 8-bit ASCII so the extra room */
    /* is needed. */
    len = sizeof(event.op)  + sizeof(event.id) + sizeof(event.len) +
		  event.len * 2;
    buf = (char*) malloc(len);
    net_serialize(event, buf, &len);
    send(viking->socket_fd, buf, len, MSG_NOSIGNAL);
    free(buf);
    buf = NULL;
    
    log_exit(g->log, __func__);
}

void net_send_event_all(Event event)
{
    int i;
    
    log_enter(g->log, __func__);
    
    for (i = 0; i < g->viking_max; i++)
    {
		net_send_event(g->vikings[i], event);
    }
    
    log_exit(g->log, __func__);
}

void net_send_event_all_but(Event event, int id_to_exclude)
{
    int i;
    
    log_enter(g->log, __func__);
    
    for (i = 0; i < g->viking_max; i++)
    {
        if (i != id_to_exclude)
        {
            net_send_event(g->vikings[i], event);
        }
    }
    
    log_exit(g->log, __func__);
}

void* net_server_thread(void* param)
{
    ServerSocket*           server;
    struct timeval          timeout;
    char                    buffer[1024];
    int                     len; /* number of bytes available to read */
    int                     max_fd; /* highest file descriptor, for select() */
    int                     i; /* generic iterator */
    int                     j; /* generic iterator */
    int                     new_fd; /* temp descriptor for new connections */
    struct sockaddr_storage addr;
    socklen_t               addr_len;
    fd_set                  set; /* select()'s return value, kind of */
    
    /* error case */
    if (g->server_socket == NULL)
    {
        return NULL;
    }
    
    log_enter(g->log, __func__);
    
    server          = g->server_socket;
    timeout.tv_sec  = 0;
    timeout.tv_usec = 10000; /* 10 milliseconds */
    len             = 0;
    max_fd          = server->socket_listener;
    i               = 0;
    j               = 0;
    new_fd          = 0;
    addr_len        = 0;
    FD_ZERO(&set);
    
    ui_print_success(g->ui, "Server thread started");
    
    while (server->running)
    {
        /* make a copy of the master set because select() will modify it */
        set = server->socket_set;
        /* check the listening and client sockets for activity */
        if (select(max_fd + 1, &set, NULL, NULL, &timeout) == -1)
        {
            ui_print_error(g->ui, "The server socket's select() call was either"
                " given an invalid file descriptor or interrupted");
            pthread_mutex_lock(&server->mutex);
            server->running = 0;
            pthread_mutex_unlock(&server->mutex);
        }
        
        /* cycle through possible socket descriptors and look for data */
        for (i = 0; i <= max_fd; i++)
        {
            /* if there was activity on socket i */
            if (FD_ISSET(i, &set))
            {
                /* if the active socket is the listening socket */
                if (i == server->socket_listener)
                {
                    /* there is a new connection */
                    addr_len = sizeof(addr);
                    new_fd   = accept(server->socket_listener,
                                      (struct sockaddr*) &addr, &addr_len);
                    
                    /* if there was some error, don't go any further */
                    if (new_fd == -1)
                    {
                        ui_print_warning(g->ui, "Tried and failed to add a "
                            "new connection");
                    }
                    else
                    {
                        /* add it to the list, determine its ID, make sure */
                        /* we have room, and probably a few other things */
                        if (viking_add(new_fd) < 0)
                        {
                            /* failed, ignore this connection */
                            close(new_fd);
                            continue;
                        }
                        /* update the highest descriptor if necessary */
                        if (new_fd > max_fd)
                        {
                            max_fd = new_fd;
                        }
                        /* add it to the master set to monitor it */
                        FD_SET(new_fd, &server->socket_set);
                    }
                }
                else /* i != server->socket_listener */
                {
                    /* something happened on client socket i */
                    len = recv(i, buffer, 1024, 0);
                    
                    /* figure out which player sent data */
					for (j = 0; j < g->viking_count; j++)
					{
						if ((g->vikings[j] != NULL) &&
							(g->vikings[j]->socket_fd == i))
						{
							break;
						}
					}
                    
                    /* if there was an error or the client disconnected */
                    if (len == 0)
                    {
                        /* remove the player */
                        viking_remove(g->vikings[j]);
                        /* determine the new highest descriptor */
                        max_fd = server->socket_listener;
                        for (j = 0; j < g->viking_count; j++)
                        {
                            if ((g->vikings[j] != NULL) &&
                                (g->vikings[j]->socket_fd > max_fd))
                            {
                                max_fd = g->vikings[j]->socket_fd;
                            }
                        }
                        /* close the temporary descriptor */
                        close(i);
                        
                        /* remove the descriptor from master set */
                        FD_CLR(i, &server->socket_set);
                    }
                    /* if data was received without error */
                    else if (len > 0)
                    {
						Event ev;
						int   bytes;
						int   offset;
						
						offset = 0;
						while (offset < len)
						{
							/* decode the data as an event */
							/* buffer + offset points to an event */
							/* buffer + len points to the end of the buffered */
							/* data for bounds checking */
							net_unserialize(&ev, buffer + offset, &bytes,
											buffer + len);
							/* bytes is set to 65535 in the event that */
							/* something dangerous happened and we will opt */
							/* to ignore this connection's data this time */
							if (bytes == 65535)
							{
								offset = len; /* exit the loop */
								continue; /* skip the below steps */
							}								
							/* we'll set the ID based on the server's memory */
							/* just in case someone was messing with it */
							ev.id = j;
							/* push the event onto the FIFO and let the */
							/* active thread/state respond accordingly */
							fifo_push_event(g->event_fifo, ev);
							/* increase the offset such that buffer + offset */
							/* points to the start of the next event */
							offset += bytes;
						}
                    }
                    
                    memset(buffer, 0, 1024);
                }
            }
        }
        
        usleep(10000); /* sleep 10 milliseconds to spare some CPU cycles */
    }
    
    ui_print_success(g->ui, "Server thread stopped");
    
    log_exit(g->log, __func__);
    
    return NULL;
}

/******************************************************************************/

void net_serialize(Event ev, char* buf, int* len)
{
	uint16_t d;
	
	log_enter(g->log, __func__);
	
	d = htons(ev.op);
	memcpy(buf, &d, sizeof(uint16_t));
	buf += sizeof(uint16_t);
	d = htons(ev.id);
	memcpy(buf, &d, sizeof(uint16_t));
	buf += sizeof(uint16_t);
	d = htons(ev.len);
	memcpy(buf, &d, sizeof(uint16_t));
	buf += sizeof(uint16_t);
    
    switch (ev.op)
    {
		case VIKING_NAME:
			{
				char*     byte_string; /* the host order string as bytes */
				uint16_t* word_string; /* the resulting string as words */
				int       i; /* index in word_string */
				
				/* same length of characters but twice the bits */
				word_string = (uint16_t*) malloc(sizeof(uint16_t) * *len);
				/* buf is the payload string */
				byte_string = (char*) ev.buf;
				
				for (i = 0; i < *len; i++)
				{
					/* convert the host order 8-bit integer (letter) at */
					/* location byte_string to a network order word (16-bit) */
					word_string[i] = htons((uint16_t) *byte_string);
					byte_string++; /* move the pointer to the next character */
				}
				
				memcpy(buf, word_string, *len);
				*len = *len + strlen((char*) ev.buf);
				free(word_string);
				word_string = NULL;
			}
			break;
		case NO_OP:
		default:
			break;
	}
	
	log_exit(g->log, __func__);
}

void net_unserialize(Event* ev, char* buf, int* len, char* buffer_end)
{
	uint16_t op;       /* event op code */
	uint16_t id;       /* event ID */
	uint16_t data_len; /* event length, separate from buffer length */
	
	log_enter(g->log, __func__);
	
	/* bounds check */
	if (buf + 3 * sizeof(uint16_t) > buffer_end)
	{
		/* reading this data would exceed the incoming data, so don't try */
		*len = 65535;
		return;
	}
	
	memcpy(&op, buf, sizeof(uint16_t));
	op       = ntohs(op);
	buf     += sizeof(uint16_t);
	memcpy(&id, buf, sizeof(uint16_t));
	id       = ntohs(id);
	buf     += sizeof(uint16_t);
	memcpy(&data_len, buf, sizeof(uint16_t));
	data_len = ntohs(data_len);
	buf     += sizeof(uint16_t);
	
	/* second bounds check */
	if (buf + *len > buffer_end)
	{
		/* reading this data would exceed the incoming data, so don't try */
		*len = 65535;
		return;
	}
	
	(*ev).op  = op;
	(*ev).id  = id;
	(*ev).len = data_len;
	memset((*ev).buf, 0, 512);
	
	/* data length should never exceed 512 bytes */
	if (data_len > 512)
	{
		/* give up */
		*len = 65535;
		log_exit(g->log, __func__);
		return;
	}
	*len = sizeof(op) + sizeof(id) + sizeof(data_len) + data_len;
	
	switch (op)
	{
		/* events with strings */
		case VIKING_NAME:
		case MESSAGE_ALL:
		case MESSAGE_TEAM:
			{
				char*     byte_string; /* the resulting string as bytes */
				uint16_t* host_string; /* the network order string as words */
				int       i; /* index in byte_string */
				
				/* same length of characters but half the bits */
				byte_string = (char*) malloc(sizeof(char) * data_len);
				/* buf is currently pointing at the payload */
				host_string = (uint16_t*) buf;
				
				for (i = 0; i < data_len; i++)
				{
					/* convert the network order 16-bit integer (letter) at */
					/* location host_string to a host order byte (char) */
					byte_string[i] = (char) ntohs(host_string[i]);
				}
				
				memcpy((*ev).buf, byte_string, data_len);
				free(byte_string);
				byte_string = NULL;
				*len += data_len;
			}
			break;
		/* events for which ev.buf is a single uint16_t */
		case VIKING_CHOICE:
		case WEAPON_SUBTYPE:
			{
				uint16_t host_order;
				
				host_order = ntohs(*((uint16_t*) buf));
				memcpy((*ev).buf, &host_order, data_len);
			}
			break;
		case NO_OP:
		default:
			break;
	}
	
	log_exit(g->log, __func__);
}
