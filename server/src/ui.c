/* C standard library */
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/* additional libraries */
#include <ncurses.h>

/* Mjolnir server stuff */
#include "globals.h"
#include "log.h"
#include "ui.h"

extern Globals* g;

UI* ui_create()
{
    UI* ui;
    
    log_enter(g->log, __func__);
    
    ui                 = (UI*) malloc(sizeof(UI));
    ui->main_win       = NULL;
    ui->output_win     = NULL;
    ui->input_win      = NULL;
    ui->status_win     = NULL;
    ui->output_win_box = NULL;
    ui->input_win_box  = NULL;
    ui->fifo           = ui_fifo_create(ui);
    
    ui->main_win = initscr();
    if (ui->main_win == NULL)
    {
        perror("Main window creation error\n");
        return NULL;
    }
    
    if (has_colors() == FALSE)
    {
        perror("Your system doesn't support color (what year is it?)\n");
        return NULL;
    }
    
    /* prevent the system from displaying the characters on its own */
    noecho();
    /* allow user input with any key press, not just after hitting enter */
    cbreak();
    /* timeout if no key is hit after a tenth of a second to avoid blocking */
    halfdelay(1);
    keypad(ui->main_win, TRUE);
    
    start_color();
    use_default_colors();
    init_pair(1, -1,          -1);
    init_pair(2, COLOR_GREEN, -1);
    init_pair(3, COLOR_WHITE, -1);
    init_pair(4, COLOR_CYAN,  -1);
    init_pair(5, COLOR_RED,   -1);
    init_pair(6, COLOR_WHITE, COLOR_RED);
    
    if ((LINES < 24) || (COLS < 76))
    {
        ui_window_too_small(ui);
    }
    else
    {
        ui_create_output_win(ui);
        ui_create_input_win(ui);
        ui_create_status_win(ui);
        ui_print_splash(ui);
    }
    
    log_exit(g->log, __func__);
    
    return ui;
}

void ui_destroy(UI* ui)
{
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    ui_fifo_destroy(ui->fifo);
    
    endwin();
    free(ui);
    
    log_exit(g->log, __func__);
}

void ui_create_output_win(UI* ui)
{
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    ui->output_win_box = subwin(ui->main_win, (LINES * 7 / 8), COLS, 0, 0);
    box(ui->output_win_box, 0, 0);
    mvwaddch(ui->output_win_box, 0, (COLS / 2) - 6, ACS_RTEE); /* the -| char */
    wattron(ui->output_win_box, COLOR_PAIR(2));
    mvwaddstr(ui->output_win_box, 0, (COLS / 2) - 5, " Mjolnir ");
    wattroff(ui->output_win_box, COLOR_PAIR(2));
    mvwaddch(ui->output_win_box, 0, (COLS / 2) + 4, ACS_LTEE); /* the |- char */
    wrefresh(ui->output_win_box);
    ui->output_win = subwin(ui->output_win_box,
        (LINES * 7 / 8) - 2, COLS - 2, 1, 1);
    scrollok(ui->output_win, TRUE);
    
    log_exit(g->log, __func__);
}

void ui_create_input_win(UI* ui)
{
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    ui->input_win_box = subwin(ui->main_win,
        (LINES / 8), COLS, (LINES * 7 / 8), 0);
    box(ui->input_win_box, 0, 0);
    ui->input_win = subwin(ui->input_win_box,
        (LINES / 8) - 2, COLS - 2, (LINES * 7 / 8) + 1, 1);
    
    log_exit(g->log, __func__);
}

void ui_create_status_win(UI* ui)
{
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    ui->status_win = subwin(ui->main_win, 1, COLS, LINES - 1, 0);
    
    log_exit(g->log, __func__);
}

void ui_window_too_small(UI* ui)
{
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    wbkgd(ui->main_win, COLOR_PAIR(6));
    wattron(ui->main_win, A_BOLD);
    mvwaddstr(ui->main_win, (LINES / 2) - 1, (COLS / 2) - 3, "WINDOW");
    mvwaddstr(ui->main_win, (LINES / 2),     (COLS / 2) - 4, "TOO SMALL");
    wattroff(ui->main_win, A_BOLD);
    wrefresh(ui->main_win);
    wbkgd(ui->main_win, COLOR_PAIR(1));
    
    log_exit(g->log, __func__);
}

void ui_print_timestamp(UI* ui)
{
    time_t     raw_time;
    struct tm* loc_time;
    
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    time(&raw_time);
    loc_time = localtime(&raw_time);
    
    wattron(ui->output_win, COLOR_PAIR(3));
    wprintw(ui->output_win, "\n%02d:%02d:%02d  |  ", loc_time->tm_hour,
            loc_time->tm_min, loc_time->tm_sec);
    wattroff(ui->output_win, COLOR_PAIR(3));
    wrefresh(ui->output_win);
    
    log_exit(g->log, __func__);
}

void ui_print(UI* ui, char* str)
{
    UIEvent event;
    
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    event.op  = PRINT;
    event.len = strlen(str);
    memcpy(&event.buf, str, event.len);
    event.buf[event.len] = '\0';
    ui_fifo_push_event(ui->fifo, event);
    
    log_exit(g->log, __func__);
}

void ui_print_success(UI* ui, char* str)
{
    UIEvent event;
    
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    log_append_debug(g->log, str);
    
    event.op  = PRINT_SUCCESS;
    event.len = strlen(str);
    memcpy(&event.buf, str, event.len);
    event.buf[event.len] = '\0';
    ui_fifo_push_event(ui->fifo, event);
    
    log_exit(g->log, __func__);
}

void ui_print_error(UI* ui, char* str)
{
    UIEvent event;
    
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    log_append_debug(g->log, str);
    
    event.op  = PRINT_ERROR;
    event.len = strlen(str);
    memcpy(&event.buf, str, event.len);
    event.buf[event.len] = '\0';
    ui_fifo_push_event(ui->fifo, event);
    
    log_exit(g->log, __func__);
}

void ui_print_warning(UI* ui, char* str)
{
    UIEvent event;
    
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    log_append_debug(g->log, str);
    
    event.op  = PRINT_WARNING;
    event.len = strlen(str);
    memcpy(&event.buf, str, event.len);
    event.buf[event.len] = '\0';
    ui_fifo_push_event(ui->fifo, event);
    
    log_exit(g->log, __func__);
}

/* print to the single-lined area at the very bottom of the window, intended */
/* to show the current status of the server, clears the old status */
void ui_print_status_good(UI* ui, char* str)
{
    UIEvent event;
    
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    event.op  = PRINT_STATUS_GOOD;
    event.len = strlen(str);
    memcpy(&event.buf, str, event.len);
    event.buf[event.len] = '\0';
    ui_fifo_push_event(ui->fifo, event);
    
    log_exit(g->log, __func__);
}

void ui_print_status_bad(UI* ui, char* str)
{
    UIEvent event;
    
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    event.op  = PRINT_STATUS_BAD;
    event.len = strlen(str);
    memcpy(&event.buf, str, event.len);
    event.buf[event.len] = '\0';
    ui_fifo_push_event(ui->fifo, event);
    
    log_exit(g->log, __func__);
}

/* prints the project name in a stylized way */
void ui_print_splash(UI* ui)
{
    /* error case */
    if (ui == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    wattron(ui->output_win, COLOR_PAIR(4));
    wprintw(ui->output_win, "\n ooo        ooooo    oooo   .oooooo.   ooooo    "
            "    ooooo      ooo ooooo ooooooooo.   ");
    wprintw(ui->output_win, "\n `88.       .888\'    `888  d8P\'  `Y8b  `888\' "
            "       `888b.     `8\' `888\' `888   `Y88. ");
    wprintw(ui->output_win, "\n  888b     d\'888      888 888      888  888    "
            "      8 `88b.    8   888   888   .d88\' ");
    wprintw(ui->output_win, "\n  8 Y88. .P  888      888 888      888  888     "
            "     8   `88b.  8   888   888ooo88P\'  ");
    wprintw(ui->output_win, "\n  8  `888\'   888      888 888      888  888    "
            "      8     `88b.8   888   888`88b.    ");
    wprintw(ui->output_win, "\n  8    Y     888      888 `88b    d88\'  888    "
            "   o  8       `888   888   888  `88b.  ");
    wprintw(ui->output_win, "\n o8o        o888o .o. 88P  `Y8bood8P\'  o888oooo"
            "ood8 o8o        `8  o888o o888o  o888o ");
    wprintw(ui->output_win, "\n                  `Y888P                        "
            "                                      \n");
    wattroff(ui->output_win, COLOR_PAIR(4));
    wrefresh(ui->output_win);
    
    log_exit(g->log, __func__);
}

void ui_handle_resize(UI* ui)
{
    log_enter(g->log, __func__);
    
    endwin();
    refresh();
    clear();
    
    if ((LINES < 24) || (COLS < 76))
    {
        ui_window_too_small(ui);
    }
    else
    {
        ui_create_output_win(ui);
        ui_create_input_win(ui);
        ui_create_status_win(ui);
        wrefresh(ui->output_win);
        wcursyncup(ui->input_win);
        wrefresh(ui->input_win);
    }
    
    log_exit(g->log, __func__);
}

int ui_keyboard_input(UI* ui, char* buffer)
{
    static char internal_buffer[128] = { '\0' };
    static int  len = 0; /* length of buffer or -1 indicates a reset */
    int         ch;
    
    /* error case */
    if (ui == NULL)
    {
        return 0;
    }
    
    /* log_enter(g->log, __func__); */ /* too many, commented */
    
    /* if the user hit enter last time this function was called */
    if (len == -1)
    {
        /* clear the buffer */
        memset(internal_buffer, '\0', 128);
        len = 0;
    }
    
    /* move the cursor to column indicated by len */
    wmove(ui->input_win, 0, len);
    wrefresh(ui->input_win);
    /* getch ("get char") will block for up to a tenth of a second */
    ch = getch();
    
    if ((ch == KEY_ENTER) || (ch == '\n'))
    {
        int i;
        
        if (len == 0)
        {
            /* user hit enter without typing anything, ignore it */
            return 0;
        }
        
        for (i = 0; i < len; i++)
        {
            /* copy the string held in local memory into the paramter */
            (*buffer) = internal_buffer[i];
            buffer++; /* increment to the next index in buffer */
        }
        (*buffer) = '\0'; /* add a null terminator */
        memset(internal_buffer, '\0', 128); /* clear local memory */
        wclear(ui->input_win);
        wrefresh(ui->input_win);
        len = -1;
        
        return 1;
    }
    /* the backspace value varies from system to system; we'll do a check */
    /* against KEY_BACKSPACE but also include 127 (Linux) and 8 (other */
    /* Unixes) just in case */
    else if ((ch == KEY_BACKSPACE) || (ch == 127) || (ch == 8))
    {
        /* move the cursor back one column */
        wmove(ui->input_win, 0, len - 1);
        /* delete the character under the cursor in the input window */
        wdelch(ui->input_win);
        len--;
        wrefresh(ui->input_win);
    }
    /* if a key other than the ones above was pressed */
    else if (ch != ERR)
    {
        internal_buffer[len++] = (char) ch;
        wprintw(ui->input_win, (char*) &ch);
        wrefresh(ui->input_win);
    }
    
    /* log_exit(g->log, __func__); */ /* too many, commented */
    
    return 0;
}

void ui_refresh(UI* ui)
{
    UIEvent ev;
    
    if (ui == NULL)
    {
        return;
    }
    
    /* log_enter(g->log, __func__); */ /* too many, commented */
    
    do
    {
        ev = ui_fifo_pop_event(ui->fifo);
        
        switch (ev.op)
        {
            char str[256];
            
            /* clear out any string currently in the array */
            memset(str, '\0', 256);
            
            case PRINT:
                strcpy(str, (char*) ev.buf);
                ui_print_timestamp(ui);
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "%s", str);
                wattroff(ui->output_win, COLOR_PAIR(3));
                wrefresh(ui->output_win);
                break;
            case PRINT_SUCCESS:
                strcpy(str, (char*) ev.buf);
                ui_print_timestamp(ui);
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "[");
                wattroff(ui->output_win, COLOR_PAIR(3));
                wattron(ui->output_win, COLOR_PAIR(2));
                wprintw(ui->output_win, "success");
                wattroff(ui->output_win, COLOR_PAIR(2));
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "] ");
                wattroff(ui->output_win, COLOR_PAIR(3));
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "%s", str);
                wattroff(ui->output_win, COLOR_PAIR(3));
                wrefresh(ui->output_win);
                break;
            case PRINT_ERROR:
                strcpy(str, (char*) ev.buf);
                ui_print_timestamp(ui);
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "[");
                wattroff(ui->output_win, COLOR_PAIR(3));
                wattron(ui->output_win, COLOR_PAIR(5));
                wprintw(ui->output_win, "error");
                wattroff(ui->output_win, COLOR_PAIR(5));
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "] ");
                wattroff(ui->output_win, COLOR_PAIR(3));
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "%s", str);
                wattroff(ui->output_win, COLOR_PAIR(3));
                wrefresh(ui->output_win);
                break;
            case PRINT_WARNING:
                strcpy(str, (char*) ev.buf);
                ui_print_timestamp(ui);
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "[");
                wattroff(ui->output_win, COLOR_PAIR(3));
                wattron(ui->output_win, COLOR_PAIR(4));
                wprintw(ui->output_win, "warning");
                wattroff(ui->output_win, COLOR_PAIR(4));
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "] ");
                wattroff(ui->output_win, COLOR_PAIR(3));
                wattron(ui->output_win, COLOR_PAIR(3));
                wprintw(ui->output_win, "%s", str);
                wattroff(ui->output_win, COLOR_PAIR(3));
                wrefresh(ui->output_win);
                break;
            case PRINT_STATUS_GOOD:
                strcpy(str, (char*) ev.buf);
                wclear(ui->status_win);
                wattron(ui->status_win, COLOR_PAIR(2));
                wprintw(ui->status_win, "%s", str);
                wattroff(ui->status_win, COLOR_PAIR(2));
                wrefresh(ui->status_win);
                break;
            case PRINT_STATUS_BAD:
                strcpy(str, (char*) ev.buf);
                wclear(ui->status_win);
                wattron(ui->status_win, COLOR_PAIR(5));
                wprintw(ui->status_win, "%s", str);
                wattroff(ui->status_win, COLOR_PAIR(5));
                wrefresh(ui->status_win);
                break;
            case UI_NO_OP:
            default:
                break;
        }
    }
    while (ev.op != UI_NO_OP);
    
    wrefresh(ui->output_win);
    wcursyncup(ui->input_win);
    wrefresh(ui->input_win);
    
    /* log_exit(g->log, __func__); */ /* too many, commented */
}





UIFIFO* ui_fifo_create(UI* ui)
{
    UIFIFO* fifo;
    
    log_enter(g->log, __func__);
    
    fifo          = (UIFIFO*) malloc(sizeof(UIFIFO));
    fifo->first   = NULL;
    fifo->last    = NULL;
    pthread_mutex_init(&fifo->mutex, NULL);
    
    log_exit(g->log, __func__);
    
    return fifo;
}

void ui_fifo_destroy(UIFIFO* fifo)
{
    UINode* curr;
    UINode* next;
    
    /* error case */
    if (fifo == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    curr = fifo->first;
    while (curr != NULL)
    {
        next = curr->next;
        free(curr);
        curr = next;
    }
    
    pthread_mutex_destroy(&fifo->mutex);
    free(fifo);
    
    log_exit(g->log, __func__);
}

void ui_fifo_push_event(UIFIFO* fifo, UIEvent event)
{
    UINode* node;
    
    /* error case */
    if (fifo == NULL)
    {
        return;
    }
    
    log_enter(g->log, __func__);
    
    node        = (UINode*) malloc(sizeof(UINode));
    node->event = event;
    node->next  = NULL;
    
    pthread_mutex_lock(&fifo->mutex);
    if (fifo->first == NULL)
    {
        fifo->first = node;
        fifo->last  = node;
    }
    else /* fifo->first != NULL */
    {
        fifo->last->next = node;
        fifo->last       = node;
    }
    pthread_mutex_unlock(&fifo->mutex);
    
    log_exit(g->log, __func__);
}

UIEvent ui_fifo_pop_event(UIFIFO* fifo)
{
    UIEvent ev;
    UINode* node;
    
    /* log_enter(g->log, __func__); */ /* too many, commented */
    
    /* if there are no pending events, or error case */
    if ((fifo == NULL) || (fifo->first == NULL))
    {
        ev.op = UI_NO_OP;
        return ev;
    }
    
    pthread_mutex_lock(&fifo->mutex);
    ev          = fifo->first->event;
    node        = fifo->first;
    fifo->first = fifo->first->next;
    free(node);
    pthread_mutex_unlock(&fifo->mutex);
    
    /* log_exit(g->log, __func__); */ /* too many, commented */
    
    return ev;
}
