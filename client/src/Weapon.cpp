#include <cstddef> /* for NULL */
#include <cstdio> /* for printf */
#include <cstdlib> /* for rand */

#include "Weapon.hpp"

Weapon::Weapon()
{
    active  = false;
    damage  = 0;
    focus   = NULL;
    subtype = 1;
}

Weapon::~Weapon()
{
    std::list<Projectile*>::iterator it;
    for (it = projs.begin(); it != projs.end(); it++)
    {
        delete (*it);
    }
    projs.clear();
}

void Weapon::step(int dt)
{
    std::list<Projectile*>::iterator it;
    for (it = projs.begin(); it != projs.end(); it++)
    {        
        if ((*it)->active)
        {
            (*it)->step(dt);
        }
    }
}

void Weapon::draw()
{    
    std::list<Projectile*>::iterator it;
    for (it = projs.begin(); it != projs.end(); it++)
    {
        if ((*it)->active)
        {
            (*it)->draw();
        }
    }
}

void Weapon::type(int t)
{
    std::list<Projectile*>::iterator it;
    
    subtype = t;
    for (it = projs.begin(); it != projs.end(); it++)
    {
        delete (*it);
    }
    projs.clear();
    init();
}

/*******************************************************************************
 *                           WeaponArmour
 ******************************************************************************/

void WeaponArmour::step(int dt)
{
    
}

bool WeaponArmour::init()
{
    return true;
}

void WeaponArmour::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponMage
 ******************************************************************************/

void WeaponMage::step(int dt)
{
    
}

bool WeaponMage::init()
{
    return true;
}

void WeaponMage::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponNakmachine
 ******************************************************************************/

void WeaponNakmachine::step(int dt)
{
    
}

bool WeaponNakmachine::init()
{
    return true;
}

void WeaponNakmachine::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponTrico
 ******************************************************************************/

void WeaponTrico::step(int dt)
{
    
}

bool WeaponTrico::init()
{
    return true;
}

void WeaponTrico::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponBigfoot
 ******************************************************************************/

void WeaponBigfoot::step(int dt)
{
    std::list<Projectile*>::iterator it;
    bool                             atLeastOneActive;
    
    atLeastOneActive = false;
    
    switch (subtype)
    {
        case 1:
        case 3:
            ms += dt;
            
            /* if half a second has passed since firing the last missile */
            if (ms >= 150 && bfIt != projs.end())
            {
                (*bfIt)->active = true;
                
                if (bfIt == projs.begin())
                {
                    focus = (*bfIt);
                }
                
                ms = 0;
                bfIt++;
            }
            break;
        case 2:
            
            break;
        default:
            break;
    }
    
    Weapon::step(dt);
    
    for (it = projs.begin(); it != projs.end(); it++)
    {        
        if ((*it)->active)
        {
            atLeastOneActive = true;
        }
    }
    
    if ((bfIt == projs.end()) && (atLeastOneActive == false))
    {
        active = false;
        focus  = NULL;
    }
}

bool WeaponBigfoot::init()
{
    Projectile* proj;
    int         num; /* how many projectiles to make */
    
    ms   = 0;
    bfIt = projs.begin();
    proj = NULL;
    num  = 0;

    switch (subtype)
    {
        case 1:
            num = 5;
            break;
        case 2:
            num = 3;
            break;
        case 3:
            num = 10;
            break;
        default:
            break;
    }

    for (int i = 0; i < num; i++)
    {
        switch (subtype)
        {
            case 1:
            case 3:
                proj = new ProjectileBigfoot1();
                break;
            case 2:
                proj = new ProjectileBigfoot2();
                break;
            default:
                break;
        }
        
        if (proj->init() == false)
        {
            printf("Bigfoot projectile initialization failed\n");
            return false;
        }
        
        projs.push_back(proj);
    }
    
    return true;
}

void WeaponBigfoot::fire(float x, float y, float radians, int power)
{
    float var; /* variance from radians */
    
    if (subtype == 2)
    {
        var = -0.05f;
    }
    
    active = true;
    for (bfIt = projs.begin(); bfIt != projs.end(); bfIt++)
    {
        switch (subtype)
        {
            case 1:
            case 3:
                {
                    int random; /* random integer */
                    
                    random = rand() % 100;
                    var    = 1.0f / ((float) random);
                    var    = (random % 2 == 1 ? var : -var);
                }
                break;
            case 2:
                (*bfIt)->active = true;
                var            += 0.05f;
                break;
            default:
                break;
        }
        
        (*bfIt)->sprite->position.x =  x;
        (*bfIt)->sprite->position.y =  y;
        (*bfIt)->physical->x        =  x;
        (*bfIt)->physical->y        =  y;
        (*bfIt)->physical->xVel     =  power * 10 * cos(radians + var);
        (*bfIt)->physical->yVel     = -power * 10 * sin(radians + var);
    }
    
    ms   = 0;
    bfIt = projs.begin()++;
}

/*******************************************************************************
 *                           WeaponBoomer
 ******************************************************************************/

void WeaponBoomer::step(int dt)
{
    
}

bool WeaponBoomer::init()
{
    return true;
}

void WeaponBoomer::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponRaonlauncher
 ******************************************************************************/

void WeaponRaonlauncher::step(int dt)
{
    
}

bool WeaponRaonlauncher::init()
{
    return true;
}

void WeaponRaonlauncher::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponLightning
 ******************************************************************************/

void WeaponLightning::step(int dt)
{
    
}

bool WeaponLightning::init()
{
    return true;
}

void WeaponLightning::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponJd
 ******************************************************************************/

void WeaponJd::step(int dt)
{
    
}

bool WeaponJd::init()
{
    return true;
}

void WeaponJd::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponAsate
 ******************************************************************************/

void WeaponAsate::step(int dt)
{
    
}

bool WeaponAsate::init()
{
    return true;
}

void WeaponAsate::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponIce
 ******************************************************************************/

void WeaponIce::step(int dt)
{
    
}

bool WeaponIce::init()
{
    return true;
}

void WeaponIce::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponTurtle
 ******************************************************************************/

void WeaponTurtle::step(int dt)
{
    
}

bool WeaponTurtle::init()
{
    return true;
}

void WeaponTurtle::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponGrub
 ******************************************************************************/

void WeaponGrub::step(int dt)
{
    
}

bool WeaponGrub::init()
{
    return true;
}

void WeaponGrub::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponAduka
 ******************************************************************************/

void WeaponAduka::step(int dt)
{
    
}

bool WeaponAduka::init()
{
    return true;
}

void WeaponAduka::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponKalsiddon
 ******************************************************************************/

void WeaponKalsiddon::step(int dt)
{
    
}

bool WeaponKalsiddon::init()
{
    return true;
}

void WeaponKalsiddon::fire(float x, float y, float radians, int power)
{
    
}

/*******************************************************************************
 *                           WeaponJfrog
 ******************************************************************************/

void WeaponJfrog::step(int dt)
{
    
}

bool WeaponJfrog::init()
{
    return true;
}

void WeaponJfrog::fire(float x, float y, float radians, int power)
{
    
}
