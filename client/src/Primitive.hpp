#ifndef _PRIMITIVE_H_
#define _PRIMITIVE_H_

#include <GL/glew.h>
#include <GL/gl.h>

#include "Drawable.hpp"

enum PrimitiveShapes
{
    PRIMITIVE_QUAD,
    PRIMITIVE_CIRCLE
};

class Primitive : public Drawable
{
public:

    Primitive();
    ~Primitive();
    
    void init(int shape);
    void draw();

    glm::vec2 position; /* X and Y coordinates */
    glm::vec2 dimensions; /* width and height */
    glm::vec3 color; /* RGB color */

private:

    GLsizei   glCount; /* number of verties */
    GLenum    glMode; /* draw mode (line, triangle, quad, etc.) */
    GLuint    glVAO; /* vertex array object */
    GLuint    glVBO; /* vertex buffer object */
};

#endif
