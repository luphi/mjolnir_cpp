#ifndef _SCORESTATE_H_
#define _SCORESTATE_H_

#include "State.hpp"

class ScoreState : public State
{
public:

    ScoreState();
    ~ScoreState();
    
    void draw();
    void resize();
    void poll();
    void step(int dt);
    int  nextState();
    
    int stateChange;
};

#endif
