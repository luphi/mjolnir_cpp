#include <cmath> /* for atan2 */
#include <cstddef> /* for NULL */
#include <cstdio> /* for printf */

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "Collisions.hpp"
#include "DevState.hpp"
#include "Event.hpp"
#include "GameState.hpp"
#include "Globals.hpp"
#include "LobbyState.hpp"
#include "MenuState.hpp"
#include "OptionsState.hpp"
#include "ScoreState.hpp"
#include "ServerState.hpp"
#include "Socket.hpp"
#include "State.hpp"
#include "TextInput.hpp"

#define GLSL(src) "#version 330 core\n" #src

/**
 * 
 * TODO
 * 
 * Server list (server state)
 * Choose viking and wait for others (lobby state)
 * Score board after game (score state)
 * Prevent movement when player changes direction
 * Prevent load time from adding to dt in initial step call
 * Individual server
 * Master server
 * Voice chat
 * Health, stats, scores, other player values
 * Complete list and implementation of events
 * Firing power
 * Parallaxed backgrounds
 * Rework hill climbing
 * Turn queue and game logic to use it
 * Shield regeneration
 * Rename players based on norse saga "characters"
 * 
 */

bool initSDL()
{
    Globals* globals = Globals::Instance();
    
    /* initialize SDL's subsystems */
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL failed to initialize\n");
        return false;
    }
    
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL,    1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,          1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,           24);
    
    /* open system-specific window */
    globals->window = SDL_CreateWindow("Mjolnir", SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED, globals->vidWidth, globals->vidHeight,
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (globals->window == NULL)
    {
        printf("Failed to initilaize window\n");
        return false;
    }
    
    if (TTF_Init() == -1)
    {
        printf("SDL TTF failed to initialize (%s), exiting...\n",
            TTF_GetError());
        return false;
    }
    
    SDL_StartTextInput();
    
    return true;
}

bool initOpenGL()
{
    Globals*  globals;
    GLenum    glewStatus;
    GLint     glStatus;
    glm::mat4 viewMatrix;
    
    globals = Globals::Instance();
    
    const GLchar* spriteVertexSource = GLSL(
            /**************************** GLSL ********************************/
            layout (location = 0) in vec2 position;
            layout (location = 1) in vec2 texCoord;
            
            layout (std140) uniform Matrices
            {
                mat4 projection;
                mat4 view;
            };
            
            uniform mat4 spriteModel;
            
            out vec2 TexCoord;
            
            void main()
            {
                TexCoord    = texCoord;
                gl_Position = projection * view * spriteModel *
                              vec4(position, 0.0f, 1.0f);
            }
            /******************************************************************/
        );
    const GLchar* spriteFragmentSource = GLSL(
            /**************************** GLSL ********************************/
            in vec2 TexCoord;
            
            uniform sampler2D spriteTexture;
            
            out vec4 outColor;
            
            void main()
            {
                vec4 fragment = texture(spriteTexture, TexCoord);
                if (fragment.a < 0.01)
                {
                    discard;
                }
                outColor = fragment;
            }
            /******************************************************************/
        );
    const GLchar* primitiveVertexSource = GLSL(
            /**************************** GLSL ********************************/
            layout (location = 0) in vec2 position;
            
            layout (std140) uniform Matrices
            {
                mat4 projection;
                mat4 view;
            };
            
            uniform mat4 primitiveModel;
            
            void main()
            {
                gl_Position = projection * view * primitiveModel *
                    vec4(position, 0.0f, 1.0f);
            }
            /******************************************************************/
        );
    const GLchar* primitiveFragmentSource = GLSL(
            /**************************** GLSL ********************************/
            uniform vec3 color;
            
            out vec4 outColor;
            
            void main()
            {
                outColor = vec4(color, 1.0f);
            }
            /******************************************************************/
        );
    
    /* create OpenGL context and set it to current */
    globals->glContext = SDL_GL_CreateContext(globals->window);

    glewExperimental = GL_TRUE;
    glewStatus = glewInit();
    if (glewStatus != GLEW_OK)
    {
        printf("Failed to initialize GLEW\n");
        return false;
    }

    glViewport(0, 0, globals->vidWidth, globals->vidHeight);

    globals->glSpriteVShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(globals->glSpriteVShader, 1, &spriteVertexSource, NULL);
    glCompileShader(globals->glSpriteVShader);
    glGetShaderiv(globals->glSpriteVShader, GL_COMPILE_STATUS, &glStatus);
    if (glStatus != GL_TRUE)
    {
        printf("Sprite vertex shader compilation failed\n");
    }
    
    globals->glPrimitiveVShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(globals->glPrimitiveVShader, 1, &primitiveVertexSource,
        NULL);
    glCompileShader(globals->glPrimitiveVShader);
    glGetShaderiv(globals->glPrimitiveVShader, GL_COMPILE_STATUS, &glStatus);
    if (glStatus != GL_TRUE)
    {
        printf("Primitive vertex shader compilation failed\n");
    }

    globals->glSpriteFShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(globals->glSpriteFShader, 1, &spriteFragmentSource, NULL);
    glCompileShader(globals->glSpriteFShader);
    glGetShaderiv(globals->glSpriteFShader, GL_COMPILE_STATUS, &glStatus);
    if (glStatus != GL_TRUE)
    {
        printf("Sprite fragment shader compilation failed\n");
    }
    
    globals->glPrimitiveFShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(globals->glPrimitiveFShader, 1, &primitiveFragmentSource,
        NULL);
    glCompileShader(globals->glPrimitiveFShader);
    glGetShaderiv(globals->glPrimitiveFShader, GL_COMPILE_STATUS, &glStatus);
    if (glStatus != GL_TRUE)
    {
        printf("Primitive fragment shader compilation failed\n");
    }

    globals->glSpriteProgram = glCreateProgram();
    glAttachShader(globals->glSpriteProgram, globals->glSpriteVShader);
    glAttachShader(globals->glSpriteProgram, globals->glSpriteFShader);
    glBindFragDataLocation(globals->glSpriteProgram, 0, "outColor");
    glLinkProgram(globals->glSpriteProgram);
    glGetShaderiv(globals->glSpriteProgram, GL_LINK_STATUS, &glStatus);
    if (glStatus != GL_TRUE)
    {
        printf("Sprite program link failed\n");
    }
    
    globals->glPrimitiveProgram = glCreateProgram();
    glAttachShader(globals->glPrimitiveProgram, globals->glPrimitiveVShader);
    glAttachShader(globals->glPrimitiveProgram, globals->glPrimitiveFShader);
    glBindFragDataLocation(globals->glPrimitiveProgram, 0, "outColor");
    glLinkProgram(globals->glPrimitiveProgram);
    glGetShaderiv(globals->glPrimitiveProgram, GL_LINK_STATUS, &glStatus);
    if (glStatus != GL_TRUE)
    {
        printf("Primitive program link failed\n");
    }

    glUniformBlockBinding(globals->glSpriteProgram,
        glGetUniformBlockIndex(globals->glSpriteProgram, "Matrices"), 0);
    glUniformBlockBinding(globals->glPrimitiveProgram,
        glGetUniformBlockIndex(globals->glPrimitiveProgram, "Matrices"), 0);
    glGenBuffers(1, &globals->glUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, globals->glUBO);
    glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL,
        GL_STATIC_DRAW);
    glBindBufferRange(GL_UNIFORM_BUFFER, 0, globals->glUBO, 0,
        2 * sizeof(glm::mat4));
    
    globals->projectionMatrix = glm::ortho(0.0f, (GLfloat) globals->vidWidth,
        (GLfloat) globals->vidHeight, 0.0f, -1.0f, 1.0f);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4),
        glm::value_ptr(globals->projectionMatrix));
    
    viewMatrix = glm::mat4(1.0f);
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) /* offset */,
        sizeof(glm::mat4), glm::value_ptr(viewMatrix));

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    return true;
}

void quitSDL()
{
    Globals* globals = Globals::Instance();
    
    if (globals->window)
    {
        SDL_GL_DeleteContext(globals->glContext);
        SDL_DestroyWindow(globals->window);
    }
    
    if (TTF_WasInit())
    {
        TTF_Quit();
    }
    
    SDL_Quit();
}

void quitOpenGL()
{
    Globals* globals = Globals::Instance();
    
    glDeleteBuffers(1, &globals->glUBO);
    glDeleteProgram(globals->glSpriteProgram);
    glDeleteProgram(globals->glPrimitiveProgram);
    glDeleteShader(globals->glSpriteFShader);
    glDeleteShader(globals->glPrimitiveFShader);
    glDeleteShader(globals->glSpriteVShader);
    glDeleteShader(globals->glPrimitiveVShader);
}

int main(int argc, char** argv)
{
    Globals* globals;
    State*   state;
    bool     quit;
    int      ticks;
    int      oldTicks;
    int      dt;
    int      t;
    int      stateChange;
    
    globals             = Globals::Instance();
    state               = NULL;
    quit                = false;
    ticks               = 0;
    dt                  = 10; /* 10 milliseconds = 100 Hz */
    t                   = 0;
    stateChange         = 0;
    
    /* initialize window, keyboard input, etc. */
    if (initSDL() == false)
    {
        printf("SDL initialization failed, exiting...\n");
        quit = true;
    }
    
    /* initialize context, shaders, profile, etc. */
    if ((!quit) && (initOpenGL() == false))
    {
        printf("OpenGL initialization failed, exiting...\n");
        quit = true;
    }
    
    if (!quit)
    {
        globals->font = TTF_OpenFont("media/fonts/font.ttf", 16);
        state         = new MenuState();
        oldTicks      = SDL_GetTicks(); 
    }
    
    while (quit == false) /* while (not quitting) */
    {
        glClear(GL_COLOR_BUFFER_BIT);
        
        ticks    = SDL_GetTicks() - oldTicks;
        oldTicks = ticks + oldTicks;
        t        = 0;
        
        state->poll();
                
        if (ticks == 0)
        {
            state->step(0);
        }
        
        while (t < ticks)
        {
            state->step(dt);
            t += dt;
        }
        
        state->draw();
        
        stateChange = state->nextState();
        if (stateChange != NO_CHANGE)
        {
			delete state;
			
            switch (stateChange)
            {
                case MENU_STATE:
                    state = new MenuState();
                    break;
                case SERVER_STATE:
					state = new ServerState();
					break;
				case LOBBY_STATE:
					state = new LobbyState();
					break;
                case GAME_STATE:
                    state = new GameState();
                    break;
                case SCORE_STATE:
					state = new ScoreState();
					break;
                case DEV_STATE:
                    state = new DevState();
                    break;
                case OPTIONS_STATE:
					state = new OptionsState();
                    break;
                case EXIT:
                    quit  = true;
                    state = NULL;
                    break;
            }
        }
                
        SDL_GL_SwapWindow(globals->window);
    }
    
    quitOpenGL();
    quitSDL();
    
    delete globals;
    
    return 0;
}
